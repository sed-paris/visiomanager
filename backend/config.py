"""Flask configuration variables."""
import json

from pathlib import Path

CONFIG_DIR = Path('configs').resolve()
filename = (CONFIG_DIR / 'global.json').resolve()
with open(filename) as config_file:
    global_config = json.load(config_file)


class Config:
    """Set Flask configuration from .env file."""

    DEBUG = False
    TESTING = False

    # CORS
    CORS_SUPPORT_CREDENTIALS = True

    # Database
    DB_PATH = Path('../resources/visiomanager_sqlite.db').resolve()
    SQLALCHEMY_DATABASE_URI = f"sqlite:///{str(DB_PATH)}"
    SQLALCHEMY_ECHO = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    # Emails
    EMAILS = CONFIG_DIR / 'email' / 'emails.json'

    # CAS
    if 'cas' in global_config['auth']:
        CAS_URL = global_config['auth']['cas']['url']

    # Flask session (cookie)
    # defines path (so that all cookies on same path - do not depend on api call subpath)
    SESSION_COOKIE_PATH = '/'

    # Swagger/Flasgger API docs
    SWAGGER = {
        "swagger_version": "2.0",
        "title": "Visiomanager Backend",
        "headers": [
        ],
        "specs": [
            {
                "version": "0.3.3",
                "title": "Visiomanager backend API",
                "endpoint": 'apispec',
                "description": 'This is the backend API ',
                "route": '/api/apispec.json'
            }
        ],
        "static_url_path": "/api/flasgger_static",  # js libs, images (defualt is /static
        # "static_folder": "static",  # must be set by user
        "swagger_ui": True,
        "specs_route": "/api/docs/"  # the subpath for the flasgger API docs (defult is apidocs)
    }


# Production setup using mostly defaults for Flask. In particular
# APPLICATION_ROOT is by default "/"
class ProductionConfig(Config):
    FLASK_ENV = 'production'
    DEBUG = False
    APPLICATION_ROOT = 'api/'
    WEB_URL = global_config['web_urls']['production']
    CORS_ORIGINS = f"{WEB_URL}/*"
    SCW_FILE_DEFS = CONFIG_DIR / 'scaleway' /'scaleway_prod.json'
    SESSION_COOKIE_SECURE = True  # Only for https request

class StagingConfig(Config):
    DEBUG = True
    FLASK_ENV = 'staging'
    DEVELOPMENT = True
    APPLICATION_ROOT = 'api/'
    WEB_URL = global_config['web_urls']['staging']
    CORS_ORIGINS = f"{WEB_URL}/*"
    SCW_FILE_DEFS = CONFIG_DIR / 'scaleway' / 'scaleway_qualif.json'
    SESSION_COOKIE_SECURE = True  # Only for https request

class DevelopmentConfig(Config):
    DEBUG = True
    FLASK_ENV = 'development'
    DEVELOPMENT = True
    APPLICATION_ROOT = 'api/'
    WEB_URL = global_config['web_urls']['development']
    CORS_ORIGINS = f"{WEB_URL}/*"
    SCW_FILE_DEFS = CONFIG_DIR / 'scaleway' / 'scaleway_test.json'

    # defines if session cookies visible to browser debug/javascript - false = visible
    SESSION_COOKIE_HTTPONLY = False

# Configuration for running unit tests on a development platform
# TESTING set to True so that Scheduler not started. Ensure there is
# no background cleanup tasks running that would send emails or terminate
# servers on the Scaleway test project as a side effect.
# This configuration thus prevents the test of any scheduler task.
class TestingConfig(Config):
    DEBUG = True
    TESTING = True
    DEVELOPMENT = False
    FLASK_ENV = 'testing'
    SQLALCHEMY_DATABASE_URI = "sqlite:///:memory:"
    APPLICATION_ROOT = 'api/'
    SCW_FILE_DEFS = Path('tests/data/scaleway_test.json').resolve()
