import logging
import argparse
import os

# Env to be loaded before import visiomanager
from tools import get_config

from visiomanager import create_app
from visiomanager.scheduler import Scheduler

from visiomanager.models import DataManager

logger = logging.getLogger(__name__)


if __name__ == "__main__":

    # Retrieve environment configuration
    parser = argparse.ArgumentParser(description="Run the backend server")
    parser.add_argument('--config',
                        choices=['dev', 'test', 'qualif' ,'prod'],
                        default='dev',
                        help="Environment configuration type. Default:dev")
    args = parser.parse_args()

    config = get_config(args.config)

    # Launch backend server
    try:
        app = create_app(config)
        data_manager = DataManager()

        with app.app_context():
            if not app.config['TESTING']:
                daemonThread = Scheduler(interval=30,
                                         alert_interval=3600,
                                         app=app)

            app.run(host='0.0.0.0', port=5002,
                    use_reloader=False, load_dotenv=False)
            data_manager.terminate()
    except SystemExit as e:
        logger.error(e.args)
        os._exit(1)
