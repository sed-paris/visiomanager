from dotenv import load_dotenv
from pathlib import Path

# Load dot env configuration (located at project backend root)
# Priority:
#    Value of that variable in the .env file.
#    Value of that variable in the environment.
#    Default value, if provided.
#    Empty string.

load_dotenv(Path('./backend.env').resolve(), override=True)


def get_config(key):
    config_switcher = {
        "development": 'config.DevelopmentConfig',
        "dev": 'config.DevelopmentConfig',
        "test": 'config.TestingConfig',
        "ci": 'config.TestingConfig',
        "qualif": 'config.StagingConfig',
        "prod": 'config.ProductionConfig',
        "production": 'config.ProductionConfig'
    }
    return config_switcher.get(key)
