import argparse
from datetime import datetime
import logging

from pathlib import Path
from shutil import copyfile

# Env to be loaded before import visiomanager
from tools import get_config

from visiomanager import create_app
from visiomanager.models import DataManager


logger = logging.getLogger(__name__)


def init_db(app, args):
    logger.debug(f"Init db: args={args}")

    input_dir = Path(args.input_directory).resolve()

    # Initializing database with data files
    userfile = input_dir / 'user_data.json'
    requestfile = input_dir / 'request_data.json'
    instancefile = input_dir / 'instance_data.json'

    with app.app_context():

        data_manager = DataManager()
        # populate user
        if userfile.is_file():
            data_manager.init_users(userfile)
        else:
            logger.warning(f"File {userfile} does not exist")
        # populate requests
        if requestfile.is_file():
            data_manager.init_requests(requestfile)
        else:
            logger.warning(f"File {requestfile} does not exist")
        # populate instances
        if instancefile.is_file():
            data_manager.init_instances(instancefile)
        else:
            logger.warning(f"File {instancefile} does not exist")


def save_db(app, args=None):
    logger.debug(f"Save database: args:{args}")
    with app.app_context():
        db_path = Path(app.config['DB_PATH']).resolve()
        now = datetime.utcnow().strftime("%Y-%M-%d_%H-%M-%S")
        backup_path = db_path.parent / f'backup_{now}'
        copyfile(db_path, backup_path)
        logger.debug(f"The database is saved: {backup_path}")


def restore_db(app, args):
    logger.debug(f"Restore: args={args}")
    with app.app_context():
        save_db()
        backup_path = Path(args.backup_path).resolve()
        db_path = Path(app.config['DB_PATH']).resolve()
        copyfile(backup_path, db_path)
        logger.debug(f"The database {backup_path} is restored to {db_path}")


if __name__ == "__main__":

    logging.basicConfig(level=logging.DEBUG,
                        format='%(asctime)s %(levelname)s:%(message)s')

    parser = argparse.ArgumentParser(description="Manage the database")
    parser.add_argument('--config',
                        choices=['dev', 'test', 'qualif' ,'prod'],
                        default='dev',
                        help="Environment configuration type. Default:dev")

    subparsers = parser.add_subparsers(title="Available subcommands")

    # Parser for init sub-command
    psr_init = subparsers.add_parser('init', help="Initialize the database")
    psr_init.add_argument('input_directory',
                          help="Path to directory containing data for db")
    psr_init.set_defaults(func=init_db)

    # Parser for save sub-command
    psr_save = subparsers.add_parser('save', help="Save the database")
    psr_save.set_defaults(func=save_db)

    # Parser for restore sub-command
    psr_restore = subparsers.add_parser('restore', help="Restore the database")
    psr_restore.add_argument('backup_path',
                             help="Path to database to restore")

    psr_restore.set_defaults(func=restore_db)

    args = parser.parse_args()
    config = get_config(args.config)

    # Create app
    app = create_app(config)

    # Launch subcommand
    args.func(app, args)


