import io

from setuptools import find_packages
from setuptools import setup

with io.open("README.md", "rt", encoding="utf8") as f:
    readme = f.read()

setup(
    name="visiomanager",
    version="1.0.0",
    url="https://gitlab.inria.fr/praverdy/visiomanager",
    license="TBD",
    maintainer="SED Paris",
    maintainer_email="sed-pro@inria.fr",
    description="Back-end server to launch BBB instances on scaleway.",
    long_description=readme,
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        "flask",
        "flask-cors",
        "pyjwt>=1.7.1,<2",
        "sqlalchemy",
        "flask-sqlalchemy",
        "python-dotenv",
        "flasgger",
        "coveralls",
        "Flask_Testing",
        "Flask-Script",
        "bcrypt",
        "python-cas",
        "python-dateutil"],
    extras_require={"test": ["pytest", "pytest-mock", "coverage"]},
)
