[[_TOC_]]
# Backend Flask server for VisioManager

The backend server uses the Flask framework.
This documentation explains how to configure, launch and test backend server.

**Assumption** : we suppose that we start in `backend` directory.

## Environment requirements

You need to install python packages listed in requirements file.

**Note**: `PYTHONPATH` environment variable shall point to `backend` folder.

Create conda environment and install required packages:

```
conda create --name visiomanager python=3.8
conda activate visiomanager
pip install -r requirements.txt'

# For unit testing
pip install -r requirements-dev.txt
```

## Configuring

To configure backend server, you need to:

- define configuration files
- set some environment variables
- create/use a sqlite database

### Configuration files

For configuring backend server, you have to put your configuration files in
`configs` directory.

An example is provided. You have to update it with your own configuration.
Refer to [./configs/README.md](./configs/README.md) for more details.

### Environment variables

Backend server requires some environment variables to be set for running
correctly.

You have two possibilities for setting the variables:

- directly in shell.
- using a python `backend.env` file located at the backend root. (cf. dotenv)

If this file exists, backend server will load it, and overrides shell variables
values.

For scaleway configuration (used by the shell scripts):

- *SCW_PROJECT_ID* : the ID of the scaleway project
- *SCW_BBB_ZONE* : the deployment zone for the resources (e.g., fr-par-1)
- *SCW_S_KEY* : the secret key for the app setup on the Scaleway credentials page
- *VM_ID_FILE* : name of the identify file (private key) to be used on the Webapp
  host (in $HOME) to access scaleway instances (e.g., *id_rsa*)

For email configuration:

- *VM_SENDER_ACCOUNT_EMAIL* : email address of the account used for notifying users
- *VM_SENDER_ACCOUNT_PWD* : password of the account to use for sending emails to clients
- *VM_SENDER_ACCOUNT_SMTP* : address of the SMTP server to use for sending emails to clients
- *VM_ADMIN_EMAIL* : destination email address used by the web app to send alerts/notifications

For database encryption:

- *DB_SALT_B64* : salt for the encrypted password (Base 64 encoded)

To get the Base64 encoded value for the salt:
```
import base64
import bcrypt
base64.b64encode(bcrypt.gensalt()).decode('utf-8')
```

**Optional (not used by backend but for other/future scripts)**

- *SCW_A_KEY* : the access key for the app setup on the Scaleway credentials page
- *SCW_ORG_ID* : the organization ID


TODO: add CAS enabling as env var because used by frontend and backend

### Database installation

Backend server needs a database located in a folder names `resources` located
at the root of the project.

If you have already an existing database for the application, it shall be
located at: `../resources/visiomanager/visiomanager_sqlite.db` with current
directory as `backend`.

If you need to initialize a new database, you can use the script tool
`tools/manage_db.py` as follows:

```
python tools/manage_db.py init <path_to_your_data>
```

`<path_to_your_data>` corresponds to the directory containing your own initial
data to populate to database.

The folder `static/data/db_test_data` contains an example of json files
to populate to database.

To use it:

```
python tools/manage_db.py init static/data/db_test_data
```

## Launching backend server

The backend server may be launched via 3 configurations:

```
conda activate visiomanager
export PYTHONPATH=.

# Development configuration
python tools/wsgi.py [--config dev]

# Qualification configuration
python tools/wsgi.py --config qualif

# Production configuration
python tools/wsgi.py --config prod
```

**Note**: If you want to locally test CAS authentication, you have to modify
your `/etc/hosts` file to use a fake domain.

As example, with the current configs directory where toto.inria.fr domain url
is used, the line to add is:

```
127.0.0.1 toto.inria.fr
```
## Testing

REST API interactive documentation is automatically generated with
[Flasgger](https://github.com/flasgger/flasgger), a Python
[swagger](https://swagger.io/) OpenAPI] implementation.

### Unit testing

```
conda activate visiomanager
pytest -v tests/

```

### API testing

For API testing, you need to launch the backend server.

Then you can access the API docs at:
[http://localhost:5002/api/docs](http://localhost:5002/api/docs)
