import os
import pytest

from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm.exc import FlushError

from visiomanager.models import User, Parameter
from visiomanager import create_app, db

from tests import DUMMY_ENV


@pytest.fixture
def app(mocker):
    mocker.patch.dict(os.environ, DUMMY_ENV)  # Stub env variables
    app = create_app('config.TestingConfig')
    with app.app_context():
        # alternative pattern to app.app_context().push()
        # all commands indented under 'with' are run in the app context
        db.create_all()

        # similar to unittests setup/teardown, everything that comes before return/yield is setup code,
        # everything that comes after yield is teardown code.
        yield app

        db.session.remove()  # looks like db.session.close() would work as well
        db.drop_all()


@pytest.fixture(params=[
    'add_user_ok',
    'add_user_fail'
])
def add_user_data(request):
    user1 = User(
        name="name1",
        login="login1",
        email="email@1.org",
        password="password1",
        role="USER",
        params={}
    )
    user2 = User(
        name="name2",
        login="login2",
        email="email@2.org",
        password="password2",
        role="USER",
        params={}
    )
    user_dup_login = User(
        name="name2",
        login="login2",
        email="nodup_email@2.org",
        password="password2",
        role="USER",
        params={}
    )
    user_dup_email = User(
        name="name2",
        login="nodup_login2",
        email="email@2.org",
        password="password2",
        role="USER",
        params={}
    )

    db.session.add(user2)
    db.session.commit()

    if request.param == 'add_user_ok':
        db.session.add(user1)
        db.session.commit()

    if request.param == 'add_user_fail':
        with pytest.raises(FlushError):
            db.session.add(user_dup_login)
            db.session.commit()
        # need to rollback due to transaction failure
        db.session.rollback()

        with pytest.raises(IntegrityError):
            db.session.add(user_dup_email)
            db.session.commit()
        # need to rollback due to transaction failure
        db.session.rollback()

    users = [user1, user2, user_dup_login, user_dup_email]

    return users, request


@pytest.fixture(params=[
    'add_param_ok'
])
def add_param_data( request):
    param1 = Parameter(
        name="par1",
        category="string",
        value="xxx"
    )
    param2 = Parameter(
        name="par2",
        category="string",
        value="yyy"
    )
    param3 = Parameter(
        name="par3",
        category="int",
        value="1"
    )

    params = [param1, param2, param3]

    return params


def test_add_user(app, add_user_data, add_param_data):
    usr_dic = add_user_data[0]
    request = add_user_data[1]

    users = db.session.query(User).filter_by(name=usr_dic[1].name).all()
    assert len(users) == 1

    if request.param == 'add_user_ok':
        users = db.session.query(User).filter_by(name=usr_dic[0].name).all()
        assert len(users) == 1

        users = db.session.query(User).filter_by(email=usr_dic[1].email).all()
        assert len(users) == 1

        users = db.session.query(User).all()
        assert len(users) == 2

        db.session.query(User).filter_by(email=usr_dic[1].email).delete()
        users = db.session.query(User).all()
        assert len(users) == 1

        user1 = users[0]

        # ensure adding params works, and list is complete
        user1.params.add_param(add_param_data[0])
        user1.params.add_param(add_param_data[1])
        user1.params.add_param(add_param_data[2])
        users = db.session.query(User).all()
        user2 = users[0]
        assert len(user2.params) == 3

        # ensure delete works
        user2.params.del_param(add_param_data[1].name)
        db.session.add(user2)
        db.session.commit()
        # still only one user (list item updated, not new user added)
        users = db.session.query(User).all()
        assert len(users) == 1
        # user in db is updated
        user3 = users[0]
        assert len(user3.params) == 2
        assert list(user3.params.keys()).index(add_param_data[2].name) == 1

        # ensure param update works
        user3.params.add_param(add_param_data[0])
        db.session.add(user3)
        db.session.commit()
        # still only one user (list item updated, not new user added)
        users = db.session.query(User).all()
        assert len(users) == 1
        # user in db is updated
        user4 = users[0]
        assert len(user4.params) == 2

        # ensure exists works
        assert user4.params.exists(add_param_data[0].name) is True
        assert user4.params.exists("bogus") is False

    if request.param == 'add_user_fail':
        users = db.session.query(User).filter_by(email=usr_dic[2].email).all()
        assert len(users) == 0

        users = db.session.query(User).filter_by(login=usr_dic[3].login).all()
        assert len(users) == 0
