import os
import pytest

from visiomanager import create_app
from visiomanager.cloud_scaleway import ScalewayCloudManager

from tests import DUMMY_ENV


@pytest.fixture
def app(mocker):
    # Stub environment variables
    mocker.patch.dict(os.environ, DUMMY_ENV)
    app = create_app('config.TestingConfig')
    with app.app_context():
        # similar to unittests setup/teardown, everything that comes before return/yield is setup code,
        # everything that comes after yield is teardown code.
        yield app


# Test case when server is available
def test_get_free_public_ip(mocker, app):
    def mock_script(script_name):
        return """
        {
            "ips": [
                {
                    "id": "string",
                    "address": "1.2.3.4",
                    "reverse": "string",
                    "server": null, 
                    "organization": "string",
                    "tags": [
                        "string"
                    ],
                    "project": "string",
                    "zone": "string"
                 }
             ],
             "total_count": 42
         }
         """
    scaleway_manager = ScalewayCloudManager(app)
    mocker.patch('visiomanager.cloud_scaleway.utils.execute_script',
                 mock_script)
    ip = scaleway_manager.get_free_public_ip()
    assert ip is not None
    assert ip['address'] == '1.2.3.4'


# Test case where all servers are busy
def test_get_free_public_ip_all_used(mocker, app):
    def mock_script(script_name):
        return """
        {
            "ips": [
                {
                    "id": "string",
                    "address": "1.2.3.4",
                    "reverse": "string",
                    "server": {
                    "id": "string",
                        "name": "string"
                    },
                    "organization": "string",
                    "tags": [
                        "string"
                    ],
                    "project": "string",
                    "zone": "string"
                 }
             ],
             "total_count": 42
         }
         """

    scaleway_manager = ScalewayCloudManager(app)
    mocker.patch('visiomanager.cloud_scaleway.utils.execute_script',
                 mock_script)

    assert scaleway_manager.get_free_public_ip() is None
