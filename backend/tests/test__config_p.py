import os
from pathlib import Path
import pytest

from visiomanager import create_app
import visiomanager.instances

from tests import DUMMY_ENV


@pytest.fixture(params=[
    'TestingConfig',
    'DevelopmentConfig',
    'StagingConfig',
    'ProductionConfig'
    ])
def config_app(request, mocker):
    mocker.patch.dict(os.environ, DUMMY_ENV)  # Stub env variables
    output_flags_dict = dict()
    if request.param == 'TestingConfig':
        app = create_app('config.TestingConfig')
        output_flags_dict = {
            'DEBUG': True,
            'FLASK_ENV': 'testing',
            'TESTING': True,
            'SQLALCHEMY_DATABASE_URI': 'sqlite:///:memory:'
        }
    if request.param == 'DevelopmentConfig':
        mocker.patch("visiomanager.instances.SSHKeyFile.exists",
                     return_value=True)
        app = create_app('config.DevelopmentConfig')
        DB_PATH = Path('../resources/visiomanager_sqlite.db').resolve()
        output_flags_dict = {
            'DEBUG': True,
            'FLASK_ENV': 'development',
            'DEVELOPMENT': True,
            'SQLALCHEMY_DATABASE_URI' : f"sqlite:///{str(DB_PATH)}"
        }
    if request.param == 'StagingConfig':
        mocker.patch("visiomanager.instances.SSHKeyFile.exists",
                     return_value=True)
        app = create_app('config.StagingConfig')
        DB_PATH = Path('../resources/visiomanager_sqlite.db').resolve()
        output_flags_dict = {
            'DEBUG': True,
            'FLASK_ENV': 'staging',
            'DEVELOPMENT': True,
            'SQLALCHEMY_DATABASE_URI' : f"sqlite:///{str(DB_PATH)}"
        }
    if request.param == 'ProductionConfig':
        mocker.patch("visiomanager.instances.SSHKeyFile.exists",
                     return_value=True)
        app = create_app('config.ProductionConfig')
        DB_PATH = Path('../resources/visiomanager_sqlite.db').resolve()
        output_flags_dict = {
            'DEBUG': False,
            'FLASK_ENV': 'production',
            'SQLALCHEMY_DATABASE_URI' : f"sqlite:///{str(DB_PATH)}"
        }
    return app, output_flags_dict


def test_config_app(config_app):

    app = config_app[0]
    output_test_data = config_app[1]

    for key, value in output_test_data.items():
        assert app.config[key] == value
