import json
import os
import pytest
from pathlib import Path

from visiomanager import create_app, db
from visiomanager.models import DataManager

from tests import DUMMY_ENV


@pytest.fixture()  # check for different scopes for speedup - scope='function' 'module'
def test_client(mocker):
    mocker.patch.dict(os.environ, DUMMY_ENV)  # stub env variables
    app = create_app('config.TestingConfig')

    # Create a test client using the Flask application configured for testing
    with app.test_client() as test_client:
        # Establish an application context
        with app.app_context():

            input_dir = Path("tests/data/db_test_data").resolve()
            print (f'Get DB init files from input_dir : {input_dir}')
            assert os.path.exists(input_dir)

            # Initializing database with data files
            userfile = input_dir / 'user_data.json'
            requestfile = input_dir / 'request_data.json'
            instancefile = input_dir / 'instance_data.json'

            data_manager = DataManager()
            # populate user
            if userfile.is_file():
                data_manager.init_users(userfile)
            else:
                pass

            # populate requests
            if requestfile.is_file():
                data_manager.init_requests(requestfile)
            else:
                pass

            # populate instances
            if instancefile.is_file():
                data_manager.init_instances(instancefile)
            else:
                pass

            yield test_client  # this is where the testing happens!

            db.session.remove()  # looks like db.session.close() would work as well
            db.drop_all()


def test_login(test_client):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/api/signin' signin REST API is called (POST)
    THEN check that the response is valid depending on the parameters
    """

    headers = {"Content-Type": "application/json"}

    data = {
        "password":"superdsi",
        "username":"superdsi"
    }
    response = test_client.post(
        '/api/signin',
        data=json.dumps(data),
        headers=headers
    )
    assert response.status_code == 200
    assert b"success" in response.data

    data = {
        "password":"superdsi",
        "username":"wrongname"
    }
    response = test_client.post(
        '/api/signin',
        data=json.dumps(data),
        headers=headers
    )
    assert response.status_code == 200
    assert b"error" in response.data
    assert b"Not authorized" in response.data

    data = {
        "password":"wrongpwd",
        "username":"superdsi"
    }
    response = test_client.post(
        '/api/signin',
        data=json.dumps(data),
        headers=headers
    )
    assert response.status_code == 200
    assert b"error" in response.data
    assert b"Not authorized" in response.data

    data = {
        "wrong": "param"
    }
    response = test_client.post(
        '/api/signin',
        data=json.dumps(data),
        headers=headers
    )
    assert response.status_code == 400


