# test/test_user_routes.py
import subprocess
from datetime import datetime, timezone, timedelta

import pytest

from visiomanager.utils import check_script, execute_script
from visiomanager.utils import get_short_time, get_elapsed_time
from visiomanager.utils import utc_to_local, local_to_utc, set_utc, set_local, parse_datetime_utc, parse_datetime_tz

def test_check_script():
    assert check_script('create_server.sh')
    assert not check_script('dowhateveryouwant.sh')

def test_execute_script(mocker):
    with pytest.raises(RuntimeError):
        execute_script('dowwhateveryouwant')
    process = subprocess.Popen(
        ['echo', '-n', 'fake'],
        stdout=subprocess.PIPE)
    mocker.patch('subprocess.Popen', return_value=process)
    assert execute_script('create_server.sh') == "fake"

def test_timestamps():
    # when converted to timestamps,
    epoch_ts = int(datetime.now().timestamp())
    epoch_ts_utc = int(datetime.now(timezone.utc).timestamp())
    assert epoch_ts_utc>=epoch_ts
    # difference should be much lower than 1 sec
    assert (epoch_ts_utc-epoch_ts)<1

def test_timezones():
    # need to set explicitely timezone to paris. Could be tested elsewhere
    # or in Docker, which often is set to UTC
    # UTC universal, all year
    # UK winter/summer: UTC and BST
    # paris winter/summer : CET and CEST - Europe/Paris adjust to summer/winter auto

    date_utc = "2020-12-02 17:51:50.156796+00:00"
    date_paris = "2020-12-02 17:51:50.156796+01:00"

    dt_paris = parse_datetime_tz(date_paris)
    dt_utc = parse_datetime_tz(date_utc)
    assert dt_utc.timestamp() != dt_paris.timestamp()

    # assigns timezone to utc instead of paris, keep same time
    # should then be equal to previous utc datetime
    dt_utc_1 = set_utc(dt_paris)
    assert dt_utc == dt_utc_1

    # equi to set_local, ensure Paris/winter
    dt_paris_1 = dt_utc.replace(tzinfo=timezone(timedelta(hours=1),'CET'))
    assert dt_paris == dt_paris_1
    assert dt_paris.timestamp() == dt_paris_1.timestamp()

    dt_utc_2 = utc_to_local(dt_utc)
    assert dt_utc.timestamp() == dt_utc_2.timestamp()
    assert dt_utc.timestamp() != dt_paris_1.timestamp()



@pytest.fixture(params=[
    'space',
    'te',
    'te2',
    'invalid1',
    'invalid2',
    'invalid3'])
def eval_short_time(request):
    date_str_space = "2020-12-02 17:51:50.156796+00:00"
    date_str_te = "2020-12-02T17:51:50.156796+00:00"
    date_str_te2 = "2021-03-21 08:57:00+01:00"
    date_str_invalid1 = "2020-12-02T17:51:50DJEKLSJSLZ"
    date_str_invalid2 = ""
    date_str_invalid3 = None
    ref_val = ''
    res = ''
    if request.param == 'space':
        res = get_short_time(date_str_space)
        ref_val = '02/12/2020 17:51'
    if request.param == 'te':
        res = get_short_time(date_str_te)
        ref_val = '02/12/2020 17:51'
    if request.param == 'te2':
        res = get_short_time(date_str_te2)
        ref_val = '21/03/2021 08:57'
    if request.param == 'invalid1':
        res = get_short_time(date_str_invalid1)
    if request.param == 'invalid2':
        res = get_short_time(date_str_invalid2)
    if request.param == 'invalid3':
        res = get_short_time(date_str_invalid3)

    return res, ref_val


def test_get_short_time(eval_short_time):
    output = eval_short_time[0]
    ref = eval_short_time[1]
    assert output == ref


@pytest.fixture(params=[
    'now',
    'old_space',
    'old_t',
    'invalid1',
    'invalid2',
    'invalid3'])
def eval_get_elapsed_time(request):
    now = str(datetime.now(timezone.utc))
    old_space = "2010-12-02 17:51:50.156796+00:00"
    old_t = "2010-12-02T17:51:50.156796+00:00"
    date_str_invalid1 = "2010-12-02T17:51:50DJEKLSJSLZ"
    date_str_invalid2 = ""
    date_str_invalid3 = None
    ref_val = ''
    out_val = ''

    if request.param == 'now':
        out_val = get_elapsed_time(str(now))
        ref_val = '0m'
    if request.param == 'old_space':
        out_val = len(get_elapsed_time(old_space))
        ref_val = 9
    if request.param == 'old_t':
        out_val = len(get_elapsed_time(old_t))
        ref_val = 9
    if request.param == 'invalid1':
        out_val = get_elapsed_time(date_str_invalid1)
    if request.param == 'invalid2':
        out_val = get_elapsed_time(date_str_invalid2)
    if request.param == 'invalid3':
        out_val = get_elapsed_time(date_str_invalid3)
    return out_val, ref_val, request


def test_get_elapsed_time(eval_get_elapsed_time):
    output = eval_get_elapsed_time[0]
    ref = eval_get_elapsed_time[1]
    request = eval_get_elapsed_time[2]

    if request.param == 'old_space' or request.param == 'old_t':
        assert output >= ref
    else:
        assert output == ref
