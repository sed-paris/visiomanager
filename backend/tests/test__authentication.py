# test/test_user_routes.py

import pytest

import visiomanager.authentication
from visiomanager.models import User
from visiomanager.authentication import AuthManager, SessionManager
from visiomanager.authentication import ROLE_ADMIN, ROLE_USER


@pytest.fixture
def admin_with_password():
    return User(
        name="name1",
        login="login1",
        email="email@1.org",
        password_hash="password1",
        role=ROLE_ADMIN
    )


@pytest.fixture
def user_without_password():
    return User(
        name="name2",
        login="login2",
        email="email@2.org",
        role=ROLE_USER
    )

@pytest.fixture(params=[
    ROLE_ADMIN,
    ROLE_USER,
    'no_user'
])
def auth_manager_init(mocker, request, admin_with_password, user_without_password):
    auth_manager = AuthManager()
    mock_session = dict()
    mocker.patch.object(visiomanager.authentication, 'session', mock_session)

    if request.param == ROLE_ADMIN:
        auth_manager.set_session_user(admin_with_password)
    if request.param == ROLE_USER:
        auth_manager.set_session_user(user_without_password)

    return auth_manager, request


def test_SessionManager(mocker, admin_with_password):
    mock_session = dict()
    mocker.patch.object(visiomanager.authentication, 'session', mock_session)
    session_mgr = SessionManager()

    # Test del empty session does not raise error
    session_mgr.del_session()

    # Test set user with password
    session_mgr.set_user(admin_with_password)
    user = session_mgr.get_user()
    assert user.name == "name1"
    assert user.login == "login1"
    assert user.email == "email@1.org"
    assert user.role == "admin"
    assert user.password_hash is None

    # Test del session
    session_mgr.del_session()
    assert session_mgr.get_user() is None


def test_is_user_authenticated(auth_manager_init):
    auth_manager = auth_manager_init[0]
    request = auth_manager_init[1]

    if request.param == ROLE_ADMIN or request.param == ROLE_USER:
        assert auth_manager.is_user_authenticated()
    if request.param == "no_user":
        assert auth_manager.is_user_authenticated() is False


def test_is_user_authorized(auth_manager_init):
    auth_manager = auth_manager_init[0]
    request = auth_manager_init[1]

    if request.param == "no_user":
        assert auth_manager.is_user_authorized(ROLE_USER) is False
        assert auth_manager.is_user_authorized(ROLE_ADMIN) is False
    if request.param == ROLE_ADMIN:
        assert auth_manager.is_user_authorized(ROLE_ADMIN)
        assert auth_manager.is_user_authorized(ROLE_USER) is False


def test_gen_password():
    auth_manager = AuthManager()
    pwd = auth_manager.gen_password()
    assert len(pwd) == 16


def test_session_methods(auth_manager_init, user_without_password):
    auth_manager = auth_manager_init[0]
    request = auth_manager_init[1]

    if request.param == ROLE_ADMIN:
        # test setting user when already set
        auth_manager.set_session_user(user_without_password)
        assert auth_manager.get_session_user().login != user_without_password.login

        # test setting user after deleting session
        auth_manager.del_session()
        auth_manager.set_session_user(user_without_password)
        assert auth_manager.get_session_user().login == user_without_password.login
