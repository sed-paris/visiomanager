# tests/__init__.py

from pathlib import Path
STATIC_DATA = (Path(__file__).parent / 'static/data').resolve()

DUMMY_ENV = {
    'SCW_PROJECT_ID': "scw-project-id",
    'SCW_BBB_ZONE': "bbb-zone-1",

    'VM_ID_FILE': "id_fake",

    "VM_SENDER_ACCOUNT_PWD": "password",
    "VM_SENDER_ACCOUNT_EMAIL": "sender@domain.fr",
    "VM_SENDER_ACCOUNT_SMTP": "smtp.domain.fr",
    "VM_ADMIN_EMAIL": "admin@domain.fr",

    "DB_SALT_B64": 'JDJiJDEyJDlHOXJJbG12c1ZqcFY5OGgxejlZSy4='
}
