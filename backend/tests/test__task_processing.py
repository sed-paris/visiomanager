import os
import pytest
import datetime

from visiomanager.models import Task, DataManager, TaskState, TaskAction
from visiomanager import create_app, db

from tests import DUMMY_ENV


@pytest.fixture
def app(mocker):
    mocker.patch.dict(os.environ, DUMMY_ENV)  # Stub env variables
    app = create_app('config.TestingConfig')
    with app.app_context():
        # alternative pattern to app.app_context().push()
        # all commands indented under 'with' are run in the app context
        db.create_all()

        # similar to unittests setup/teardown, everything that comes before return/yield is setup code,
        # everything that comes after yield is teardown code.
        yield app

        db.session.remove()  # looks like db.session.close() would work as well
        db.drop_all()


@pytest.fixture(params=[
    'task_dataset_ok'
    ])
def add_task_data():
    now = datetime.datetime.now()
    now_ts = int(now.timestamp())

    tmp_issued = now - datetime.timedelta(minutes=1000)

    tmp_due = now_ts - 1000
    task1 = Task(
        issued=tmp_issued,
        due=tmp_due,
        status=TaskState.COMPLETED,
        action=TaskAction.EMAIL
        )
    tmp_due = now_ts - 100
    task2 = Task(
        issued=tmp_issued,
        due=tmp_due,
        status=TaskState.COMPLETED,
        action=TaskAction.EMAIL
        )
    tmp_due = now_ts - 10
    task3 = Task(
        issued=tmp_issued,
        due=tmp_due,
        status=TaskState.PENDING,
        action=TaskAction.EMAIL
        )
    tmp_due = now_ts + 10
    task4 = Task(
        issued=tmp_issued,
        due=tmp_due,
        status=TaskState.PENDING,
        action=TaskAction.EMAIL
        )
    tmp_due = now_ts + 100
    task5 = Task(
        issued=tmp_issued,
        due=tmp_due,
        status=TaskState.PENDING,
        action=TaskAction.EMAIL
        )

    tasks = [task1, task2, task3, task4, task5]
    
    return tasks

def test_tasks_processing(app, add_task_data):

    data_manager = DataManager()
    data_manager.add_commit(add_task_data[0])
    data_manager.add_commit(add_task_data[1])
    data_manager.add_commit(add_task_data[2])
    data_manager.add_commit(add_task_data[3])
    data_manager.add_commit(add_task_data[4])

    tasks1 = data_manager.get_pending_tasks()
    assert len(tasks1) == 3

    tasks2 = data_manager.get_upcoming_pending_tasks()
    assert len(tasks2) == 2

    tasks3 = data_manager.get_due_pending_tasks()
    assert len(tasks3) == 1
