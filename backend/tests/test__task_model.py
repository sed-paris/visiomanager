import json
import os
import pytest
import datetime

from visiomanager.instances import RequestParameters
from visiomanager.models import Task, TaskState, TaskAction, Request
from visiomanager.emails import EmailManager
from visiomanager import create_app, db
from visiomanager import task_creator

from tests import DUMMY_ENV


@pytest.fixture
def app(mocker):
    mocker.patch.dict(os.environ, DUMMY_ENV)  # Stub env variables
    app = create_app('config.TestingConfig')
    with app.app_context():
        # alternative pattern to app.app_context().push()
        # all commands indented under 'with' are run in the app context
        db.create_all()

        # similar to unittests setup/teardown, everything that comes before return/yield is setup code,
        # everything that comes after yield is teardown code.
        yield app

        db.session.remove()  # looks like db.session.close() would work as well
        db.drop_all()

@pytest.fixture()
def init_request(app):
    start_time = datetime.datetime.now(datetime.timezone.utc) +\
                 datetime.timedelta(hours=24)
    duration = 30

    content = dict(
        name="toto",
        email="toto@domain.fr",
        login="toto",
        server_type="DEV",
        duration=str(duration),
        is_now=str(False),
        start_time=start_time.strftime('%Y-%m-%d %H:%M:%S.%f%z')
    )

    user_request = RequestParameters(content, content['login'])

    server = dict(
        url_server="server@domain.fr",
        id="instance_id"
    )
    instance_request = Request(
        id="request1",
        instance_id=server['id']
    )

    return user_request, server, instance_request


def test_plan_visio(app,init_request):
    user_request = init_request[0]
    due_ts = int(user_request.start_time.timestamp())
    task_creator.plan_visio(due_ts, user_request)

    task1 = db.session.query(Task).one()
    assert task1 is not None
    assert task1.due == due_ts
    assert task1.status == TaskState.PENDING
    assert task1.action == TaskAction.LAUNCH


def test_plan_warn_termination(app, init_request):
    user_request = init_request[0]
    server = init_request[1]
    due_ts = int(user_request.start_time.timestamp())
    task_creator.plan_warn_termination(
        due_time=due_ts,
        dest="dest",
        server=server,
        emails_config=EmailManager(app).emails
    )

    task1 = db.session.query(Task).one()
    assert task1 is not None
    assert task1.due == due_ts
    assert task1.status == TaskState.PENDING
    assert task1.action == TaskAction.EMAIL

    task1_extras = json.loads(task1.extra)
    assert task1_extras.get('hostname') == server['url_server']
    assert task1_extras.get('server_id') == server['id']
    assert task1_extras.get('dest') == 'dest'


def test_delay_warn_termination(app, init_request):
    user_request = init_request[0]
    server = init_request[1]
    instance_request = init_request[2]
    due_ts = int(user_request.start_time.timestamp())
    task_creator.plan_warn_termination(
        due_time=due_ts,
        dest="dest",
        server=server,
        emails_config=EmailManager(app).emails
    )
    initial_task = db.session.query(Task).one()
    initial_task_due = initial_task.due
    delay_minutes = 30
    task_creator.delay_warn_termination(instance_request, delay_minutes)

    delayed_task = db.session.query(Task).one()
    assert delayed_task.due - (initial_task_due + delay_minutes*60) == 0
