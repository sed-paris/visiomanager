import os
import pytest

from visiomanager.models import User, Instance, Request, DataManager
from visiomanager import create_app, db

from tests import DUMMY_ENV

@pytest.fixture
def app(mocker):
    mocker.patch.dict(os.environ, DUMMY_ENV)  # Stub env variables
    app = create_app('config.TestingConfig')
    with app.app_context():
        # alternative pattern to app.app_context().push()
        # all commands indented under 'with' are run in the app context
        db.create_all()

        # similar to unittests setup/teardown, everything that comes before return/yield is setup code,
        # everything that comes after yield is teardown code.
        yield app

        db.session.remove()  # looks like db.session.close() would work as well
        db.drop_all()

@pytest.fixture
def user_test1():
    '''Returns a Wallet instance with a zero balance'''
    return User(
        name="name1",
        login="login1",
        email="email@1.org",
        password_hash="password1",
        role="USER"
    )

@pytest.fixture
def user_test1_dup():
    '''Returns a Wallet instance with a zero balance'''
    return User(
        name="name1dup",
        login="login1",
        email="email@1.org",
        password_hash="password1",
        role="USER"
    )

@pytest.fixture
def request_test1():
    '''Returns a Wallet instance with a zero balance'''
    return Request(
        id="request_id_1",
        owner="login1",
        status="issued",
        issued="2020-12-02T17:51:50.156796+00:00",
        server_type="TYPE_1",
        start_time="2020-12-02 17:51:50.156796+00:00",
        end_time="2020-12-02 17:51:50.156796+00:00",
        bbb_login="bbblogin",
        bbb_email="bbb@email.org",
        bbb_name="bbbname",
        url="visio2.drearch.org",
        instance_id="instance_id_1",
    )

@pytest.fixture
def instance_test1():
    '''Returns a Wallet instance with a zero balance'''
    return Instance(
        id="instance_id_1",
        name="instance_name_1",
        ip="10.0.0.1",
        url="http://test.url.org"
    )

@pytest.fixture(params=[
    'commit_user',
    'commit_request',
    'commit_instance',
    'find_request_by_owner',
    'get_request',
    'find_user_by_login',
    'remove_commit',
    'commit_user_error'
    ])
def data_manager(request, user_test1, user_test1_dup, request_test1, instance_test1):
    data_manager = DataManager()
    ref_val = ''
    out_val = ''
    user_ok = data_manager.add_commit(user_test1)
    request_ok = data_manager.add_commit(request_test1)
    instance_ok = data_manager.add_commit(instance_test1)
    if request.param == 'commit_user':
       out_val = user_ok
       ref_val = True
    if request.param == 'commit_request':
       out_val = request_ok
       ref_val = True
    if request.param == 'commit_instance':
       out_val = instance_ok
       ref_val = True
    if request.param == 'find_request_by_owner':
       requests = data_manager.find_requests_by_owner(user_test1.login)        
       out_val = len(requests)
       ref_val = 1
    if request.param == 'get_request':
       requests = data_manager.get_requests()
       out_val = len(requests)
       ref_val = 1 
    if request.param == 'find_user_by_login':
       user = data_manager.find_user_by_login(user_test1.login)
       out_val = user.name
       ref_val = user_test1.name 
    if request.param == 'remove_commit':
       data_manager.del_commit(request_test1) 
       requests = data_manager.get_requests()
       out_val = len(requests)
       ref_val = 0 
    if request.param == 'commit_user_error':
       out_val = data_manager.add_commit(user_test1_dup)
       ref_val = False
    return out_val, ref_val

def test_data_manager_simple(app, data_manager):

    out_val = data_manager[0]
    ref_val = data_manager[1]
    
    assert out_val == ref_val

