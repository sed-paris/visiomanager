from flask import jsonify, request
from flask import Blueprint

import logging

from flasgger import swag_from
from flask_cors import cross_origin

from .instances import RequestParameters
from .models import DataManager

from .authentication import AuthManager, ROLE_ADMIN
from .task_creator import plan_visio, delay_warn_termination

from datetime import datetime, timedelta
from pathlib import Path

from .utils import get_short_time, get_elapsed_time, get_duration, parse_datetime_tz, utc_to_local

from . import scaleway_manager, email_manager

simple_page = Blueprint('simple_page', __name__)
monitoring = Blueprint('monitoring', __name__)

auth_manager = AuthManager()
data_manager = DataManager()

logger = logging.getLogger(__name__)


# ============================ REST API methods =========================================

@simple_page.route('/api/get_tasks', methods=['GET'])
@swag_from(Path('visiomanager/openapi/get_tasks.yml').resolve())
def get_tasks():
    try:
        logger.info("Processing get_tasks call")

        # Verify authentication
        if not auth_manager.is_user_authenticated():
            logger.error("Processing get_tasks not authenticated")
            return jsonify({"status": "error", "message": "Need to be authenticated"})

        # Display all requests if admin role
        if auth_manager.is_user_authorized(ROLE_ADMIN):
            logger.debug("Processing get get_tasks call as admin")
            result = data_manager.get_pending_launch_tasks_list()
            return jsonify({"status": "success", "tasks": result})

        # Display user requests otherwise
        user_login = auth_manager.get_session_user().login
        result = data_manager.get_pending_launch_tasks_list(user_login)
        return jsonify({"status": "success", "tasks": result})
    except Exception:
        logger.error("Exception while processing call", exc_info=True)
        return jsonify({"status": "error", "message": "invalid request (json)"})


@simple_page.route('/api/delete_task', methods=['POST'])
@swag_from(Path('visiomanager/openapi/delete_task.yml').resolve())
def delete_task():
    try:
        logger.info("Processing delete_task call")

        content = request.get_json(silent=True)
        task_id = content["taskid"]

        # Verify authentication
        if not auth_manager.is_user_authenticated():
            logger.error("Processing delete_task not authenticated")
            return jsonify({"status": "error", "message": "Need to be authenticated"})

        # Display all requests if admin role
        if auth_manager.is_user_authorized(ROLE_ADMIN):
            logger.debug("Processing get delete_task call as admin")
            result = data_manager.delete_task(task_id)
            return jsonify({"status": "success", "result": result})
        else:
            user_login = auth_manager.get_session_user().login
            result = data_manager.delete_task(task_id, user_login)
            return jsonify({"status": "success", "tasks": result})
    except Exception:
        logger.error("Exception while processing call", exc_info=True)
        return jsonify({"status": "error", "message": "invalid request (json)"})


@simple_page.route('/api/get_requests', methods=['GET'])
@swag_from(Path('visiomanager/openapi/get_requests.yml').resolve())
def get_requests():
    try:
        logger.info("Processing get_requests call")

        # Verify authentication
        if not auth_manager.is_user_authenticated():
            logger.error("Processing get_requests not authenticated")
            return jsonify({"status": "error", "message": "Need to be authenticated"})

        # Display all requests if admin role
        if auth_manager.is_user_authorized(ROLE_ADMIN):
            logger.debug("Processing get request call as admin")
            result = data_manager.get_request_list()
            return jsonify({"status": "success", "requests": result})

        # Display user requests otherwise
        user_login = auth_manager.get_session_user().login
        result = data_manager.get_request_list(user_login)
        return jsonify({"status": "success", "requests": result})
    except Exception:
        logger.error("Exception while processing call", exc_info=True)
        return jsonify({"status": "error", "message": "invalid request (json)"})


@simple_page.route('/api/launch_visio', methods=['POST'])
@swag_from(Path('visiomanager/openapi/launch_visio.yml').resolve(),
           validation=True)
def launch_visio():
    try:
        logger.info("Processing launch_visio call")

        content = request.get_json(silent=True)

        # Verify user authentication
        if not auth_manager.is_user_authenticated():
            logger.error("Processing get_requests not authenticated")
            return jsonify({"status": "error", "message": "need to be authenticated"})

        request_user = auth_manager.get_session_user()
        user_login = request_user.login
        logger.debug(f"Request user is {user_login}")

        new_visio = RequestParameters(content, user_login)
        try:
            new_visio.check()
        except Exception as error:
            logger.error("Invalid visio parameters", exc_info=True)
            return jsonify({"status": "error", "message": error})

        # launch the visio 13 minutes in advance (10 for link in advance, 3 for preparation)
        due = new_visio.start_time - timedelta(minutes=13)
        due_ts = int(due.timestamp())
        plan_visio(due_ts, new_visio)

        # Reduce the number of emails when relevant
        if not new_visio.is_now:
            email_manager.send_visio_request_email(new_visio.email,
                                                   new_visio.name,
                                                   new_visio.server_type,
                                                   new_visio.start_time,
                                                   new_visio.duration)

        return jsonify({"status": "success"})
    except Exception:
        logger.error("Exception while processing launch visio call",
                     exc_info=True)
        return jsonify({"status": "error", "message": "invalid request (json)"})


@simple_page.route('/api/delete_all_visio', methods=['GET'])
@swag_from(Path('visiomanager/openapi/delete_all_visio.yml').resolve())
def delete_all_visio():
    try:
        logger.info("Processing delete_all_visio call")

        # Verify user authentication
        if not auth_manager.is_user_authenticated():
            logger.error(f"Need to be authenticated for {request}")
            return jsonify({"status": "error", "message": "need to be authenticated"})
        # Verify if admin role
        if not auth_manager.is_user_authorized(ROLE_ADMIN):
            logger.error(f"Need to be authorized for {request}")
            return jsonify({"status": "error", "message": "need to be authorized"})

        # TODO the code below seems unwarranted
        # now = datetime.now()
        # now_ts = int(now.timestamp())
        # tmp_issued = now - timedelta(minutes=1000)
        # tmp_due = now_ts + 10
        # task4 = Task(
        #    issued=tmp_issued,
        #    due=tmp_due,
        #    status=TaskState.PENDING,
        #    action=TaskAction.EMAIL
        #)
        #data_manager.add_commit(task4)

        servers = scaleway_manager.get_servers()
        for server in servers:
            res = scaleway_manager.delete_server(server["id"])

        return jsonify({"status": "success", "message": "Instances have been deleted"})
    except Exception as inst:
        logger.error("Exception while processing delete_all_visio call",
                     exc_info=True)
        return jsonify({"status": "error", "message": "invalid request (json)"})


@simple_page.route('/api/delete_single_visio', methods=['POST'])
@swag_from(Path('visiomanager/openapi/delete_single_visio.yml').resolve(),
           validation=True)
def delete_single_visio():
    try:
        logger.info("Processing delete_single_visio call")

        content = request.get_json(silent=True)
        instance_name = content["name"]

        # Verify user authentication
        if not auth_manager.is_user_authenticated():
            logger.error("Need to be authenticated")
            return jsonify({"status": "error", "message": "need to be authenticated"})

       # Verify if user is authorized
        user_login = auth_manager.get_session_user().login
        instance_request = data_manager.get_instance_request(instance_name)
        if instance_request.owner != user_login:
            logger.error(f"Need to be authorized for {request}")
            return jsonify({"status": "error", "message": "need to be authorized"})

        servers = scaleway_manager.get_servers()

        for server in servers:
            if server["name"] == instance_name:
                res = scaleway_manager.delete_server(server["id"])
                return jsonify({"status": "success", "message": "Instances have been deleted"})

        return jsonify({"status": "error", "message": "not authorized"})
    except Exception:
        logger.error("Exception while processing delete_single_visio call",
                     exc_info=True)

        return jsonify({"status": "error", "message": "invalid request (json)"})


@simple_page.route('/api/add_time_visio', methods=['POST'])
@swag_from(Path('visiomanager/openapi/add_time_visio.yml').resolve(),
           validation=True)
def add_time_visio():
    try:
        logger.info("Processing add_time_visio call")

        content = request.get_json(silent=True)
        instance_name = content["name"]

        # Verify user authentication
        if not auth_manager.is_user_authenticated():
            logger.error("Need to be authenticated")
            return jsonify({"status": "error", "message": "need to be authenticated"})

        # Verify if user is authorized
        user_login = auth_manager.get_session_user().login
        instance_request = data_manager.get_instance_request(instance_name)
        if instance_request.owner != user_login:
            logger.error(f"Need to be authorized for {request}")
            return jsonify({"status": "error", "message": "need to be authorized"})

        # TODO: use scaleway api to verify if instance is still active
        servers = scaleway_manager.get_servers()

        for server in servers:
            if server["name"] == instance_name:
                delay_min = 30
                res = delay_warn_termination(instance_request, delay_min)
                if res:
                    instance_request.extend_time(delay_min)
                    return jsonify({
                        "status": "success",
                        "message": "Instance time has been extended"})
                break
        return jsonify({"status": "error", "message": "not authorized"})
    except Exception:
        logger.error("Exception while processing add_time_visio call",
                     exc_info=True)
        return jsonify({"status": "error", "message": "invalid request (json)"})



@simple_page.route('/api/check_visios', methods=['GET'])
@swag_from(Path('visiomanager/openapi/check_visios.yml').resolve())
def check_visios():
    try:
        logger.info("Processing check_visios call")

        if not auth_manager.is_user_authenticated():
            logger.error("Need to be authenticated")
            return jsonify({"status": "error", "message": "need to be authenticated"})

        # get the user associated with the request (logged in)
        current_user = auth_manager.get_session_user()

        servers = scaleway_manager.get_servers()

        ongoing_visios = []
        for server in servers:
            # empty data for now
            tmp_visio = {
            }

            # get the URL of the server
            url_server = scaleway_manager.get_url_server(server["public_ip"]["address"])
            # get the stats of the server
            stats_server = scaleway_manager.get_stats_server(server["public_ip"]["address"])
            # get instance's request info
            instance_request = data_manager.get_instance_request(server["name"])
            if (instance_request == None):
                # no request instance associated to this server, this is an issue (rogue server ?)
                # continue server list, this will be checked by the periodic cleanup task
                continue
            else:
                tmp_visio["duration"] = get_duration(instance_request.start_time,
                                                     instance_request.end_time)

            # add public info
            tmp_visio["name"] = server["name"]
            tmp_visio["server_type"] = server["commercial_type"]

            # need to convert the incoming dt in UTC to the local (Paris) time
            start_time_server = parse_datetime_tz(server["creation_date"])
            date_time_obj_cet = utc_to_local(start_time_server)
            start_time_paris = get_short_time(str(date_time_obj_cet))

            end_time_server = parse_datetime_tz(instance_request.end_time)
            end_time_paris = get_short_time(str(utc_to_local(end_time_server)))

            tmp_visio["created"] = start_time_paris
            tmp_visio["end"] = end_time_paris
            tmp_visio["elapsed"] = get_elapsed_time(server["creation_date"])

            tmp_visio["zone"] = server["zone"]

            # may provide detailed/short status
            str1 = server["state"]
            str2 = server["state_detail"]
            tmp_visio["server_state"] = f'{str1}'

            # add info if user.login (request's owner) is the owner's of the instance
            if current_user is not None and \
                    current_user.login == instance_request.owner:
                logger.info(f"User {current_user.login} can view this instance {url_server}")
                tmp_visio["owner"] = instance_request.owner
                tmp_visio["ts_recv"] = instance_request.issued
                tmp_visio["ts_start"] = instance_request.start_time
                tmp_visio["ts_stop"] = instance_request.end_time
                tmp_visio["state"] = instance_request.status
                #
                tmp_visio["url"] = url_server
                tmp_visio["monit"] = stats_server

            # add info if admin
            if current_user is not None and current_user.role == ROLE_ADMIN:
                logger.info(f"Admin user can view this instance {url_server}")
                tmp_visio["owner"] = instance_request.owner
                tmp_visio["ts_recv"] = instance_request.issued
                tmp_visio["ts_start"] = instance_request.start_time
                tmp_visio["ts_stop"] = instance_request.end_time
                tmp_visio["state"] = instance_request.status
                #
                tmp_visio["url"] = url_server
                tmp_visio["ip"] = ""  # server["public_ip"]["address"]
                tmp_visio["monit"] = stats_server

            ongoing_visios.append(tmp_visio)

        return jsonify({"status": "success", "servers": ongoing_visios})
    except Exception as inst:
        logger.error("Exception while processing check_visio call",
                     exc_info=True)
        return jsonify({"status": "error", "message": "invalid request (json)"})


@simple_page.route('/api/check_resources', methods=['GET'])
@swag_from(Path('visiomanager/openapi/check_resources.yml').resolve())
def check_resources():
    logger.info('check_resources not implemented')
    return jsonify({'status': 'error: not implemented'})


@simple_page.route('/api/signin', methods=['POST'])
@cross_origin(supports_credentials=True)
@swag_from(Path('visiomanager/openapi/signin.yml').resolve(),
           validation=True)
def sign_in():
    try:
        logger.info("Processing sign in call")
        content = request.get_json(silent=True)

        # Standard submit form
        user_name = content['username']
        password = content['password']

        user = data_manager.find_user_by_login(user_name)

        if user is not None and user.verify_password(password):
            auth_manager.set_session_user(user)
            return jsonify({"status": "success",
                            "message": "Login successful"})

        logger.error(f"User not authorized")
        return jsonify({"status": "error", "message": "Not authorized"})

    except Exception as inst:
        logger.error("Exception while processing sign in call", exc_info=True)

        # can be bad parsing, but also unknown user
        return jsonify({"status": "error", "message": "Not authorized"})

@simple_page.route('/api/logout', methods=['GET'])
@swag_from(Path('visiomanager/openapi/logout.yml').resolve())
def logout():
    logger.info("Logging out")
    auth_manager.del_session()
    return jsonify({"status": "success"})

@simple_page.route('/api/get_user_info', methods=['GET'])
@cross_origin()
@swag_from(Path('visiomanager/openapi/get_user_info.yml').resolve())
def get_user_info():
    logger.info("Get user info")

    user = auth_manager.get_session_user()
    if user is not None:
        user_name = user.name
        user_email = user.email
        user_role = user.role

        return jsonify({"status": "success",
                        "role": user_role,
                        "name": user_name,
                        "email": user_email})

    return jsonify({"status": "error", "message": "No user authenticated"})

# --------------- #
# Monitoring apis #
# --------------- #

@monitoring.route('/health', methods=['GET'])
def is_awake():
    logger.info('Check if backend server is still running')
    return "Backend server is still alive", 200

@monitoring.route('/scw_health', methods=['GET'])
def is_scw_available():
    logger.info('Check if scw is accessible')
    servers = scaleway_manager.get_servers()
    if servers is not None:
        return "Scaleway is available", 200
    else:
        return "Scaleway is not available", 500
