from datetime import timezone, timedelta, datetime
import json

from .models import Task, DataManager, TaskState, TaskAction
from .utils import parse_datetime_tz

data_manager = DataManager()


def plan_warn_termination(due_time, dest, server, emails_config):
    now = datetime.now(timezone.utc)
    emails = emails_config

    template_values = {
        'hostname': server['url_server'],
        'name': "name"
    }

    subject = emails["termination"]["subject"]
    text_str = "\n".join(emails["termination"]["text"])
    html_str = "\n".join(emails["termination"]["html"])

    msg_text = text_str.format(**template_values)
    msg_html = html_str.format(**template_values)

    extra = dict({
        "hostname": server['url_server'],
        "server_id": server['id'],
        "dest": dest,
        "subject": subject,
        "text": msg_text,
        "html": msg_html
    })
    new_task = Task(
        issued=now,
        due=due_time,
        status=TaskState.PENDING,
        action=TaskAction.EMAIL,
        extra=json.dumps(extra)
    )
    data_manager.add_commit(new_task)


#     subject = emails["termination2"]["subject"]
#     text_str = "\n".join(emails["termination2"]["text"])
#     html_str = "\n".join(emails["termination2"]["html"])
#
#     msg_text = text_str.format(**template_values)
#     msg_html = html_str.format(**template_values)
#
#     extra = dict({
#         "dest": dest,
#         "subject": subject,
#         "text": msg_text,
#         "html":  msg_html
#     })
#     new_task = Task(
#         issued=now,
#         due=due_time,
#         status=TaskState.PENDING,
#         action=TaskAction.EMAIL,
#         extra=json.dumps(extra)
#     )
#     data_manager.add_commit(new_task)

def plan_visio(due_time, request):
    now = datetime.now(timezone.utc)
    extra = dict({
        "name": request.name,
        "email": request.email,
        "login": request.login,
        "server_type": request.server_type,
        "duration": request.duration,
        "start_time": request.start_time.strftime('%Y-%m-%d %H:%M:%S.%f%z'),
        "end_time": request.end_time.strftime('%Y-%m-%d %H:%M:%S.%f%z'),
        "request_user": request.request_user
    })
    new_task = Task(
        issued=now,
        due=due_time,
        status=TaskState.PENDING,
        action=TaskAction.LAUNCH,
        extra=json.dumps(extra)
    )
    data_manager.add_commit(new_task)


def delay_warn_termination(instance_request, delay_min):

    task = data_manager.get_email_task(instance_request)
    if task is not None:
        task.status = TaskState.PENDING
        task.due = task.due + delay_min*60
        data_manager.add_commit(task)
        return True
    return False


def cancel_email_termination(instance_request):
    task = data_manager.get_email_task(instance_request)
    if task is not None:
        task.status = TaskState.CANCELLED
        data_manager.add_commit(task)
        return True
    return False
