import threading
import time
import logging
import json

from datetime import datetime, timezone

from sqlalchemy.exc import OperationalError

from .models import DataManager, User, TaskAction, Task, TaskState
from .utils import parse_datetime_tz, parse_datetime_utc, parse_datetime_tz
from .instances import InstanceManager

from . import scaleway_manager, email_manager, ScalewayCloudManager

logger = logging.getLogger(__name__)

data_manager = DataManager()

mutex = threading.Lock()

KEEP_ROGUE_PREFIX="keep-"

class Scheduler(object):
    """ SchedulerThread class for handling periodc tasks in the backgrund

    The run() method will be started and it will run in the background
    until the application exits (daemon mode).
    """
    last_run = 0

    app = None
    email_manager = None
    scaleway_manager = None

    def __init__(self, interval, alert_interval, app):
        """ Constructor

        :type interval: int
        :param interval: Check interval, in seconds
        :param alert_interval: Minimum interval between two email alerts, in s
        """
        self.interval = interval

        self.email_manager = email_manager
        self.scaleway_manager = scaleway_manager
        self.alert_interval = alert_interval

        thread = threading.Thread(target=self.run, args=(app,))
        thread.daemon = True
        thread.start()

        self.monitor_thread = threading.Timer(
            self.interval,
            function=self.monitor,
            args=(app,))
        self.monitor_thread.daemon = True
        self.monitor_thread.start()

    def run(self, appcontext):
        """ Method that runs forever """
        with appcontext.app_context():
            while True:
                try:
                    logger.info("Scheduler loop: stats servers")
                    self.process_stats_task()

                    logger.info("Scheduler loop: cleanup scaleway")
                    self.process_scw_cleanup()

                    logger.info("Scheduler loop: process pending tasks")
                    self.process_pending_tasks()

                except ValueError as err:
                    logger.info("Exception while processing main scheduler loop")
                    pass

                time.sleep(self.interval)

    def monitor(self, app):
        try:
            logger.info(f"Monitor loop")

            with app.app_context():
                alert = self.process_scw_alert()

                if alert is False:
                    interval = self.interval
                else:
                    interval = self.alert_interval

                # Create next thread
                self.monitor_thread = threading.Timer(
                    interval=interval,
                    function=self.monitor,
                    args=(app,))
                self.monitor_thread.daemon = True
                self.monitor_thread.start()

        except ValueError as err:
            logger.info("Exception while processing monitor loop")
            pass

    def process_pending_tasks(self):
        try:
            logger.info("Scheduler loop: process pending tasks")
            # TODO add/check mutex
            tasks = None
            mutex.acquire()
            try:
                tasks = data_manager.get_due_pending_tasks()
                for task in tasks:
                    task.status = TaskState.PROCESSING
                    data_manager.add_commit(task)
            except Exception as e:
                logger.error("unable to update tasks in mutex")
            finally:
                mutex.release()

            for task in tasks:
                logger.info(f'Scheduler loop: process task {task}')
                if task.action == TaskAction.EMAIL:
                    self.process_email_task(task)
                if task.action == TaskAction.LAUNCH:
                    self.process_launch_task(task)
                pass

        except TypeError:
            logger.warning("TypeError while processing process_pending_tasks")
            pass
        except OperationalError:
            logger.warning("OperationalError while processing process_pending_tasks")
            pass

    def process_stats_task(self):
        try:
            servers = self.scaleway_manager.get_servers()
            for server in servers:
                pass
        except TypeError:
            logger.warning("Exception while processing process_stats_task")
            pass

    def process_scw_alert(self):
        try:
            servers = self.scaleway_manager.get_servers()
            if len(servers) >= self.scaleway_manager.threshold:
                email_manager.send_alert_capacity(
                        self.scaleway_manager.threshold,
                        len(servers))
                return True
            return False
        except TypeError:
            logger.warning("Exception while processing process_scw_alert")
            return False

    # checks for running servers, and finds the corresponding request
    # to dertmine if it should be terminated
    def process_scw_cleanup(self):
        try:
            servers = self.scaleway_manager.get_servers()
            for server in servers:
                # only consider started servers, not starting/stopping
                if server["state"] == "running":
                    # get instance's request info
                    instance_request = data_manager.get_instance_request(server["name"])
                    if instance_request is None:
                        # a rogue server ? terminate immediately
                        if server["name"].startswith(KEEP_ROGUE_PREFIX):
                            logger.warning("Found a rogue server instance in SCW, but with keep prefix")
                            # TODO should send email only a few tiles (every hour ?)
                            # self.email_manager.send_keep_rogue(server["id"], server["name"])
                        else:
                            logger.warning("Found a rogue server instance in SCW, terminate immediately")
                            logger.warning(instance_request)
                            res = self.scaleway_manager.delete_server(server["id"])
                            self.email_manager.send_terminating_rogue(server["id"])
                    else:
                        now = datetime.now(timezone.utc)
                        now_ts = int(now.timestamp())
                        due_time = parse_datetime_tz(instance_request.end_time)
                        due_time_ts = int(due_time.timestamp())
                        if due_time_ts <= now_ts:
                            res = self.scaleway_manager.delete_server(server["id"])
                            self.email_manager.send_terminated_instance(server["id"],
                                                                        instance_request.bbb_email,
                                                                        instance_request.bbb_name,
                                                                        instance_request.url)

                    #
                    # get the URL of the server
                    # url_server = scaleway_manager.get_url_server(server["public_ip"]["address"])
                    # get the stats of the server
                    # stats_server = scaleway_manager.get_stats_server(server["public_ip"]["address"])
        except TypeError:
            logger.warning("TypeError while processing process_scw_cleanup")
            pass
        except OperationalError:
            logger.warning("OperationalError while processing process_pending_tasks")
            pass

    def process_email_task(self, task):
        try:
            extras = json.loads(task.extra)

            server_id = str(extras["server_id"])
            server_name = str(extras["hostname"])
            if ScalewayCloudManager.get_server_state(server_id) is None:
                logger.info(f"Cancel email warn termination for {server_id}")
                data_manager.delete_task(task_id=task.id)
                return
            dest = str(extras["dest"])
            subj = str(extras["subject"])
            html_msg = str(extras["html"])
            txt_msg = str(extras["text"])

            self.email_manager.send_email_html(
                dest,
                subj,
                html_msg,
                txt_msg
            )

            task.status = TaskState.COMPLETED
            data_manager.add_commit(task)
        except Exception as error:
            logger.warning("Exception while processing email task")
            task.status = TaskState.FAILED
            data_manager.add_commit(task)

    def process_launch_task(self, task):

        extras = json.loads(task.extra)
        try:
            instance_manager = InstanceManager(extras)
            instance_manager.prepare_visio(self.scaleway_manager, self.email_manager)
            instance_manager.record(extras['request_user'])
            instance_manager.finalize_visio(self.email_manager)

            task.status = TaskState.COMPLETED
            data_manager.add_commit(task)
        except:
            logger.warning("Exception while processing visio launch task")
            task.status = TaskState.FAILED
            data_manager.add_commit(task)

            self.email_manager.send_maximum_capacity(extras["email"], extras["name"])
