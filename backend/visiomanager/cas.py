from flask import redirect, request, current_app, make_response, session, jsonify
from flask import Blueprint

import logging

from cas import CASClient

from .routes import auth_manager
from .models import User
from .authentication import ROLE_USER

cas_page = Blueprint('cas_page', __name__)

logger = logging.getLogger(__name__)


@cas_page.route('/api/login')
def cas_login():
    cas_client = CASClient(
        version=3,
        service_url=f"{current_app.config['WEB_URL']}/api/login?",
        server_url=current_app.config['CAS_URL']
    )
    login_web_url = f"{current_app.config['WEB_URL']}/#/login"
    home_web_url = f"{current_app.config['WEB_URL']}/#/visios"

    if auth_manager.get_session_user() is not None:
        # Already logged in - redirect and frontend pulls user info
        response = redirect(login_web_url)
        return response

    ticket = request.args.get('ticket')
    if not ticket:
        # No ticket, the request come from end user, send to CAS login
        cas_login_url = cas_client.get_login_url()
        logger.debug('CAS login URL: %s', cas_login_url)
        response = redirect(cas_login_url)
        return response

    # There is a ticket, the request come from CAS as callback.
    # need call `verify_ticket()` to validate ticket and get user profile.
    logger.debug('ticket: %s', ticket)

    user, attributes, pgtiou = cas_client.verify_ticket(ticket)

    logger.debug(
        'CAS verify ticket response: user: %s, attributes: %s, pgtiou: %s',
        user, attributes, pgtiou)

    if not user:
        # verify failed, return to login page
        logger.error('Failed to verify ticket')
        response = redirect(login_web_url)
        return response
    else:
        # Login successfully, add cas_user in current session
        cas_user = User(name=user,
                        login=user,
                        email=attributes['mail'],
                        role=ROLE_USER)

        auth_manager.set_session_user(cas_user)

        response = redirect(home_web_url)
        return response
