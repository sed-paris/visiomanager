from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS

from flasgger import Swagger

from dotenv import load_dotenv
import logging
from logging.handlers import TimedRotatingFileHandler
import os
from pathlib import Path
import sys
import uuid

from .utils import SSHKeyFile
from .emails import EmailManager
from .cloud_scaleway import ScalewayCloudManager

# define variables before importing routes
db = SQLAlchemy()
email_manager = EmailManager()
scaleway_manager = ScalewayCloudManager()


from .routes import simple_page, monitoring
from .cas import cas_page

ENV_FILE = Path('./backend.env').resolve()
load_dotenv(ENV_FILE, override=True)

# Required environment variables list
ENV_VAR_LIST = [
        'SCW_PROJECT_ID',
        'SCW_BBB_ZONE',
        'VM_ID_FILE',
        'VM_SENDER_ACCOUNT_EMAIL',
        'VM_SENDER_ACCOUNT_PWD',
        'VM_SENDER_ACCOUNT_SMTP',
        'VM_ADMIN_EMAIL',
        'DB_SALT_B64'
    ]


def create_app(configuration=""):
    """Construct the core application."""
    app = Flask(__name__, instance_relative_config=False)
    # allows cookies and credentials to be submitte dacross domains
    CORS(app)

    # secret key so sign session info
    # if there is a need to maintain session across restart, would need to be persistent acrooss restarts
    app.secret_key = str(uuid.uuid4())

    # should use the proper configuration (dev/prod/staging/testing/
    app.config.from_object(configuration)


    # Configure logging
    os.makedirs('logs', exist_ok=True)
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)
    fh = TimedRotatingFileHandler(filename='logs/runtime.log', when='D',
                                  interval=1, backupCount=7, encoding='utf-8',
                                  delay=False)
    fh.setLevel(logging.DEBUG)
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(name)s '
                                  '(%(lineno)d): %(message)s')
    fh.setFormatter(formatter)
    ch.setFormatter(formatter)

    logger.addHandler(fh)
    logger.addHandler(ch)

    logger.info(f'Config for application: {configuration}')

    if configuration != 'config.TestingConfig':
        # setup flasgger/swagger API docs
        app.config["SWAGGER"]["headers"].append(
            ('Access-Control-Allow-Origin', app.config["CORS_ORIGINS"]))

        swagger = Swagger(app, config=app.config["SWAGGER"])
        # Check if backend environment variables are set (otherwise exits)
        if not ENV_FILE.is_file():
            logger.info(f"{ENV_FILE} is not used as not existing")

        check_env_ok = True
        for env_var in ENV_VAR_LIST:
            if os.getenv(env_var) is None:
                logger.error(f"{env_var} is not set")
                check_env_ok = False
        if not check_env_ok:
            sys.exit("Required environment variables are not all set. Verify "
                     "logs for more info.")

        if not SSHKeyFile.exists():
            sys.exit(f"Value set for VM_ID_FILE does not exists")

    # Initialize app
    logger.info(f"Database path: {app.config['SQLALCHEMY_DATABASE_URI']}")
    db_path = Path(app.config['DB_PATH']).resolve()
    db_path.parent.mkdir(parents=True, exist_ok=True)
    db.init_app(app)

    with app.app_context():

        app.register_blueprint(simple_page)
        app.register_blueprint(monitoring)
        if 'CAS_URL' in app.config:
            app.register_blueprint(cas_page)
        db.create_all()
        scaleway_manager.init_app(app)
        email_manager.init_app(app)

        return app
