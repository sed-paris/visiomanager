#!/bin/bash

# shell script to perm delete the first user in greenligh (the default acocunt on the image)

# if running on the instance, launch the rails console
# docker exec -it greenlight-v2 bundle exec rails c
# and then get the first user and delete it
# u = User.first
# u.destroy(true)

# getting the first user gives the member varialbes of the class
# u = User.first
# => #<User id: 1, room_id: 1, provider: "greenlight", uid: "gl-erbmmzehytgf", name: "SuperDsi", username: nil, email: "superdsi@ouaibe.fr", social_uid: nil, image: nil, password_digest: "$2a$12$PKLZuDc1.DZ6NF.0b0zld.NMqNcnyVo9UksgkM89Fc3...", accepted_terms: true, created_at: "2020-11-25 08:20:15", updated_at: "2020-11-25 08:20:16", email_verified: true, language: "default", reset_digest: nil, reset_sent_at: nil, activation_digest: nil, activated_at: nil, deleted: false, role_id: 2>

# only interactive docker exec, as commands given in the pipe
echo 'u = User.first;u.destroy(true)' | docker exec -i greenlight-v2 bundle exec rails c
exit 0
