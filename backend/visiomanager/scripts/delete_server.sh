#!/usr/bin/env bash

# check required env var
if [ -z "$SCW_S_KEY" ]
then
  echo >&2 "SCW_S_KEY not defined"
  exit 1
fi

if [ -z "$SCW_BBB_ZONE" ]
then
  echo >&2 "SCW_BBB_ZONE not defined"
  exit 1
fi

if [ "$#" -ne 1 ]
then
  echo >&2 "usage: $0 server_id"
  exit 1
fi

# delete just archives the instance, and keeps the volume, use action instead
# curl -X DELETE \
#  -H "X-Auth-Token: $SCW_S_KEY" \
#   https://api.scaleway.com/instance/v1/zones/$SCW_BBB_ZONE/servers/$1

curl -X DELETE \
  -H "X-Auth-Token: $SCW_S_KEY" \
  -H "Content-Type: application/json" \
  -d "{\"action\":\"terminate\"}" \
   https://api.scaleway.com/instance/v1/zones/$SCW_BBB_ZONE/servers/$1/action

# read console output to check that the operation succeeded, or error (e.g., unknown resource)

