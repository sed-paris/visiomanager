#!/bin/bash

if [ "$#" -ne 2 ]
then
  echo >&2 "usage: $0 target_host identity_file"
  echo >&2 "   - example: $0 myvisio.bbb.org id_rsa_bbb"
  echo >&2 "   - identity_file : private key located in .ssh folder and copied by cloud provider on server instance"
  exit 1
fi

## launch local script to add account on remote host
ssh -i $HOME/.ssh/$2 "root@$1" "bash -s" < ./visiomanager/scripts/srv_delete_account.sh


