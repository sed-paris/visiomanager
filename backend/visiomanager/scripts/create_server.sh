#!/usr/bin/env bash

# check required env var
if [ -z "$SCW_S_KEY" ]
then
  echo >&2 "SCW_S_KEY not defined"
  exit 1
fi

if [ -z "$SCW_BBB_ZONE" ]
then
  echo >&2 "SCW_BBB_ZONE not defined"
  exit 1
fi

if [ -z "$SCW_PROJECT_ID" ]
then
  echo >&2 "$SCW_PROJECT_ID not defined"
  exit 1
fi

if [ "$#" -ne 4 ]
then
  echo >&2 "usage: $0 server_name server_type image_id public_ip_id"
  exit 1
fi

curl -X POST \
  -H "X-Auth-Token: $SCW_S_KEY" \
  -H "Content-Type: application/json" \
  -d "{\"name\":\"$1\",\"image\":\"$3\",\"commercial_type\":\"$2\",\"public_ip\":\"$4\",\"volumes\":{},\"tags\":[\"temporary\",\"BBB\"],\"project\":\"$SCW_PROJECT_ID\"}" \
  https://api.scaleway.com/instance/v1/zones/$SCW_BBB_ZONE/servers

# read console output to check that the operation succeeded, or error (e.g., invalid image)
