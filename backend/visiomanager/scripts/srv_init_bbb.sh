#!/bin/bash

# shell script to launch again the bbb init script in order to
# - set the url, even with only 1 image
# - set a unique secret

# quick patch to avoid patched bbb install of certificates (webroot)
# only for the first image, do letsencrypt - otherwise use copied cert
# if not doing cert, need to set flag when calling bbb patched install

do_cert=$2
#if [ $do_cert == "false" ]
#then
#  echo "######################## disable cert setup when launching init"
#  sed -i "s/patched.sh -v xenial-22/patched.sh -d -v xenial-22/g" /root/bbb/bbb_config/scripts/install_bbb.sh
#fi

cd /root/bbb/bbb_config/scripts/
sed -i "s/https:\/\/ifconfig.me/http:\/\/ifconfig.me/" *.sh

# launch bbb install patched script
yes n | /root/bbb/bbb_config/scripts/install_bbb.sh "$1" "salons-paris@inria.fr" &>/dev/null

# not needed as reinstall done systematically
# add line in rc.local, beware of \n support in gnu/bsd sed : https://stackoverflow.com/questions/46082397/insert-newline-n-using-sed
#if [ "$do_cert" == "true" ]
#then
#  # TODO : rewrite
#  # issue with exit0 appearing twice, delete last line, add instead
#  #nl=$'\n'
#  #sed -i 's/exit 0/\/root\/bbb\/bbb_config\/scripts\/change_internal_ip.sh'"\\${nl}"'exit 0/' /etc/rc.local
#  sed -i '$ d' /etc/rc.local
#  echo "/root/bbb/bbb_config/scripts/change_internal_ip.sh" >> /etc/rc.local
#  echo "exit 0" >> /etc/rc.local
#  systemctl enable rc-local.service
#fi

exit 0
