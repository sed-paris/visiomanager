#!/bin/bash

# shell script to add a user in greenligh, with some basic checks

# scaleway / bash tips
# assume the ssh key has been uploaded on the account
# ssh -i $HOME/.ssh/id_rsa_scw <instance_ip/hostname>
# prior to that, as instances are recreated with the same public ip/hostname, the .known_hosts file needs to be cleanup


regex_email="^(([A-Za-z0-9]+((\.|\-|\_|\+)?[A-Za-z0-9]?)*[A-Za-z0-9]+)|[A-Za-z0-9]+)@(([A-Za-z0-9]+)+((\.|\-|\_)?([A-Za-z0-9]+)+)*)+\.([A-Za-z]{2,})+$"
regex_alpha="^[a-zA-Z0-9_-]{6,20}$"

roles=['admin','user']

if [ "$#" -ne 4 ]
then
  echo >&2 "usage: $0 name email password role"
  exit 1
fi

[[ $1 =~ $regex_alpha ]] && ret="VALID" || ret="INVALID"
echo name $ret
if [ "$ret" == "INVALID" ]
then
  exit 1
fi

[[ $2 =~ $regex_email ]] && ret="VALID" || ret="INVALID"
echo email $ret
if [ "$ret" == "INVALID" ]
then
  exit 1
fi

[[ $3 =~ $regex_alpha ]] && ret="VALID" || ret="INVALID"
echo name $ret
if [ "$ret" == "INVALID" ]
then
  exit 1
fi

[[ ${roles[*]} =~ $4 ]] && ret="VALID" || ret="INVALID"
echo role $ret
if [ "$ret" == "INVALID" ]
then
  exit 1
fi

docker exec greenlight-v2 bundle exec rake user:create[$1,$2,$3,$4]
exit 0