#!/bin/bash

if [ "$#" -ne 4 ]
then
  echo >&2 "usage: $0 target_host identity_file target_host do_cert"
  echo >&2 "   - example: $0 myvisio.bbb.org id_rsa_bbb myvisio.bbb.org true"
  exit 1
fi

## launch local script to add account on remote host
ssh -i $HOME/.ssh/$2 "root@$1" "bash -s" < ./visiomanager/scripts/srv_init_bbb.sh "\"$3\" \"$4\""
