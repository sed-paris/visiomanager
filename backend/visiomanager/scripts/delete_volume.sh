#!/usr/bin/env bash

# check required env var
if [ -z "$SCW_S_KEY" ]
then
  echo >&2 "SCW_S_KEY not defined"
  exit 1
fi

if [ -z "$SCW_BBB_ZONE" ]
then
  echo >&2 "SCW_BBB_ZONE not defined"
  exit 1
fi

if [ "$#" -ne 1 ]
then
  echo >&2 "usage: $0 volume_id"
  exit 1
fi

curl -X DELETE \
  -H "X-Auth-Token: $SCW_S_KEY" \
   https://api.scaleway.com/instance/v1/zones/$SCW_BBB_ZONE/volumes/$1

# read console output to check that the operation succeeded, or error (e.g., unknown resource)

