#!/bin/bash

if [ "$#" -ne 6 ]
then
  echo >&2 "usage: $0 target_host identity_file name email password role"
  echo >&2 "   - example: $0 myvisio.bbb.org id_rsa_bbb NewAdminUser user&organisation.org EIFNJFN46 admin"
  echo >&2 "   - identity_file : private key located in .ssh folder and copied by cloud provider on server instance"
  echo >&2 "   - role : should be admin or user"
  echo >&2 "   - name, password should only contain alphanumeric characters plus - and _ and be between 6 and 20 characters"
  exit 1
fi

## launch local script to add account on remote host
ssh -i $HOME/.ssh/$2 "root@$1" "bash -s" < ./visiomanager/scripts/srv_add_account.sh "\"$3\" \"$4\" \"$5\" \"$6\""


