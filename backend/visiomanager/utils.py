import io
from datetime import datetime, timezone
from dateutil import tz
import logging
import os
from pathlib import Path
import subprocess

SCRIPTS = (Path(__file__).parent / 'scripts').resolve()
logger = logging.getLogger(__name__)


########################################
# Utilities for executing bash scripts #
########################################

class SSHKeyFile:
    filename = None

    @classmethod
    def get_name(cls):
        if cls.filename is None:
            cls.filename = os.getenv('VM_ID_FILE')
        return cls.filename

    @classmethod
    def exists(cls):
        path = Path(str(os.getenv('HOME'))) / '.ssh' / cls.get_name()
        if not path.exists():
            logger.error(f"{path} is not found")
            return False
        return True


def check_script(name):
    whitelist = [
        'clean_known_hosts.sh',
        'create_server.sh',
        'get_ips.sh',
        'delete_volume.sh',
        'terminate_server.sh',
        'get_servers.sh',
        'poweron_server.sh',
        'mgr_init_bbb.sh',
        'mgr_add_account.sh',
        'mgr_delete_account.sh'
    ]
    if name in whitelist:
        return True
    return False


def execute_script(name, *args):
    """
    Execute scaleway scripts stored in SCRIPTS directory
    :param name: name of the script
    :param args: arguments to pass to the script
    :return: the output of the script execution (None if it fails)
    """
    if not check_script(name):
        raise RuntimeError(f"Script {name} is not authorized to be executed")

    script_path = SCRIPTS / name
    cmd = [str(script_path)]
    for arg in args:
        cmd.append("{}".format(arg))
    env_vars = os.environ.copy()  # necessary if backend.env file is used

    process = subprocess.Popen(
        cmd,
        env=env_vars,
        shell=False,
        bufsize=-1,
        stdout=subprocess.PIPE
    )

    try:
        # wait for maximum of 10 minutes, kill subprocess if no return
        data, err = process.communicate(timeout=600)
    except subprocess.TimeoutExpired:
        logger.error(f"Executing {cmd} failed: process communicate timeout")
        process.kill()
        data, err = process.communicate()

    if process.returncode == 0:
        res = data.decode('utf-8')
        logger.debug(f"Executing {cmd}")  # {cmd} : {res}
        return res
    else:
        logger.error(f"Executing {cmd} failed: {err}")
        return None


###################################
# Utilities for date manipulation #
###################################

def utc_to_local(utc_dt: datetime) -> datetime:
    dst = tz.gettz('Europe/Paris')
    return utc_dt.astimezone(tz=datetime.utcnow().astimezone(dst).tzinfo)


def local_to_utc(local_dt: datetime) -> datetime:
    return local_dt.astimezone(tz=timezone.utc)


def set_utc(dt: datetime) -> datetime:
    return dt.replace(tzinfo=timezone.utc)


def set_local(dt: datetime) -> datetime:
    return dt.replace(tzinfo=datetime.utcnow().astimezone().tzinfo)


def parse_datetime_utc(str_time: str) -> datetime:
    formats = ['%Y-%m-%dT%H:%M:%S.%f', '%Y-%m-%dT%H:%M:%S.%f+00:00', '%Y-%m-%d %H:%M:%S.%f',
               '%Y-%m-%d %H:%M:%S.%f+00:00', '%Y-%m-%d %H:%M:%S+00:00', '%a, %d %b %Y %H:%M:%S GMT',
               '%a, %d %b %Y %H:%M:%S']
    for fmt in formats:
        try:
            dt = datetime.strptime(str_time, fmt)
            dt.replace(tzinfo=timezone.utc)
            return dt
        except (TypeError, ValueError):
            pass
    raise ValueError("No valid date format")


def parse_datetime_tz(str_time: str) -> datetime:
    formats = ['%Y-%m-%dT%H:%M:%S.%f%z', '%Y-%m-%dT%H:%M:%S%z', '%Y-%m-%d %H:%M:%S.%f%z',
               '%Y-%m-%d %H:%M:%S%z', '%a, %d %b %Y %H:%M:%S GMT']
    for fmt in formats:
        try:
            dt = datetime.strptime(str_time, fmt)
            return dt
        except (TypeError, ValueError):
            pass
    raise ValueError("No valid date format")

def get_duration_str(duration):
    duration_in_s = duration.total_seconds()
    days = divmod(duration_in_s, 86400)
    hours = divmod(days[1], 3600)
    minutes = divmod(hours[1], 60)

    if int(days[0]) == 0:
        res = f"{int(hours[0])}h {int(minutes[0])}m"
        if int(hours[0]) == 0:
            res = f"{int(minutes[0])}m"
    else:
        res = f"{int(days[0])}d {int(hours[0])}h {int(minutes[0])}m"

    return res

def get_duration(str_start: str, str_end: str) -> str:
    """
    Returns a simplified duration
    :param str_start: string like "2020-11-28T13:28:40.668089+00:00"
                             or "2020-11-28 13:28:40.668089+00:00"
    :param str_end: string like "2020-11-28T13:28:40.668089+00:00"
                             or "2020-11-28 13:28:40.668089+00:00"
    :return: string like 13:28 ('' otherwise)
    """
    logger.info(f"Get duration between {str_start} and {str_end}")

    try:
        date_time_obj1 = parse_datetime_tz(str_start)
        date_time_obj2 = parse_datetime_tz(str_end)
        duration = date_time_obj2 - date_time_obj1
        return get_duration_str(duration)

    except ValueError:
        try:
            date_time_obj1 = parse_datetime_utc(str_start)
            date_time_obj1b = utc_to_local(date_time_obj1)
            date_time_obj2 = parse_datetime_utc(str_end)
            date_time_obj2b = utc_to_local(date_time_obj2)
            duration = date_time_obj2b - date_time_obj1b
            return get_duration_str(duration)
        except ValueError:
            logger.error(f"{str_start} or {str_end} not a supported utc/tz time string")
            return ''


def get_short_time(str_time: str) -> str:
    """
    Returns a simplified date format
    :param str_time: string like "2020-11-28T13:28:40.668089+00:00"
                             or "2020-11-28 13:28:40.668089+00:00"
    :return: string like 28/11/2020 13:28 ('' otherwise)
    """
    logger.info(f"Get short time for {str_time}")

    try:
        date_time_obj1 = parse_datetime_tz(str_time)
        return date_time_obj1.strftime("%d/%m/%Y %H:%M")
    except ValueError:
        try:
            date_time_obj1 = parse_datetime_utc(str_time)
            date_time_obj2 = utc_to_local(date_time_obj1)
            return date_time_obj2.strftime("%d/%m/%Y %H:%M")
        except ValueError:
            logger.error(f"{str_time} not a supported utc/tz time string")
            return ''


def get_elapsed_time(str_time: str) -> str:
    """
    Returns elapsed time since started time
    :param str_time: string like "2020-11-28T13:28:40.668089+00:00"
                             or "2020-11-28 13:28:40.668089+00:00"
    :return:
    """
    logger.info(f"Get elapsed time since {str_time}")
    try:
        date_time_obj1 = parse_datetime_tz(str_time)
        date_time_obj1 = local_to_utc(date_time_obj1)
    except ValueError:
        try:
            date_time_obj1 = parse_datetime_utc(str_time)
        except ValueError:
            logger.error(f"{str_time} not a supported utc/tz time string")
            return ''

    date_time_obj2 = datetime.now(timezone.utc)
    elapsed_time = date_time_obj2 - date_time_obj1
    duration_in_s = elapsed_time.total_seconds()
    days = divmod(duration_in_s, 86400)
    hours = divmod(days[1], 3600)
    minutes = divmod(hours[1], 60)

    if int(days[0]) == 0:
        res = f"{int(hours[0])}h {int(minutes[0])}m"
        if int(hours[0]) == 0:
            res = f"{int(minutes[0])}m"
    else:
        res = f"{int(days[0])}d {int(hours[0])}h {int(minutes[0])}m"

    logger.debug(f"Return {res}")
    return res
