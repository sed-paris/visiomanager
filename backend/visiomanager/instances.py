import json
import logging
import sys
import time
import threading
from datetime import datetime, timezone, timedelta
import uuid
from . import utils

from .cloud_scaleway import ScalewayCloudManager

from .authentication import AuthManager
from .utils import SSHKeyFile
from .utils import parse_datetime_tz, set_utc
from .models import DataManager, Request, Instance

from .task_creator import plan_warn_termination

# roles defined on the BBB side
BBB_ROLE_ADMIN = "admin"
BBB_ROLE_USER = "user"

auth_manager = AuthManager()
data_manager = DataManager()

logger = logging.getLogger(__name__)

launcher_mutex = threading.Lock()

def task_visio_finalize(instance_manager, email_manager, host, password):
    try:
        loop_nb = 1
        status = None
        while status != "running" and loop_nb < 10:
            time.sleep(20)
            status = ScalewayCloudManager.get_server_state(instance_manager.new_server["id"])
            loop_nb = loop_nb+1



        # temporarily disable
        # TODO once images rebuild without the rc.local for all projects, enable again
        # TODO may still need to time after sshd to be sure all other daemons are up and running
        #  and have exchanged any required info
        # first loop and wait for the server to be up and running
        # loop_nb = 1
        # status = None
        # while status != "running" and loop_nb < 10:
        #    time.sleep(20)
        #    status = ScalewayCloudManager.get_server_state(instance_manager.new_server["id"])
        #    loop_nb = loop_nb+1
        # now wait for initial init of the services/daemons (sshd but also greenlight/bbb)
        # would need  separate check for sshd wtih empty script
        # time.sleep(60)
        # need to wait for sshd to be fine - should take less than a minue, otherwise error
        # loop_nb = 1
        # result = None
        # while result is None and loop_nb < 7:
        #    time.sleep(10)
        #    result = utils.execute_script('mgr_init_bbb.sh', host, instance_manager.keyfile, host, "false")

        time.sleep(180)

        try:
            launcher_mutex.acquire()
            utils.execute_script('clean_known_hosts.sh', host)
        except Exception as e:
            logger.error("unable to clean known hosts")
        finally:
            launcher_mutex.release()

        result = None
        try:
            result = utils.execute_script('mgr_init_bbb.sh', host,
                                          instance_manager.keyfile, host, "false")
        except Exception as e:
            logger.error("unable to execute_script mgr_init_bbb")

        if result is None:
            # script failed, send email
            logger.error(f"task_visio_finalize unable to connect to scaleway VM (sshd error ?)")
            email_manager.send_visio_failed_email(instance_manager.params["email"],
                                                  instance_manager.params["name"])
        else:
            # cotinue operations
            email_manager.send_visio_ready_email(instance_manager.params["email"],
                                                 instance_manager.params["name"],
                                                 host,
                                                 password)
            instance_manager.add_account(host, BBB_ROLE_ADMIN)
            instance_manager.delete_default_account(host)
    except Exception as err:
        logger.error(f"task_visio_finalize unable to complete launch:{err}")


class RequestParameters:
    def __init__(self, content, request_user):
        self.request_user = request_user
        self.name = content["name"]
        self.email = content["email"]
        self.login = content["login"]
        self.server_type = content["server_type"]
        self.duration = int(content["duration"])
        self.is_now = bool(content["is_now"])
        # assume 3 minutes for setup
        if self.is_now:
            self.start_time = datetime.now(timezone.utc)
        else:
            self.start_time = parse_datetime_tz(content["start_time"]) - timedelta(minutes=3)
            self.start_time = set_utc(self.start_time)
        # add the duration and the extra 3 minutes for the setup.
        self.end_time = self.start_time + timedelta(minutes=self.duration) + timedelta(minutes=3)

    def check_email(self):
        return self.email.lower().endswith("@inria.fr")

    def check_server_type(self):
        return self.server_type in ScalewayCloudManager.SERVER_TYPES.keys()

    def check(self):
        if not self.check_email():
            raise Exception(f"Invalid email - Contact the developer")
        if not self.check_server_type():
            raise Exception(f"Invalid server type {self.server_type}")
        if not self.is_now and self.start_time < datetime.now(timezone.utc):
            raise Exception(f"Invalid start time (past)")
        if not self.is_now and self.start_time >= self.end_time:
            raise Exception(f"Invalid end time")
        if self.duration <= 0:
            raise Exception(f"Invalid duration")
        return True


class InstanceManager:
    new_server = None

    def __init__(self, params):
        self.params = params
        self.password = auth_manager.gen_password()
        self.keyfile = SSHKeyFile.get_name()
        if not SSHKeyFile.exists():
            sys.exit(f"{SSHKeyFile.get_name()} does not exist")

    def prepare_visio(self, scaleway_manager, email_manager):
        logger.info("Prepare visio instance")
        servers = scaleway_manager.get_servers()
        if len(servers) >= scaleway_manager.limit:
            raise Exception(f"Limit number of VMs is reached")
        server_type = ScalewayCloudManager.SERVER_TYPES[self.params["server_type"]]
        self.free_ip = scaleway_manager.get_free_public_ip()
        if self.free_ip is None:
            # TODO send email about failure to start
            raise Exception(f"No available public ip")
        logger.info(f"Launch server {server_type}: {self.free_ip}")
        self.new_server = scaleway_manager.launch_server(server_type,
                                                         self.free_ip["id"],
                                                         self.free_ip["address"])
        if scaleway_manager.power_server(self.new_server["id"]) is None:
            # TODO send email about failure to start
            raise Exception("Impossible to power on the visio server")
        email_manager.send_visio_creation_email(self.params["email"], self.params["name"])

    def record(self, request_user):

        new_instance = Instance(
            id=self.new_server["id"],
            name=self.new_server["name"],
            ip=self.new_server["id"],
            url=self.new_server["url_server"]
        )
        data_manager.add_commit(new_instance)

        issue_time = datetime.now(timezone.utc)

        new_request: Request = Request(
            id=str(uuid.uuid4()),
            owner=request_user,
            status="issued",
            issued=issue_time.isoformat(),
            server_type=self.params["server_type"],
            url=self.new_server["url_server"],
            start_time=parse_datetime_tz(self.params["start_time"]).isoformat(),
            end_time=parse_datetime_tz(self.params["end_time"]).isoformat(),
            bbb_login=self.params["login"],
            bbb_email=self.params["email"],
            bbb_name=self.params["name"],
            bbb_pwd=self.password,
            instance_id=new_instance.id
        )
        data_manager.add_commit(new_request)

    def finalize_visio(self, email_manager):
        # launch thread for account creation and emailing
        thread = threading.Thread(target=task_visio_finalize,
                                  args=[self, email_manager, self.new_server["url_server"], self.password])
        thread.daemon = True  # Daemonize thread
        thread.start()

        try:
            # and plan task to warn requester when the visio is about to end
            due = datetime.strptime(self.params["end_time"], '%Y-%m-%d %H:%M:%S.%f%z') - timedelta(minutes=5)

            tmp_due = int(due.timestamp())
            plan_warn_termination(tmp_due, self.params["email"],
                                  self.new_server,
                                  emails_config=email_manager.emails)
        except Exception as err:
            logger.debug(f"finalize_visio unable to plan termination email")

    def add_account(self, host, role):
        try:
            result = utils.execute_script('mgr_add_account.sh', host,
                                         self.keyfile, self.params["name"],
                                         self.params["email"],
                                         self.password, role)
            if result is None:
                return False
            else:
                logger.warning(f"User account added to {host} : {result}")
                return True
        except (TypeError, RuntimeError, json.JSONDecodeError):
            logger.warning(f"Unable to add account to {host} : {result}")
            return False

    def delete_default_account(self, host):
        try:
            result = utils.execute_script('mgr_delete_account.sh', host,
                                          self.keyfile)
            if result is None:
                return False
            else:
                logger.warning(f"Account deleted to {host} : {result}")
                return True
        except (TypeError, RuntimeError, json.JSONDecodeError):
            logger.warning(
                f"Unable to delete default account to {host} : {result}")
            return False
