"""Data models."""
from .utils import get_short_time, utc_to_local
from . import db

import base64
import bcrypt
import json
import logging
import os
from datetime import datetime, timezone, timedelta

from enum import Enum

from sqlalchemy import String
from sqlalchemy.orm.exc import FlushError
from sqlalchemy.types import TypeDecorator, VARCHAR

from sqlalchemy.ext.mutable import Mutable

from .utils import parse_datetime_tz, set_local, set_utc

logger = logging.getLogger(__name__)


class DBSalt:
    DB_SALT = None

    @classmethod
    def get(cls):
        if cls.DB_SALT is None:
            logger.info("Using B64 encoded DB_SALT")
            b64encoded_db_salt = os.getenv('DB_SALT_B64')
            base64_bytes = b64encoded_db_salt.encode('ascii')
            cls.DB_SALT = base64.b64decode(base64_bytes)
        return cls.DB_SALT


class JSONEncodedDict(TypeDecorator):
    "Represents an immutable structure as a json-encoded string."

    impl = VARCHAR

    def process_bind_param(self, value, dialect):
        if value is not None:
            value = json.dumps(value)
        return value

    def process_result_value(self, value, dialect):
        if value is not None:
            value = json.loads(value)
        return value


class ParameterName(Enum):
    TITLE = "title"
    DESCRIPTION = "description"


class Parameter:
    name: String = ""
    value: String = ""
    category: String = ""

    # default constructor
    def __init__(self):
        self.name = ""
        self.value = ""
        self.category = ""

    def __init__(self, name: String, category: String, value: String):
        self.name = name
        self.value = category
        self.category = value


class MutableDict(Mutable, dict):
    @classmethod
    def coerce(cls, key, value):
        "Convert plain dictionaries to MutableDict."

        if not isinstance(value, MutableDict):
            if isinstance(value, dict):
                return MutableDict(value)

            # this call will raise ValueError
            return Mutable.coerce(key, value)
        else:
            return value

    def __setitem__(self, key, value):
        "Detect dictionary set events and emit change events."

        dict.__setitem__(self, key, value)
        self.changed()

    def __delitem__(self, key):
        "Detect dictionary del events and emit change events."

        dict.__delitem__(self, key)
        self.changed()

    def get_param(self, name):
        elem = self[name]
        if elem is None:
            return None
        else:
            res = Parameter(name, elem["category"], elem["value"])
            return res

    def exists(self, name):
        if name in self.keys():
            return True
        else:
            return False

    def del_param(self, name):
        del self[name]

    def add_param(self, param: Parameter):
        self[param.name] = {"name": param.name,
                            "category": param.category,
                            "value": param.value}


class TaskState(Enum):
    PENDING = 1
    COMPLETED = 2
    FAILED = 3
    PROCESSING = 4
    CANCELLED = 5


class TaskAction(Enum):
    NONE = 1
    EMAIL = 2
    LAUNCH = 3


class Task(db.Model):
    __tablename__ = 'tasks'

    epoch_ts = int(datetime.now().timestamp())

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    issued = db.Column(db.TIMESTAMP(timezone=False), nullable=False, default=datetime.now())
    due = db.Column(db.Integer, nullable=False, default=epoch_ts)
    status = db.Column(db.Enum(TaskState), default=TaskState.PENDING)
    action = db.Column(db.Enum(TaskAction), default=TaskAction.NONE)
    extra = db.Column(String, default="")

    def __repr__(self):
        return "<Task(due='%s', action='%s', status='%s')>" % (
            self.due, self.action, self.status)

    def set_completed(self):
        self.status = TaskState.COMPLETED

    def set_failed(self):
        self.status = TaskState.FAILED

    def is_pending(self):
        return isinstance(self.status, TaskState.PENDING)

    def is_failed(self):
        return isinstance(self.status, TaskState.FAILED)

    def is_completed(self):
        return isinstance(self.status, TaskState.COMPLETED)

    def is_action(self, action: TaskAction):
        return isinstance(self.action, action)


class Instance(db.Model):
    __tablename__ = 'instances'

    id = db.Column(String, primary_key=True, unique=True)
    name = db.Column(String, unique=True)
    ip = db.Column(String)
    url = db.Column(String)
    params = db.Column(MutableDict.as_mutable(JSONEncodedDict))

    def __repr__(self):
        return "<Instance(name='%s', ip='%s', url='%s')>" % (
            self.name, self.ip, self.url)


#
class User(db.Model):
    __tablename__ = 'users'

    login = db.Column(String, primary_key=True, unique=True)
    email = db.Column(String, unique=True)
    name = db.Column(String)
    password_hash = db.Column(db.String(128))
    role = db.Column(String)

    params = db.Column(MutableDict.as_mutable(JSONEncodedDict))

    def __repr__(self):
        return "<User(email='%s', login='%s')>" % (
            self.email, self.login)

    def to_dict(self):
        return {'login': self.login,
                'email': self.email,
                'name': self.name,
                'role': self.role}

    @property
    def password(self):
        raise AttributeError('password not readable')

    def init_hashed_password(self, password_hash):
        self.password_hash = password_hash

    @password.setter
    def password(self, password_plain):
        self.password_hash = bcrypt.hashpw(bytes(password_plain, "UTF-8"),
                                           DBSalt.get())

    def verify_password(self, password):
        tmp_hash = bcrypt.hashpw(bytes(password, "UTF-8"), DBSalt.get())
        return tmp_hash == self.password_hash


#
class Request(db.Model):
    __tablename__ = 'requests'

    id = db.Column(String, primary_key=True, unique=True)
    # user login of the creator (external key)
    owner = db.Column(String, default="")
    # issue time and status of the request
    status = db.Column(String, default="")
    issued = db.Column(String, default="")
    server_type = db.Column(String, default="")
    # requested start/end time
    start_time = db.Column(String, default="")
    end_time = db.Column(String, default="")
    # email, name, login and pwd of the BBB admin
    bbb_login = db.Column(String, default="")
    bbb_email = db.Column(String, default="")
    bbb_name = db.Column(String, default="")
    bbb_pwd = db.Column(String, default="")
    url = db.Column(String, default="")
    # instance id (unique)
    instance_id = db.Column(String, default="")
    params = db.Column(MutableDict.as_mutable(JSONEncodedDict))

    def __repr__(self):
        return "<Request(name='%s', bbb_email='%s')>" % (
            self.owner, self.bbb_email)

    def extend_time(self, minutes):
        due_time = parse_datetime_tz(self.end_time)
        new_due_time = due_time + timedelta(minutes=minutes)
        self.end_time = new_due_time.isoformat()
        db.session.commit()
#
#
#

class DataManager:

    def __init__(self):
        pass

    # Base methods for database access
    def terminate(self):
        logger.info("Close db")
        db.session.close()

    def add_commit(self, add_obj):
        logger.debug(f"Commit {add_obj} in db")
        try:
            db.session.add(add_obj)
            db.session.commit()
            return True
        except FlushError:
            logger.error(f"Commit failed for {add_obj}", exc_info=True)
            return False

    def del_commit(self, del_obj):
        logger.debug(f"Remove commit for {del_obj}")
        # TODO check potential cascading
        try:
            db.session.delete(del_obj)
            db.session.commit()
            return True
        except FlushError:
            logger.error(f"Removing commit failed for {del_obj}",
                         exc_info=True)
            return False

    # ----------------------------------- #
    # Methods for accessing Request table #
    # ----------------------------------- #

    def find_requests_by_owner(self, login_filter):
        logger.info(f"Find requests by owner {login_filter}")
        user_requests = db.session.query(Request).filter_by(
            owner=login_filter).all()
        logger.debug(f"Return {user_requests}")
        return user_requests

    def get_requests(self):
        logger.info(f"Get all requests in db")
        user_requests = db.session.query(Request).all()
        logger.debug(f"Return {user_requests}")
        return user_requests

    def get_request_list(self, owner=None):
        logger.info("Get requests list from db")
        res = []
        if owner is not None:
            db_requests = self.find_requests_by_owner(owner)
        else:
            db_requests = self.get_requests()
        for request in db_requests:
            # TODO refactoring of dates
            # issued here is string - time in utc with timezone
            local_issued = get_short_time(str(utc_to_local(parse_datetime_tz(request.issued))))
            # start_time/end_time are strings - time in utc with timezone
            local_start = get_short_time(str(utc_to_local(parse_datetime_tz(request.start_time))))
            local_end = get_short_time(str(utc_to_local(parse_datetime_tz(request.end_time))))

            res.append(
                {"id": request.id, "owner": request.owner,
                 "status": request.status,
                 "issued": local_issued,
                 "url": request.url,
                 "start_time": local_start,
                 "end_time": local_end,
                 "server_type": request.server_type})
        logger.debug(f"Return {res}")
        return res

    # -------------------------------- #
    # Methods for accessing User table #
    # -------------------------------- #

    def find_user_by_login(self, login_filter):
        logger.info(f"Find user by login {login_filter}")
        try:
            user = db.session.query(User).filter_by(login=login_filter).one()
            logger.debug(f"Return {user}")
            return user
        except Exception:
            logger.warning(f"User not found {login_filter}")
            return None

    # ------------------------------------ #
    # Methods for accessing Instance table #
    # ------------------------------------ #

    def get_instance_request(self, server_name):
        logger.info(f"Get request for instance {server_name}")
        try:
            instance = db.session.query(Instance).filter_by(name=server_name).one()
            request = db.session.query(
                Request).filter_by(instance_id=instance.id).one()
            logger.debug(f"Return {request}")
            return request
        except Exception:
            logger.warning(f"No corresponding request for {server_name}:")
            return None

    # --------------------------------- #
    # Methods for initializing database #
    # --------------------------------- #

    def init_instances(self, filename):
        logger.info(f"Init db with instances from {filename}")
        with open(filename) as db_file:
            default_instances = json.load(db_file)
            for instance in default_instances:
                tmp_instance = Instance(
                    id=instance["id"],
                    name=instance["name"],
                    ip=instance["ip"],
                    url=instance["url"]
                )
                self.add_commit(tmp_instance)

    def init_users(self, filename):
        logger.info(f"Init db with users from {filename}")
        with open(filename) as db_file:
            default_users = json.load(db_file)
            for user in default_users:
                password_plain = None
                if "password_plain" in user:
                    password_plain = user["password_plain"]
                tmp_user = User(
                    name=user["name"],
                    login=user["login"],
                    email=user["email"],
                    password=password_plain,
                    role=user["role"]
                )
                # if a hashed password is provided in the file, directly use it
                if "password_hash" in user:
                    if user["password_hash"] is not None:
                        tmp_user.init_hashed_password(user["password_hash"])
                self.add_commit(tmp_user)

    def init_requests(self, filename):
        logger.info(f"Init db with requests from {filename}")
        with open(filename) as db_file:
            default_requests = json.load(db_file)
            for request in default_requests:
                tmp_request = Request(
                    id=request["id"],
                    owner=request["owner"],
                    status=request["status"],
                    issued=request["issued"],
                    server_type=request["server_type"],
                    start_time=request["start_time"],
                    end_time=request["end_time"],
                    bbb_login=request["bbb_login"],
                    bbb_email=request["bbb_email"],
                    bbb_name=request["bbb_name"],
                    bbb_pwd=request["bbb_pwd"],
                    instance_id=request["instance_id"]
                )
                self.add_commit(tmp_request)

    # -------------------------------- #
    # Methods for accessing Task table #
    # -------------------------------- #

    def get_pending_tasks(self):
        logger.info("Get all pending tasks from db")
        tasks = db.session.query(Task).filter_by(status=TaskState.PENDING).all()
        logger.debug(f"- pending tasks: {tasks}")
        return tasks

    def get_upcoming_pending_tasks(self):
        now = datetime.now()
        now_ts = int(now.timestamp())

        logger.info("Get upcoming pending tasks from db")
        tasks = db.session.query(Task).filter(Task.status == TaskState.PENDING, Task.due > now_ts).all()
        logger.debug(f"- upcoming pending tasks: {tasks}")
        return tasks

    def get_due_pending_tasks(self):
        now = datetime.now(timezone.utc)
        now_ts = int(now.timestamp())

        logger.info("Get late pending tasks from db")
        tasks = db.session.query(Task).filter(Task.status == TaskState.PENDING, Task.due <= now_ts).all()
        logger.debug(f"- late pending tasks: {tasks}")
        return tasks

    def get_email_task(self, instance_request):
        email_tasks = db.session.query(Task).filter_by(
            action=TaskAction.EMAIL
        ).all()
        for task in email_tasks:
            extras = json.loads(task.extra)
            if 'server_id' in extras:
                if instance_request.instance_id == extras['server_id']:
                    return task

    def get_pending_launch_tasks_list(self, owner=None):
        logger.info("Get upcoming requests list from db")
        res = []

        db_tasks = self.get_pending_tasks()
        for task in db_tasks:
            if task.action is TaskAction.LAUNCH:
                extra = json.loads(task.extra)
                if owner is None or extra["name"] == owner:
                    # TODO refactoring of dates
                    # issued is datetime in utc but without tz info - need to set utc tz, convert to local, get short time
                    local_issued = get_short_time(str(utc_to_local(set_utc(task.issued))))
                    # start_time/end_time are strings - time in utc with timezone
                    local_start = get_short_time(str(utc_to_local(parse_datetime_tz(extra["start_time"]))))
                    local_end = get_short_time(str(utc_to_local(parse_datetime_tz(extra["end_time"]))))
                    res.append(
                        {"id": str(task.id),
                         "owner": extra["name"],
                         "status": "pending",
                         "issued": local_issued,
                         "start_time": local_start,
                         "end_time": local_end,
                         "server_type": extra["server_type"]})
        logger.debug(f"Return {res}")
        return res

    def delete_task(self, task_id, owner=None):
        logger.info(f'Delete task with id {task_id} for user {owner}')
        try:
            task = db.session.query(Task).filter(Task.id == task_id).one()
            extras = json.loads(task.extra)
            if owner is None or extras["name"] == owner:
                task.status = TaskState.COMPLETED
                extras["status_details"] = "CANCELLED"
                task.extra = json.dumps(extras)
                self.add_commit(task)
                return True
            return False
        except Exception as e:
            logger.error(e.args)
            logger.warning(f"Task not found")
            return False

