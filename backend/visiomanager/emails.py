import ssl
import datetime
import logging
import smtplib
import sys
import threading

from email.utils import formatdate
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import json
import os
from .utils import get_short_time, utc_to_local
logger = logging.getLogger(__name__)

SSL_PORT = 465
TLS_PORT = 587

email_mutex = threading.Lock()

class EmailManager:

    def __init__(self, app=None):
        self.sender = None
        self.alerts = None
        self.pwd = None
        self.smtp = None
        self.emails = None
        self.config = None
        if app is not None:
            self.init_app(app)

    def init_app(self, app):
        self.sender = os.getenv('VM_SENDER_ACCOUNT_EMAIL')
        self.alerts = os.getenv('VM_ADMIN_EMAIL')
        self.pwd = os.getenv('VM_SENDER_ACCOUNT_PWD')
        self.smtp = os.getenv('VM_SENDER_ACCOUNT_SMTP')
        self.config = app.config.get('FLASK_ENV')
        filename = app.config.get('EMAILS')
        if not filename.exists():
            sys.exit(f"Missing email configuration file at {filename}")
        with open(filename) as emails_file:
            self.emails = json.load(emails_file)
        logger.debug(f"email: {self.sender}, config: {filename}")

    def send_email_html(self, dest, subject, html, text):
        # Create a secure connection (using TLS)
        try:
            email_mutex.acquire()

            logger.debug(f"Sending email from {self.sender} to {dest} with subject: {subject}")
            # using TLS instead of SSL, no need for context but need explicit starttls/login
            # context = ssl.create_default_context()
            # server = smtplib.SMTP_SSL(self.smtp, 465, context=context)
            server = smtplib.SMTP(self.smtp, TLS_PORT)
            # server.set_debuglevel(1)
            server.starttls()
            server.ehlo()
            server.login(self.sender, self.pwd)
            server.ehlo()
        except:
            logger.error("Problem while connecting to the SMTP server cannot send email")
            email_mutex.release()
            return

        try:
            today = datetime.date.today()
            now = today.ctime()

            msg = MIMEMultipart('alternative')

            if self.config != 'production':
                subject = f"[{self.config}] " + subject

            msg['Subject'] = subject
            msg['From'] = self.sender
            msg['To'] = dest
            msg['Date'] = formatdate(localtime=True)

            # Record the MIME types of both parts - text/plain and text/html.
            part1 = MIMEText(text, 'plain', 'utf-8')
            # encode html part part2 = MIMEText(html, 'html', 'utf-8')
            encoded_html = html.encode()
            part2 = MIMEText(encoded_html, 'html', 'utf-8')

            # Attach parts into message container.
            # According to RFC 2046, the last part of a multipart message, in this case
            # the HTML message, is best and preferred.
            msg.attach(part1)
            # html part is disabled for now as Inria SMTP server filters when links/urls inside
            # msg.attach(part2)

            resp = server.sendmail(self.sender, dest, msg.as_string())
            # if dictionary empty, means email could not be sent (single dest)
            if bool(resp):
                logger.warning("Error when sending the email", exc_info=True)
        except:
            logger.warning("Error when sending the email", exc_info=True)
        finally:
            email_mutex.release()

        server.quit()

    # WARN: the text and html parts should be validated with the email sender
    # as sending "invalid" content will silently fail. For exemple, having a
    # url in the text part (e.g. https://visio1.drearch.online) fails. Any
    # text should be tested/validated.

    def prepare_parts(self, template, value):
        subject = self.emails[template]["subject"]
        text_str = "\n".join(self.emails[template]["text"])
        html_str = "\n".join(self.emails[template]["html"])
        msg_text = text_str.format(**value)
        msg_html = html_str.format(**value)
        return subject, msg_text, msg_html

    def send_visio_request_email(self, dest, name, server_type, start_time, duration):

        # need to convert the incoming dt in UTC to the local (Paris) time
        date_time_obj_cet = utc_to_local(start_time)
        start_time_paris = get_short_time(str(date_time_obj_cet))
        template_values = {
            'name': name,
            'server_type': server_type,
            'start_time': start_time_paris,
            'duration': duration
        }
        subject, msg_text, msg_html = self.prepare_parts("request", template_values)
        self.send_email_html(dest, subject, msg_html, msg_text)

    def send_visio_creation_email(self, dest, name):
        template_values = {
            'name': name
        }
        subject, msg_text, msg_html = self.prepare_parts("creation", template_values)
        self.send_email_html(dest, subject, msg_html, msg_text)

    def send_visio_ready_email(self, dest, name, visio_host, password):
        template_values = {
            'dest': dest,
            'name': name,
            'visio_host': visio_host,
            'password': password
        }
        subject, msg_text, msg_html = self.prepare_parts("ready", template_values)
        self.send_email_html(dest, subject, msg_html, msg_text)

    #     def send_terminating_server(self, dest, name, visio_host):
    #         template_values = {
    #             'name': name,
    #             'visio_host': visio_host
    #         }
    #         subject, msg_text, msg_html = self.prepare_parts("terminating", template_values)
    #         self.send_email_html(dest, subject, msg_html, msg_text)

    def send_terminated_instance(self, id, dest, name, visio_host):
        template_values = {
            'name': name,
            'visio_host': visio_host
        }
        subject, msg_text, msg_html = self.prepare_parts("terminated", template_values)
        self.send_email_html(dest, subject, msg_html, msg_text)

    def send_terminating_rogue(self, server_id):
        template_values = {
            'server_id': server_id
        }
        subject, msg_text, msg_html = self.prepare_parts("rogue", template_values)
        self.send_email_html(self.alerts, subject, msg_html, msg_text)

    def send_keep_rogue(self, server_id, server_name):
        template_values = {
            'server_id': server_id,
            'server_name': server_name
        }
        subject, msg_text, msg_html = self.prepare_parts("keep", template_values)
        self.send_email_html(self.alerts, subject, msg_html, msg_text)

    def send_maximum_capacity(self, dest, name):
        template_values = {
            'name': name
        }
        subject, msg_text, msg_html = self.prepare_parts("full", template_values)
        self.send_email_html(dest, subject, msg_html, msg_text)

    def send_visio_failed_email(self, dest, name):
        template_values = {
            'name': name
        }
        subject, msg_text, msg_html = self.prepare_parts("init_failed", template_values)
        self.send_email_html(dest, subject, msg_html, msg_text)

    def send_alert_capacity(self, threshold, instances_count):
        template_values = {
            'threshold': threshold,
            'count': instances_count
        }
        subject, msg_text, msg_html = self.prepare_parts("alert", template_values)
        self.send_email_html(self.alerts, subject, msg_html, msg_text)
