import logging
import secrets
import string

from flask import session

from .models import DataManager, User

logger = logging.getLogger(__name__)

ROLE_ITEM = "role"
ROLE_ADMIN = "admin"
ROLE_USER = "user"

USER_SESSION_KEY = "USER"

data_manager = DataManager()


# specific class to handle session information. Injected into AuthManager
# http session used to return the ticket ID from CAS signin across redirects
# allows to track/map client before and after the CAS signin
# use class and not directly session object to do unit testing of AuthManager (which has no flask/session enabled)
class SessionManager:

    def set_user(self, user):
        if session.get(USER_SESSION_KEY) is None:
            session[USER_SESSION_KEY] = user.to_dict()

    def get_user(self):
        if session.get(USER_SESSION_KEY) is not None:
            return User(**session[USER_SESSION_KEY])
        return None

    def del_session(self):
        if USER_SESSION_KEY in session:
            session.pop(USER_SESSION_KEY)


class AuthManager:
    session_mgr = None

    def __init__(self):
        self.session_mgr = SessionManager()

    def is_user_authenticated(self):
        logger.info("Verify if the user is authenticated")
        current_user = self.get_session_user()
        if current_user is not None:
            return True

        logger.debug(f"No user in current session")
        return False

    def is_user_authorized(self, expected_role):
        logger.info(f"Verify if the user has the {expected_role} role")
        current_user = self.get_session_user()
        if current_user is not None:
            role = current_user.role
            logger.debug(f"User role is {role}")
            if role == expected_role:
                return True
        return False

    def gen_password(self):
        pwd = ''.join(secrets.choice(string.ascii_letters + string.digits)
                      for k in range(16))
        return pwd

    # Methods for accessing current session
    def set_session_user(self, user):
        self.session_mgr.set_user(user)

    def get_session_user(self):
        return self.session_mgr.get_user()

    def del_session(self):
        self.session_mgr.del_session()
