import sys
import time
import json
import logging
import random

from . import utils

logger = logging.getLogger(__name__)


class ScalewayCloudManager:
    SERVER_TYPES = {
        "xs": "GP1-XS",
        "s": "GP1-S",
        "m": "GP1-M",
        "dev-l": "DEV1-L"
    }

    def __init__(self, app=None):
        self.domains = None
        self.images = None
        self.limit = None
        self.threshold = None
        if app is not None:
            self.init_app(app)

    def init_app(self, app):
        filepath = app.config.get('SCW_FILE_DEFS')
        if not filepath.exists():
            sys.exit(f"Missing scaleway file configutation at {filepath}")
        logger.info(f"Init ScalewayCloudManager with {filepath}")
        with open(filepath) as defs_file:
            defs = json.load(defs_file)
            self.domains = defs['dns']
            self.images = defs['images']
            self.limit = int(defs['instances']['limit'])
            self.threshold = int(defs['instances']['threshold'])

    def get_image_id(self, id_address):
        logger.info(f"Get image id from {id_address}")
        image_id = ""
        for ipdef in self.domains:
            if ipdef["address"] == id_address:
                for imagedef in self.images:
                    if ipdef["server"] == imagedef["server"]:
                        image_id = imagedef["id"]
        logger.debug(f"Return image id {image_id}")
        return image_id

    def get_url_server(self, id_address):
        logger.info(f"Get url server from {id_address}")
        url_server = ""
        for ipdef in self.domains:
            if ipdef["address"] == id_address:
                url_server = ipdef["server"]
        logger.debug(f"Return {url_server}")
        return url_server

    def get_free_public_ip(self):
        logger.info("Get a free public ip")
        all_public_ips = ScalewayCloudManager.get_ip_addresses()
        logger.debug(f"All public ips: {all_public_ips}")
        for ip in all_public_ips:
            if ip["server"] is None:
                for registered_ips in self.domains:
                    if registered_ips['address'] == ip['address']:
                        logger.debug(f"Return {ip}")
                        return ip

        logger.warning("No IP available : return None")
        return None

    def launch_server(self, server_type, ip_id, id_address):
        logger.info("Launch server on scaleway")
        seconds_since_epoch = time.time()
        server_name = f"BBB_{seconds_since_epoch}"
        url_server = self.get_url_server(id_address)
        image_id = self.get_image_id(id_address)

        logger.debug(f"Execute create_server.sh : {server_name}, "
                     f"{server_type}, {image_id}, {ip_id}")
        try:
            result = utils.execute_script('create_server.sh', server_name,
                                          server_type, image_id, ip_id)
            json_result = json.loads(result)
            server = json_result["server"]
            logger.debug(f"Script result (key server) : {server}")

            # created server info
            res = {"id": server["id"],
                   "server_type": server["commercial_type"],
                   "name": server["name"],
                   "public_ip": server["public_ip"]["address"],
                   "url_server": url_server, "server_name": server_name}
            logger.debug(f"Return {res}")
            return res

        except Exception as error:
            logger.error(f"Unable to create server {server_type} at {ip_id}",
                         exc_info=True)
            logger.debug("Return None")
            return None

    def get_stats_server(self, id_address):
        # TODO query each server for current load
        stats = {
            "cpu_load": random.randint(5, 95),
            "mem_load": random.randint(30, 95),
            "net_load": random.randint(10, 100)
        }
        return stats

    ########################################
    # STATIC METHODS FOR QUERYING SCALEWAY #
    ########################################

    @staticmethod
    def get_server_state(server_id):
        servers = ScalewayCloudManager.get_servers()
        try:
            for server in servers:
                if server["id"] == server_id:
                    return server["state"]
        except:
            return None

    @staticmethod
    def get_ip_addresses():
        logger.info("Get ip addresses")
        res = []
        try:
            result = utils.execute_script('get_ips.sh')
            json_result = json.loads(result)
            for ip in json_result["ips"]:
                res.append({"address": ip["address"],
                            "id": ip["id"], "server": ip["server"]})
        except (TypeError, json.JSONDecodeError, KeyError) as error:
            logger.error(f"Unable to get ip addresses: {result}",
                         exc_info=True)
        logger.debug(f"Return {res}")
        return res

    @staticmethod
    def delete_volume(volume_id):
        logger.info(f"Start deleting volume {volume_id}")
        try:
            result = utils.execute_script('delete_volume.sh', volume_id)
            res = json.loads(result)
            logger.debug(f"Return {res}")
            return res
        except (TypeError, json.JSONDecodeError):
            logger.error(f"Unable to delete volume {volume_id}: {result}",
                         exc_info=True)
            return None

    @staticmethod
    def delete_server(server_id):
        logger.info(f"Start deleting server instance {server_id}")
        try:
            result = utils.execute_script('terminate_server.sh', server_id)
            res = json.loads(result)
            logger.debug(f"Return {res}")
            return res
        except (TypeError, json.JSONDecodeError):
            logger.error(f"Unable to delete server {server_id} : {result}",
                         exc_info=True)
            return None

    @staticmethod
    def get_servers():
        logger.info("Get servers")
        res = []
        try:
            result = utils.execute_script('get_servers.sh')
            json_result = json.loads(result)
            for server in json_result["servers"]:
                res.append(server)
                logger.debug(f"Server: {server['name']}")
            return res
        except (TypeError, json.JSONDecodeError, KeyError) as error:
            # error while parsing
            logger.error(f"Unable to get servers info: {result}",
                         exc_info=True)
            return None

    @staticmethod
    def power_server(server_id):
        logger.info("Start powering on the server")
        try:
            result = utils.execute_script('poweron_server.sh', server_id)
            res = json.loads(result)
            logger.debug(f"Return {res}")
            return res
        except (TypeError, json.JSONDecodeError):
            logger.error(f"Unable to power on server {server_id} : {result}",
                         exc_info=True)
            return None
