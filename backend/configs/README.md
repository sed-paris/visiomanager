[[_TOC_]]
# Backend configuration files

This folder contains all configuration files for the backend server.
These files are given as configuration example.

**Reminder**: The backend configuration also depends on some environment
variables to be set.

## Global configuration

The `global.json` file contains information about:

- web urls (frontend) for development, qualification and production servers
- additional external authentication method (e.g., cas)

## Scaleway configuration

The `scaleway` folder contains files providing the server URL-IPs and
URL-IMAGEIDs for scaleway.

The development, qualification and production files shall be respectively
named:

- `scaleway_test.json`
- `scaleway_qualif.json`
- `scaleway_prod.json`

## Email configuration

The `email/emails.json` file contains email configuration, e.g., email content.

