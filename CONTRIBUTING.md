# Contributing to VisioManager
We love your input! We want to make contributing to this project as easy and transparent as possible, whether it's:

- Reporting a bug
- Discussing the current state of the code
- Submitting a fix
- Proposing new features
- Becoming a maintainer

## Reporting a bug

Please open an issue in order to report a bug. A template for bugs is provided.

## Discussing the current state of the code

Please open an issue in order to give any comment about the current state of the code. Feedback is needed!

## Submitting a fix

If you are already a developer, please create a new branch from *dev* branch with your proposed changes, propose your Merge Request (MR) and request a review.

Details for contributing with a Merge Request (MR) are described in the [development workflow](./docs/dev-workflow.md) document.

## Proposing new features

Please open an issue, or if you are already a developer and brave enough open also a MR with the proposed changes ;).

## Becoming a maintainer

Please contact the main maintainer **Pierre-Guillaume RAVERDY** at <gitlab@gitlab.inria.fr>.
