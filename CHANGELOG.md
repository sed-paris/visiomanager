# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Fixed

- Fix accessibility errors with linter and chrome extensions tools #123

### Added

### Changed

- Enable text input on date and time #122
- Update alt attribute in english and french #129
- Update nodejs version to v14 #128

### Continuous Integration

- Test directly with docker app image #74
- Enable static analysis & unit test reports #127


## [1.4]

- Update scripts for BBB version 2.3 !67

## [1.3]

### Fixed

- Fix manage_db tool !64
- Fix thread deadlocks !66
- Fix emails sending !66

### Security

- Implement recommendations of security audit report !64
  - Protect script execution #110 (closed)
  - Bind docker port to localhost only (via docker_run script)
  - Rework CORS (restrict backend access to host and remove useless CORS headers in frontend) #112
- Add more control on scaleway instances (threshold alerts + limit on the number
  of simultaneous instances) #109 !66
- Add backend API for monitoring web app health from an external program #67 !66

## [1.2.1]

### Fixed

- Fixed issue wrt change in root CA of ifconfig.me (bbb install scripts)

## [1.2]

### Added

- Add scripts to build a BBB base image #90 !44
- Add button to add extra time !61
- Add page with user's future requests, ability to delete #116

### Changed

- Improve reusability by reworking env variables, config management & docker #98
  !49
  - Separate docker frontend & backend containers & update docker scripts
  - Move logs outside the container (add variable VM_APP_DATA)
  - Gather backend configuration (e.g., emails, scaleway) into a configs
    directory to copy into docker container
- Check environment variables at startup !56
- Improved home page and creation form readability (terminology) and design, added links to youtube tutorial #114
- Update BBB images (2.2.32) !62

### Security & Privacy

- Update privacy policy and tos !57
- Use 'secrets' library for generating password !58

## [1.1.1]

### Fixed

- Fix missing time in visio request email #106

## [1.1.0]

### Added

- Add CAS functionality #86 !52 !50
- Simplify and make CAS functionality optional #104 !54
- Add possibility for user to remove its instances !54
- Add validation checks in the creation form #93 !50

### Changed

- Pre-fill form with user info (user must be logged to create visio) !50
- Remove jwt token usage (replace by flask session) !54

### Fixed

- Fix cookie consent #101 !53
- Fix time in request email to use CET #93 !50
- Fix various UI issues #93 !50

## [1.0.3]

### Added

- Add python database tool script (init/save/restore) #91 !48
- Add db save in docker clean script !48
- Add latests info of scaleway ids for testing env !48

### Changed

- Use environment variable to configure frontend/backend configuration (prod, qualif, test, dev)
- Use blueprint for routes !48

### Fixed

- Fix send date for email !48
- Increased timeout to deal with sshd/rc.local (temporary) !48

## [1.0.2] - 2021-02-03

### Changed

- Do not terminate "rogue" servers with specific prefix #89 !43

## [1.0.1] - 2021-02-03

### Fixed

- Wrong field checked when filtering instance to terminate

## [1.0] - 2021-02-03

### Added

- Add GDPR info and consent management to the Web app
- Add logging support in the backend #63 !27
- Add support for periodic tasks in the background #15
- Add UI/backend support for planning visio in the future #76
- Add generic parameters support in User, Instance and Request models #68
- Add possibility to launch docker container as current user #79 !36
- Add CI/CD for qualif/prod deployment #10 !37

### Changed

- Clean dead code (old test/init)  #71 !27
- Refactor routes & scaleway script management !27
- Simplify server creation form #76
- Generate password in background, notify by emails, text for emails as resources #65
- Update docker scripts (db init, clean, qualif/prod run)  #10 !37

### Fixed

- Fix access to backend apidocs #69 !35
- Fix missing emails #83 !37

### Security

- Fix issue when connecting to the same domain but a new server #87

## [0.3.2] - 2020-12-13

### Fixed

- Fixes bug when reserved IP on scaleway side not assigned to imageReserved

## [0.3.1] - 2020-12-12

### Fixed

- Fix wrong paths in scripts

## [0.3] - 2020-12-11

### Added
- Replace unittest for Pytest in the back-end !18
- Add test for launching back-end in different configurations !18
- Add tests in the back-end for data management, data utils, add users !18
- Added configuration file to setup mappings between URLs and IPs/ImagesIDs #70
- Added duration in visio creation form #52

### Fixed
- Fixed server status display in servers #52
- Fixed owner display in requests #52

## [0.2.2] - 2020-12-08

### Fixed
- Hotfix for Image IDs with proper handling of internal IP

## [0.2.1] - 2020-12-02

### Fixed
- Hotfix for typo in fieldname, datetime processing #61

## [0.2.0] - 2020-12-02

### Added
- Dockerfile support #1
- Internationalization with fr/en languages #21 !2
- Gitlab templates for issues and merge requests #44
- Continuous integration setup both backend unittests and frontent unit and e2e #9 !8
- Proper database with SQLAlchemy #41 !10
- Cards on home page on visio sytems #50 !14
- bcrypt password encyption for password #33 !11

### Fixed
- Fixed SQLLite DB in container #57 !15
- Fields in server/requests tables #38 !12

### Minor
- Development/test instances on scaleway #45
- Add button margin #47 !7
- Fixes minor spacing issues !13
- Add button margin #47
- Add scaleway development instance !6

## [0.1.0] - 2020-11-22

### Added
- initial version of the Web app
