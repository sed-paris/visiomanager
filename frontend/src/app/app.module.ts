import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule, HttpClient, HTTP_INTERCEPTORS} from '@angular/common/http';
import {RouterModule, Routes} from '@angular/router';

import {LayoutModule} from '@angular/cdk/layout';
import {CommonModule} from '@angular/common';

import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material/select';
import {MatListModule} from '@angular/material/list';
import {MatNativeDateModule, MatOptionModule, MAT_DATE_LOCALE} from '@angular/material/core';
import {MatInputModule} from '@angular/material/input';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatButtonModule} from '@angular/material/button';
import {MatSliderModule} from '@angular/material/slider';
import {MatDatepickerModule} from '@angular/material/datepicker';

import {FlexLayoutModule} from '@angular/flex-layout';

import {FooterComponent} from './footer.component';
import {HeaderComponent} from './header.component';
import {VisiosComponent} from './visios/visios.component';
import {SidenavComponent} from './visios-nav/visios-nav.component';
import {VisioTasksComponent} from './visio-tasks/visio-tasks.component';


// import ngx-translate and the http loader
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {BrowserModule, HAMMER_GESTURE_CONFIG} from '@angular/platform-browser';
import {MatTableModule} from '@angular/material/table';
import {LoginComponent} from './login/login.component';
import {VisioCreateComponent} from './visio-create/visio-create.component';
import {VisioServersComponent} from './visio-servers/visio-servers.component';
import {MatCardModule} from '@angular/material/card';
import {SessionListService} from './session-list.service';
import {ConfirmDialogDirective} from './ConfirmDialog';
import {VisioRequestsComponent} from './visio-requests/visio-requests.component';
import {STEPPER_GLOBAL_OPTIONS} from '@angular/cdk/stepper';
import {MatStepperModule} from '@angular/material/stepper';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatChipsModule} from '@angular/material/chips';

import {CookieconsentService} from './consent/cookie-consent.service';
import { CookieconsentComponent } from './consent/cookie-consent.component';

import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatSortModule} from '@angular/material/sort';
import { MatDialogModule, MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material/dialog';


// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

const appRoutes: Routes = [
  {
    path: '',
    redirectTo: '/visios',
    pathMatch: 'full'
  },
  {
    path: 'visios',
    component: VisiosComponent,
    data: {title: 'Visio Manager'}
  },
  {
    path: 'visios-nav',
    component: SidenavComponent,
    data: {title: 'Menu'}
  },
  {
    path: 'login',
    component: LoginComponent,
    data: {title: 'Login'}
  },
  {
    path: 'visio-create',
    component: VisioCreateComponent,
    data: {title: 'Create'}
  },
  {
    path: 'visio-servers',
    component: VisioServersComponent,
    data: {title: 'Servers'}
  },
  {
    path: 'visio-requests',
    component: VisioRequestsComponent,
    data: {title: 'Requests'}
  },
  {
    path: 'visio-tasks',
    component: VisioTasksComponent,
    data: {title: 'Tasks'}
  },
];


// material related def, imported afterwards
@NgModule({
  exports: [
    MatButtonModule,
    MatCardModule,
    MatSliderModule,
    MatSelectModule,
    MatFormFieldModule,
    MatProgressSpinnerModule,
    MatInputModule,
    MatListModule,
    MatIconModule,
    MatOptionModule,
    FlexLayoutModule,
    MatSidenavModule,
    MatToolbarModule,
    MatStepperModule,
    MatSnackBarModule,
    MatChipsModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatNativeDateModule,
    NgxMaterialTimepickerModule,
    BrowserAnimationsModule
  ],
  providers: [
    SessionListService,
    CookieconsentService,
    {
      provide: STEPPER_GLOBAL_OPTIONS,
      useValue: {displayDefaultIndicatorType: false}
    },
    {provide: MAT_DATE_LOCALE, useValue: 'fr-FR'}
  ],
  imports: [
    LayoutModule,
    MatStepperModule,
    MatToolbarModule,
    MatButtonModule,
    MatProgressSpinnerModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    BrowserAnimationsModule,
    NgbModule,
    NgxMaterialTimepickerModule],
  declarations: []
})
export class AllMaterialModule {
}


@NgModule({
  declarations: [
    AppComponent,
    VisiosComponent,
    FooterComponent,
    HeaderComponent,
    SidenavComponent,
    LoginComponent,
    VisioCreateComponent,
    VisioServersComponent,
    ConfirmDialogDirective,
    VisioRequestsComponent,
    VisioTasksComponent,
    CookieconsentComponent
  ],
  imports: [
    CommonModule,
    AllMaterialModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot(
      appRoutes,
      {useHash: true, enableTracing: true} // <-- enableTracing debugging purposes only
    ),
    MatIconModule,
    MatTableModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    NgxMaterialTimepickerModule.setLocale('fr-FR'),
    MatSortModule,
    MatDialogModule
  ],
  providers: [
    { // config for cookie consent
      provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue:{
      disableClose: true,
      autoFocus: true,
      restoreFocus: true,
      panelClass: "custom-cc-box"
    }}
  ],
  bootstrap: [AppComponent],
  entryComponents : [CookieconsentComponent]
})
export class AppModule {

}

