import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';
import {VisiosComponent} from './visios.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SessionListService} from '../session-list.service';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatCardModule} from '@angular/material/card';
import {MatChipsModule} from '@angular/material/chips';
import {MatToolbarModule} from '@angular/material/toolbar';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';

describe('VisiosComponent', () => {
  let component: VisiosComponent;
  let fixture: ComponentFixture<VisiosComponent>;
  let sessionService: SessionListService;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        ReactiveFormsModule,
        FormsModule,
        MatCardModule,
        MatChipsModule,
        MatToolbarModule,
        MatProgressSpinnerModule,
        TranslateModule.forRoot()
      ],
      declarations: [VisiosComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [SessionListService, TranslateService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisiosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    sessionService = TestBed.get(SessionListService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
