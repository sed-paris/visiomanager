'use strict';

import {Component, OnInit, OnDestroy, AfterViewInit, ViewChild} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {ActivatedRoute, Router} from '@angular/router';
import {Meta} from '@angular/platform-browser';
import {SessionListService} from '../session-list.service';

import {environment} from '../../environments/environment';
import {TranslateService} from '@ngx-translate/core';

export interface VisioInstance {
  name: string;
  url: string;
  server_type: string;
  state: string;
  ip: string;
  created: string;
  zone: string;
}

const ELEMENT_DATA: VisioInstance[] = [];

@Component({
  selector: 'app-root',
  templateUrl: './visios.component.html',
  styleUrls: ['./visios.component.scss']
})
export class VisiosComponent implements OnInit, AfterViewInit, OnDestroy {

  title = 'Visio Manager';

  html = {
    response: '',
    request_new_video: 'Launch a new BBB server'
  };

  showSpinner = false;


  /**
   *
   * @param router
   * @param http
   * @param meta
   */
  constructor(
    private router: Router,
    private http: HttpClient,
    private meta: Meta,
    private sessiontListService: SessionListService,
    private translate: TranslateService
  ) {

  }
  
  data: any;
  API_GETUSERINFO = '/get_user_info';

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.showSpinner = false;

    // Get user info if already logged at backend
    const httpOptions = {
      withCredentials: true,
      headers: new HttpHeaders({
        'Accept': 'application/json'
      })
    };
    this.http.get(environment.apiEndpoint + this.API_GETUSERINFO, httpOptions).subscribe(resp => {
      this.data = resp;
      // completed, hides spinner
      this.showSpinner = false;

      // if user info returned, setup and set as connected
      if (this.data.status === 'success') {
        this.sessiontListService.connect(this.data.role, this.data.name, this.data.email);
      } else {
        this.sessiontListService.disconnect();
      }
    
    }, err => {
      this.showSpinner = false;

    });

  }

  ngOnDestroy() {
  }


  //
  //

  gotIt() {
    this.sessiontListService.gotIt();
  }

  isDisclaimerOk() {
    return this.sessiontListService.isDisclaimerOk();
  }
}
