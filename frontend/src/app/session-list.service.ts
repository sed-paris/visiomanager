import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {ActivatedRoute, Router} from '@angular/router';

const LOGIN_KEY = 'vmLogin';
const EMAIL_KEY = 'vmEmail';
const ROLE_KEY = 'vmRole';

const ROLE_ADMIN = 'admin';
const ROLE_USER = 'user';

@Injectable()
export class SessionListService {



  private isConnected = false;
  private gotDisclaimer = false;


  /**
   *
   */
  constructor(private router: Router) {

    // initialize the connection flag
      this.isConnected = false;
  }

  loginName() {
    return localStorage.getItem(LOGIN_KEY);
  }

  loginRole() {
    return localStorage.getItem(ROLE_KEY);
  }

  isAdmin() {
    const role = this.loginRole();
    return role === ROLE_ADMIN;
  }

  disconnect(): void {
    // remove user info from the global storage object
    localStorage.removeItem(LOGIN_KEY);
    localStorage.removeItem(ROLE_KEY);
    localStorage.removeItem(EMAIL_KEY);

    this.isConnected = false;
  }

  connect(role: string, name: string, email: string): void {
    // save user info to the global storage object
    localStorage.setItem(ROLE_KEY, role);
    localStorage.setItem(LOGIN_KEY, name);
    localStorage.setItem(EMAIL_KEY, email);
    this.isConnected = true;
  }

  isUserConnected() {
    return this.isConnected;
  }

  gotIt() {
    this.gotDisclaimer = true;
  }

  isDisclaimerOk() {
    return this.gotDisclaimer;
  }

  getLogin(): string {
    return localStorage.getItem(LOGIN_KEY);
  }

  getEmail(): string {
    return localStorage.getItem(EMAIL_KEY);
  }

}
