import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import { AppComponent } from '../app.component';

/**
 * @title Dialog elements
}*/


@Component({
  selector: 'cookie-consent',
  templateUrl: './cookie-consent.component.html',
  styleUrls: ['./cookie-consent.component.css'],
  // this will allow us to override the mat-dialog-container CSS class
  encapsulation: ViewEncapsulation.None
})
export class CookieconsentComponent {
}
