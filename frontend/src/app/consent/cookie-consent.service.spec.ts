import {TestBed, waitForAsync} from '@angular/core/testing';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';

import {CookieconsentService} from './cookie-consent.service';


describe('CookieconsentService', () => {
  let cookieConsentService: CookieconsentService;
  let matDialogSpy: jasmine.SpyObj<MatDialog>


  beforeEach(waitForAsync(() => {
    const spy  = jasmine.createSpyObj('MatDialog', ['open']);
    TestBed.configureTestingModule({
      imports: [
        MatDialogModule
      ],
      providers: [
        {provide: MatDialog, useValue: spy}
      ],
    }).compileComponents();

    cookieConsentService = TestBed.inject(CookieconsentService);
    matDialogSpy = TestBed.inject(MatDialog) as jasmine.SpyObj<MatDialog>
  }));

  it('should be created', () => {
    expect(cookieConsentService).toBeTruthy();
  });

});

