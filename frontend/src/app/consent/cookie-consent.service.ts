import {Injectable} from '@angular/core';
import { MatDialog, MatDialogConfig, MatDialogRef, _closeDialogVia } from '@angular/material/dialog';
import { CookieconsentComponent } from './cookie-consent.component';

const CC_KEY = 'cconsented';

@Injectable({
  providedIn: 'root'
})
export class CookieconsentService {

  private consented = false;

  constructor(private dialog: MatDialog) {
    // initialize the connection flag depending on jwt token
    if (localStorage.getItem(CC_KEY) === null || localStorage.getItem(CC_KEY) === undefined) {
      localStorage.setItem(CC_KEY, 'false');
      this.consented = false;
    } else {
      const ccValue = localStorage.getItem(CC_KEY);
      const isTrueSet = (ccValue === 'true');
      localStorage.setItem(CC_KEY, isTrueSet.toString());
      this.consented = isTrueSet;
    }
  }

  consent(): void {
    this.consented = true;
    localStorage.setItem(CC_KEY, 'true');
  }

  cancel(): void {
    this.consented = false;
    localStorage.setItem(CC_KEY, 'false');
  }

  hasConsented(): boolean {
    return this.consented;
  }

  openDialog() {

    this.dialog.closeAll()
    const dialogRef = this.dialog.open(CookieconsentComponent);
    dialogRef.afterClosed().subscribe({
      next: (result: string) => {
        if (result === 'accept') {
          this.consent();
        }
        if (result === 'deny') {
          this.cancel();
        }
      }
    });
  }

  closeDialog(){
    this.dialog.closeAll()
  }
}
