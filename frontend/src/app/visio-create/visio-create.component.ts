'use strict';

import {Component, OnInit, OnDestroy, AfterViewInit, ViewChild} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {ActivatedRoute, Router} from '@angular/router';

import {FormControl, FormGroup, Validators, AbstractControl} from '@angular/forms';
import {FormBuilder} from '@angular/forms';

import {MatSnackBar} from '@angular/material/snack-bar';

import {environment} from '../../environments/environment';
import {TranslateService} from '@ngx-translate/core';
import {SessionListService} from '../session-list.service';

import {
  MAT_MOMENT_DATE_FORMATS,
  MomentDateAdapter,
  MAT_MOMENT_DATE_ADAPTER_OPTIONS,
} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';


// eslint-disable-next-line
function startDateConditionallyRequiredValidator(formControl: AbstractControl) {
  if (!formControl.parent) {
    return null;
  }
  if (formControl.parent.get('myCheckbox').value) {
    return null;
  }
  return Validators.required(formControl);
}

@Component({
  selector: 'app-root',
  templateUrl: './visio-create.component.html',
  styleUrls: ['./visio-create.component.scss'],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },
    {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS}
  ]
})
export class VisioCreateComponent implements OnInit, AfterViewInit, OnDestroy {

  visioSizes = [
    {value: 'dev-l', view: 'X-Small (for tests)'},
    {value: 'xs', view: 'Small (2-5)'},
    {value: 's', view: 'Medium (4-10)'},
    {value: 'm', view: 'Large (8-20)'},
  ];

  visioDurationsHour = [
    {value: '0', view: '0 hour'},
    {value: '1', view: '1 hour'},
    {value: '2', view: '2 hours'},
    {value: '3', view: '3 hours'},
    {value: '4', view: '4 hours'},
    {value: '5', view: '5 hours'},
    {value: '6', view: '6 hours'},
    {value: '7', view: '7 hours'},
    {value: '8', view: '8 hours'},
    {value: '9', view: '9 hours'}
  ];

  visioDurationsMin = [
    {value: '0', view: '0 minutes'},
    {value: '15', view: '15 minutes'},
    {value: '30', view: '30 minutes'},
    {value: '45', view: '45 minutes'}
  ];

  // required for creation form
  myControl = new FormControl();
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;

  // flag to fore (true) or not (false) the lineat step-by-step form fill
  isLinear = true;

  showSpinner = false;

  API_LAUNCH_VISIO = '/launch_visio';

  // feedback message to be displayed after network calls
  message = '';

  // for starting a new video server
  newVisioData = {
    name: new FormControl('', [
      Validators.required,
      Validators.minLength(6),
      Validators.maxLength(20),
      Validators.pattern('^[a-zA-Z0-9_-]{6,20}$')
    ]),
    email: new FormControl('', [Validators.required, Validators.email]),
    server_type: 'xs', // new FormControl('xs'),
    start_time: new FormControl('',[
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(5),
        Validators.pattern('^[0-9_-]{1,2}:[0-9_-]{1,2}$')
      ]
    ),
    duration_hour: '0',
    duration_min: '0',
    login: new FormControl('', [
      Validators.required,
      Validators.minLength(6),
      Validators.maxLength(20),
      Validators.pattern('^[a-zA-Z0-9_-]{6,20}$')
    ]),
    start_date: new FormControl('', [
      Validators.minLength(2),
      startDateConditionallyRequiredValidator
    ]),
    isNow: false,
  };
  newdata = {};

  hideForm = false;


  /**
   *
   */
  constructor(
    private router: Router,
    private http: HttpClient,
    private formBuilder: FormBuilder,
    private snackBar: MatSnackBar,
    private translate: TranslateService,
    private dateAdapter: DateAdapter<any>,
    private sessiontListService: SessionListService
  ) {
      if (!sessiontListService.isUserConnected()) {
        this.router.navigate(['login']);
      }
  }

  ngOnInit() {
    this.firstFormGroup = this.formBuilder.group({
      nameCtrl: this.newVisioData.name,
      emailCtrl: this.newVisioData.email
    });
    this.secondFormGroup = this.formBuilder.group({
      dateCtrl: this.newVisioData.start_date,
      timeCtrl: this.newVisioData.start_time,
      myCheckbox: ['']
    });
    this.thirdFormGroup = this.formBuilder.group({
      thirdCtrl: ['', Validators.required]
    });

    this.secondFormGroup.get('myCheckbox').valueChanges
      .subscribe(value => {
        this.newVisioData.isNow = value;
        this.secondFormGroup.get('dateCtrl').updateValueAndValidity();
        this.secondFormGroup.get('timeCtrl').updateValueAndValidity();
      });

    this.newVisioData.name.setValue(this.sessiontListService.getLogin());
    this.newVisioData.email.setValue(this.sessiontListService.getEmail());
  }

  get reqEmail() {
    return this.firstFormGroup.get('emailCtrl');
  }

  get reqName() {
    return this.firstFormGroup.get('nameCtrl');
  }

  ngAfterViewInit() {
    this.showSpinner = false;
    this.newVisioData.server_type = 'xs'; // new FormControl('xs')
    this.newVisioData.duration_min = '0';
    this.newVisioData.duration_hour = '0';
  }

  ngOnDestroy() {
  }

  getErrorMessage(item) {
    if (item === 'email') {
      if (this.newVisioData.email.hasError('required')) {
        return 'You must enter a value';
      }
      return this.newVisioData.email.hasError('email') ? 'Not a valid email' : '';
    }
    if (item === 'name') {
      if (this.newVisioData.name.hasError('required')) {
        return 'You must enter a value';
      }
      return (this.newVisioData.name.invalid) ? 'Should only contain 6 to 20 alphanumeric _ - . characters (BBB/Greenlight setup limitation, can be changed once logged in)' : '';
    }
    if (item === 'start_date') {
      if (this.newVisioData.start_date.hasError('required')) {
        return 'You must enter a value';
      }
      return (this.newVisioData.start_date.invalid) ? 'Should be a date in the format DD/MM/YYYY' : '';
    }
    if (item === 'start_time') {
      if (this.newVisioData.start_time.hasError('required')) {
        return 'You must enter a value';
      }
      return (this.newVisioData.start_time.invalid) ? 'Should be a time in the format HH:MM': '';
    }
  }

  parseTime(timeString) {
    if (timeString === '') {
      return null;
    }

    const time = timeString.match(/(\d+)(:(\d\d))?\s*(p?)/i);
    if (time == null) {
      return null;
    }

    let hours = parseInt(time[1], 10);
    if (hours == 12 && !time[4]) {
      hours = 0;
    } else {
      hours += (hours < 12 && time[4]) ? 12 : 0;
    }
    const d = new Date();
    d.setHours(hours);
    d.setMinutes(parseInt(time[3], 10) || 0);
    d.setSeconds(0, 0);
    return d;
  }


  onSubmit() {

    let start_date, start_time;
    if (this.newVisioData.isNow) {
      start_date = new Date(Date.now());
    } else {
      start_date = new Date(Date.parse(this.newVisioData.start_date.value));
      start_time =  this.parseTime(this.newVisioData.start_time.value);
      start_date.setHours(start_time.getHours());
      start_date.setMinutes(start_time.getMinutes());
    }

    const cloneVisioData = {
      // requester info
      name: this.newVisioData.name.value,
      email: this.newVisioData.email.value,
      // visio specs
      is_now: this.newVisioData.isNow,
      server_type: this.newVisioData.server_type,
      start_time: start_date.toUTCString(),
      duration: parseInt(this.newVisioData.duration_hour) * 60 + parseInt(this.newVisioData.duration_min),
      // BBB config
      login: this.newVisioData.name.value,
    };

    // Process checkout data here
    this._launchVisio(cloneVisioData);
  }


  async _launchVisio(newVisio) {
    // display spinner while loading data
    this.showSpinner = true;

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      })
    };

    this.http.post(environment.apiEndpoint + this.API_LAUNCH_VISIO, newVisio, httpOptions)
      .subscribe(resp => {
          const response: any = resp;
          if (response.status === 'success') {
            // completed with success
            this.openSnackBar('Request processed (wait for our emails)', 'OK');
            this.router.navigate(['visio-tasks']);
          } else {
            // completed with failure
            this.openSnackBar('Unable to process request', 'OK');
          }
          this.showSpinner = false;
        }, err => {
          this.showSpinner = false;
          // completed with failure
          this.openSnackBar('Unable to process request. Reason: ' + this.message, 'OK');
        }
      );
  }

  openSnackBar(
    message: string,
    action: string
  ) {
    const snackBarRef = this.snackBar.open(message, action, {
      duration: 8000,
    });
    snackBarRef.afterDismissed().subscribe(() => {
      console.log('The snack-bar was dismissed');
    });
    snackBarRef.onAction().subscribe(() => {
      console.log('The snack-bar action was triggered!');
    });
  }
}
