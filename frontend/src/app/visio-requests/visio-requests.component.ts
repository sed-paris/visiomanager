'use strict';

import {Component, OnInit, OnDestroy, AfterViewInit, ViewChild} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {ActivatedRoute, Router} from '@angular/router';

import {catchError, timeout} from 'rxjs/operators';

import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';

import {environment} from '../../environments/environment';
import {TranslateService} from '@ngx-translate/core';
import {SessionListService} from '../session-list.service';

export interface RequestInstance {
  id: string;
  owner: string;
  status: string;
  issued: string;
  title: string;
  start_time: string;
  end_time: string;
  server: string;
  server_type: string;
  server_status: string;
  ts_start: string;
  ts_stop: string;
}

const ELEMENT_DATA: RequestInstance[] = [];

@Component({
  selector: 'app-root',
  templateUrl: './visio-requests.component.html',
  styleUrls: ['./visio-requests.component.scss']
})
export class VisioRequestsComponent implements OnInit, AfterViewInit, OnDestroy {

  title = 'VisioRequestsComponent';

  displayedColumns: string[] = ['owner', 'status', 'issued', 'start_time', 'end_time', 'server_type', 'url'];

  dataSource = new MatTableDataSource(ELEMENT_DATA);

  @ViewChild(MatSort) sort: MatSort;

  html = {
    response: ''
  };

  showSpinner = false;

  API_GET_REQUESTS_INFO = '/get_requests';

  // feedback message to be displayed after network calls
  message = '';

  /**
   *
   * @param router
   * @param http
   * @param meta
   * @param formBuilder
   */
  constructor(
    private router: Router,
    private http: HttpClient,
    private sessiontListService: SessionListService,
    private translate: TranslateService
  ) {
    if (!sessiontListService.isUserConnected()) {
      this.router.navigate(['login']);
    }
  }

  ngOnInit() {
    this.reloaRequestsInfo();
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
  }

  ngOnDestroy() {
  }

  // Actions
  reloaRequestsInfo() {
    this._getVisioRequestsStatus();
  }

  cancel() {
  }

  //

  /**
   */
  async _getVisioRequestsStatus() {

    // display spinner while loading data
    this.showSpinner = true;

    const httpOptions = {
      headers: new HttpHeaders({
        'Accept': 'application/json'
      })
    };

    // call backedn server for room info with 60 sec timeout
    this.http.get(environment.apiEndpoint + this.API_GET_REQUESTS_INFO
      , httpOptions)
      .pipe(timeout(60000),
        catchError(e => {
          // do something on a timeout
          return null;
        }))
      .subscribe(data => {
          const response: any = data;

          if (response['status'] === 'success') {
            // completed, hides spinner
            console.log(response);
            const requests = response['requests'] as RequestInstance[];

            ELEMENT_DATA.length = 0;
            for (const request of requests) {
              const element: RequestInstance = request as any as RequestInstance;
              ELEMENT_DATA.push(element);
            }

            this.dataSource = new MatTableDataSource(ELEMENT_DATA);
            console.log('Updated data source');
          } else {
            // completed, hides spinner
            console.log('Call failed');
          }
          this.showSpinner = false;
        }, err => {
          console.log('Error during http call ' + JSON.stringify(err));
          this.showSpinner = false;
        }
      );
  }
}
