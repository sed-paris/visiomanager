import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-footer',
  template: `
    <div class="footer_row">
      <div class="footer_item">
        <img src="/assets/images/logo_inria.jpg" alt=""/>
      </div>
    </div>
  `,
  styleUrls: ['./app.component.css']
})

export class FooterComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

}
