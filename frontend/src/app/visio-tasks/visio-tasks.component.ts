'use strict';

import {Component, OnInit, OnDestroy, AfterViewInit, ViewChild} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {ActivatedRoute, Router} from '@angular/router';

import {catchError, timeout} from 'rxjs/operators';

import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {MatSnackBar} from '@angular/material/snack-bar';

import {environment} from '../../environments/environment';
import {TranslateService} from '@ngx-translate/core';
import {SessionListService} from '../session-list.service';

export interface TaskInstance {
  id: string;
  owner: string;
  status: string;
  issued: string;
  start_time: string;
  end_time: string;
  server_type: string;
}

const ELEMENT_DATA: TaskInstance[] = [];

@Component({
  selector: 'app-root',
  templateUrl: './visio-tasks.component.html',
  styleUrls: ['./visio-tasks.component.scss']
})
export class VisioTasksComponent implements OnInit, AfterViewInit, OnDestroy {

  title = 'VisioTasksComponent';

  displayedColumns: string[] = ['owner', 'status', 'issued', 'start_time', 'end_time', 'server_type', 'actionq'];
  dataSource = new MatTableDataSource(ELEMENT_DATA);

  @ViewChild(MatSort) sort: MatSort;

  html = {
    response: ''
  };

  showSpinner = false;

  API_GET_TASKS_INFO = '/get_tasks';
  API_DELETE_UPCOMING_TASK = '/delete_task';

  // feedback message to be displayed after network calls
  message = '';

  /**
   *
   * @param router
   * @param http
   * @param snackBar
   * @param sessiontListService
   * @param translate
   */
  constructor(
    private router: Router,
    private http: HttpClient,
    private snackBar: MatSnackBar,
    private sessiontListService: SessionListService,
    private translate: TranslateService
  ) {
    if (!sessiontListService.isUserConnected()) {
      this.router.navigate(['login']);
    }
  }

  ngOnInit() {
    this.dataSource.sort = this.sort;
    this.reloaTasksInfo();
  }

  ngAfterViewInit() {
  }

  ngOnDestroy() {
  }

  reloaTasksInfo() {
    this._getVisioTasksStatus();
  }

  cancel() {
  }

  //

  /**
   */
  async _getVisioTasksStatus() {

    // display spinner while loading data
    this.showSpinner = true;

    const httpOptions = {
      headers: new HttpHeaders({
        'Accept': 'application/json',
      })
    };

    // call backedn server for room info with 60 sec timeout
    this.http.get(environment.apiEndpoint + this.API_GET_TASKS_INFO
      , httpOptions)
      .pipe(timeout(60000),
        catchError(e => {
          // do something on a timeout
          return null;
        }))
      .subscribe(data => {
          const response: any = data;

          if (response['status'] === 'success') {
            // completed, hides spinner
            const tasks = response['tasks'] as TaskInstance[];

            ELEMENT_DATA.length = 0;
            for (const task of tasks) {
              const element: TaskInstance = task as any as TaskInstance;
              ELEMENT_DATA.push(element);
            }
            this.dataSource = new MatTableDataSource(ELEMENT_DATA);
          } else {
            // completed, hides spinner
            console.log('Call failed');
          }
          this.showSpinner = false;
        }, err => {
          console.log('Error during http call ' + JSON.stringify(err));
          this.showSpinner = false;
        }
      );
  }



  deleteItem(id) {

    // display spinner while loading data
    this.showSpinner = true;
    this.html.response = '';

    const postData = {
      taskid: id
    };

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      })
    };

    // call backedn server for room info with 60 sec timeout
    this.http.post(environment.apiEndpoint + this.API_DELETE_UPCOMING_TASK, postData
      , httpOptions)
      .pipe(timeout(60000),
        catchError(e => {
          // do something on a timeout
          return null;
        }))
      .subscribe(data => {
          const response: any = data;
          if (response['status'] === 'success') {
            // completed, hides spinner
            this.openSnackBar('Request deleted', 'OK');
            this.reloaTasksInfo();
          } else {
            // completed, hides spinner
            this.openSnackBar('Unable to proceed', 'OK');
          }
          this.showSpinner = false;
        }, err => {
          this.openSnackBar('Unable to proceed', 'OK');
          this.showSpinner = false;
        }
      );
  }

  openSnackBar(
    message: string,
    action: string
  ) {
    const snackBarRef = this.snackBar.open(message, action, {
      duration: 8000,
    });
    snackBarRef.afterDismissed().subscribe(() => {
      console.log('The snack-bar was dismissed');
    });
    snackBarRef.onAction().subscribe(() => {
      console.log('The snack-bar action was triggered!');
    });
  }
}
