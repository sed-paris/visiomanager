import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';
import {VisioTasksComponent} from './visio-tasks.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {SessionListService} from '../session-list.service';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {MatTableModule} from '@angular/material/table';
import { LoginComponent } from '../login/login.component';

describe('VisioTasksComponent', () => {
  let component: VisioTasksComponent;
  let fixture: ComponentFixture<VisioTasksComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule.withRoutes(
          [{path: 'login', component: LoginComponent}]
        ),
        ReactiveFormsModule,
        FormsModule,
        MatSnackBarModule,
        MatProgressSpinnerModule,
        MatTableModule,
        TranslateModule.forRoot()
      ],
      declarations: [VisioTasksComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [SessionListService, TranslateService]

    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisioTasksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
