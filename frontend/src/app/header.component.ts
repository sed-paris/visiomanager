import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {SessionListService} from './session-list.service';

@Component({
  selector: 'app-header',
  template: `
    <div class="header">
      <div class="container">
        <div class="row">
          <div class="col-md-8" style="width:auto;display: flex;align-items: center;">
            <span>Visio Manager</span>
          </div>
          <div class="col-md-4" style="display: flex;flex-direction:row;width:auto;">
            <div style="display: flex;">
              <button type="button" class="btn btn-secondary" (click)="routeHome()" style="flex;align-items: center;">
                <mat-icon>home</mat-icon>
                <span> {{ html.visios }}</span>
              </button>
            </div>

            <div [hidden]="!isConnected()" style="display: flex;">
              <button type="button" class="btn" style="vertical-align: middle;">
                <span> ddd{{ loginName() }}</span>
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  `,
  styleUrls: ['./app.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private router: Router,
              private route: ActivatedRoute,
              private sessiontListService: SessionListService) {
  }

  html = {
    visios: 'Visios',
    title: 'BBB Visio Manager',
  };


  ngOnInit() {
  }

  routeHome() {
    // go back to main page
    this.router.navigate(['visios']);
  }

  loginName() {
    return this.sessiontListService.loginName();
  }

  isConnected() {
    return this.sessiontListService.isUserConnected();
  }
}
