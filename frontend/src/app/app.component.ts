import {AfterViewInit, Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import '@angular/material/prebuilt-themes/deeppurple-amber.css';
import {TranslateService} from '@ngx-translate/core';

import {CookieconsentService} from './consent/cookie-consent.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements AfterViewInit {
  title = 'visiomanager';
  isConnected = true;

  // UI language setup
  browserLangage = 'en';
  isLangInit = false;


  constructor(private router: Router,
              private http: HttpClient,
              private consentService: CookieconsentService,
              private translate: TranslateService) {
    // UI language setup
    try {
      this.browserLangage = window.navigator['userLanguage'] || window.navigator.language;
      if (this.browserLangage !== 'fr' && this.browserLangage !== 'fr-FR') {
        this.browserLangage = 'en';
      } else {
        this.browserLangage = 'fr';
      }
    } catch (err) {
    }

    // this language will be used as a fallback when a translation isn't found in the current language
    translate.setDefaultLang(this.browserLangage);
    // the lang to use, if the lang isn't available, it will use the current loader to get them
    translate.use(this.browserLangage);

  }

  ngAfterViewInit(): void {
    // Useful for page refresh
    if (this.consentService.hasConsented()) {
      this.consentService.closeDialog();
    } else {
      this.consentService.openDialog();
    }
  }

}
