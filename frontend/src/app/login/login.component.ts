import {Component, OnInit, OnDestroy, AfterViewInit} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {ActivatedRoute, Router} from '@angular/router';

import {environment} from '../../environments/environment';
import {SessionListService} from '../session-list.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MatSnackBar} from '@angular/material/snack-bar';
import {LangChangeEvent, TranslateService} from '@ngx-translate/core';

// import ngx-translate and the http loader
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {HttpClientModule} from '@angular/common/http';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit, AfterViewInit, OnDestroy {

  constructor(
    private http: HttpClient,
    private router: Router,
    private sessiontListService: SessionListService,
    private snackBar: MatSnackBar,
    private formBuilder: FormBuilder,
    private translate: TranslateService
  ) {
  }

  // message = '';
  data: any;

  showCas = false;

  API_SIGNIN = '/signin';

  // for starting a new video server
  loginData = {
    username: new FormControl('', [
      Validators.required,
      Validators.minLength(6),
      Validators.maxLength(20),
      Validators.pattern('^[a-zA-Z0-9_-]{6,20}$')
    ]),
    password: new FormControl('', [
      Validators.required,
      Validators.minLength(6),
      Validators.maxLength(20),
      Validators.pattern('^[a-zA-Z0-9_-]{6,20}$')
    ])
  };
  hidePwd = true;

  // newdata = {}

  showSpinner = false;

  displaySignUp = environment.enable_registration;

  // required for creation form
  myControl = new FormControl();
  //
  loginForm: FormGroup;

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      firstCtrl: ['', Validators.required]
    });

    this.showCas = environment.cas;
  }

  ngAfterViewInit() {
  }

  ngOnDestroy = () => {
  }

  signcas() {
    if (environment.cas == true){
      this.showSpinner = true;
      window.location.pathname = '/api/login';
    }
    else {
      this.openSnackBar('CAS functionality is disabled', 'OK');
    }
  }

  login() {
    this.showSpinner = true;

    // login using the form's username and password
    const creds = {username: this.loginData.username.value, password: this.loginData.password.value};
    const httpOptions = {
      withCredentials: true,
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      })
    };
    this.http.post(environment.apiEndpoint + this.API_SIGNIN, creds, httpOptions).subscribe(resp => {
      this.data = resp;

      // completed, hides spinner
      this.showSpinner = false;

      if (this.data.status === 'success') {
        // navigate to visios to get user info
        this.router.navigate(['visios']);
      } else {
        this.sessiontListService.disconnect();

        // this.message = "Not authorized";
        this.openSnackBar('Not authorized', 'OK');
      }

    }, err => {
      this.showSpinner = false;
      // this.message = err.error.msg;
      this.openSnackBar('Not authorized', 'OK');
    });
  }

  openSnackBar(message: string, action: string) {
    const snackBarRef = this.snackBar.open(message, action, {
      duration: 8000,
    });
    snackBarRef.afterDismissed().subscribe(() => {
      console.log('The snack-bar was dismissed');
    });
    snackBarRef.onAction().subscribe(() => {
      console.log('The snack-bar action was triggered!');
    });
  }


  getErrorMessage(item) {
    if (item === 'username') {
      if (this.loginData.username.hasError('required')) {
        return 'You must enter a value';
      }
      return (this.loginData.username.invalid) ? 'Should only contain 6 to 20 alphanumeric _ - characters' : '';
    }
    if (item === 'password') {
      if (this.loginData.password.hasError('required')) {
        return 'You must enter a value';
      }
      return (this.loginData.password.invalid) ? 'Should only contain 6 to 20 alphanumeric _ - characters' : '';
    }
  }
}
