import {TestBed, inject} from '@angular/core/testing';

import {SessionListService} from './session-list.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

describe('SessionListService', () => {
  let service: SessionListService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      providers: [SessionListService]
    });
    service = TestBed.inject(SessionListService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  //it('should be created', inject([SessionListService], (service: SessionListService) => {
  //  expect(service).toBeTruthy();
  //}));
});
