import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';
import {SidenavComponent} from './visios-nav.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {SessionListService} from '../session-list.service';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {MatListModule} from '@angular/material/list';
import { LoginComponent } from '../login/login.component';
import { VisiosComponent } from '../visios/visios.component';
import { VisioCreateComponent } from '../visio-create/visio-create.component';
import { MatDialogModule } from '@angular/material/dialog';
import { CookieconsentService } from '../consent/cookie-consent.service';

describe('SidenavComponent', () => {
  let component: SidenavComponent;
  let fixture: ComponentFixture<SidenavComponent>;
  let sessionService: SessionListService;

  beforeEach(waitForAsync(() => {
    const spy = jasmine.createSpyObj('MatDialog', ['open']);
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule.withRoutes(
          [
            {path: 'login', component: LoginComponent},
            {path: 'visios', component: VisiosComponent},
            {path: 'visio-create', component: VisioCreateComponent}
          ]
        ),
        MatProgressSpinnerModule,
        MatListModule,
        TranslateModule.forRoot(),
        MatDialogModule
      ],
      declarations: [SidenavComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [SessionListService, TranslateService, CookieconsentService ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SidenavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    sessionService = TestBed.get(SessionListService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
