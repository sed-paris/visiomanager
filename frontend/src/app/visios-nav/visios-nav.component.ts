import {Component} from '@angular/core';
import {BreakpointObserver, Breakpoints} from '@angular/cdk/layout';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {ActivatedRoute, Router} from '@angular/router';
import {SessionListService} from '../session-list.service';
import {TranslateService} from '@ngx-translate/core';
import {HttpClientModule} from '@angular/common/http';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../environments/environment';

import {CookieconsentService} from '../consent/cookie-consent.service';

@Component({
  selector: 'visios-nav',
  templateUrl: './visios-nav.component.html',
  styleUrls: ['./visios-nav.component.css']
})
export class SidenavComponent {

  API_LOGOUT = '/logout';
  data: any;

  html = {
    about: 'jjj',
    response: '',
    request_new_video: 'Launch a new BBB server',
    status: 'Status',
    sign_signin: 'Signin',
    logout: 'Logout',
    title: 'BBB Visio Manager',
    create: 'Create',
    servers: 'Servers',
    requests: 'Past requests',
    tasks: 'Upcoming requests',
    home: 'Home'
  };


  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );


  constructor(
    private http: HttpClient,
    private breakpointObserver: BreakpointObserver,
    private router: Router,
    private route: ActivatedRoute,
    private sessiontListService: SessionListService,
    private translate: TranslateService,
    private consentService: CookieconsentService
  ) {
  }

  //
  //
  stopVideoIfOn() {
  }

  routeHome() {
    this.router.navigate(['visios']);
  }

  create() {
    this.router.navigate(['visio-create']);
  }

  /**
   * Logs out the current user and redirects to the login page.
   */
  logout() {

    const httpOptions = {
      headers: new HttpHeaders({
        'Accept': 'application/json'
      })
    };

    this.http.get(environment.apiEndpoint + this.API_LOGOUT,
      httpOptions)
      .subscribe(resp => {
        this.data = resp;

        if (this.data['status'] === 'success') {
          this.sessiontListService.disconnect();
          this.router.navigate(['login']);
        }
      });
  }

  displayConsentPopup() {
    this.consentService.cancel();
    this.consentService.openDialog();
  }

  hasConsented() {
    return this.consentService.hasConsented();
  }

  isConnected() {
    return this.sessiontListService.isUserConnected();
  }

  loginName() {
    return this.sessiontListService.loginName();
  }

  navWithDrawer() {
  }
}
