import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';
import {VisioServersComponent} from './visio-servers.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {SessionListService} from '../session-list.service';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {MatTableModule} from '@angular/material/table';
import { LoginComponent } from '../login/login.component';

describe('VisioServersComponent', () => {
  let component: VisioServersComponent;
  let fixture: ComponentFixture<VisioServersComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule.withRoutes(
          [{path: 'login', component: LoginComponent}]
        ),
        ReactiveFormsModule,
        FormsModule,
        MatSnackBarModule,
        MatProgressSpinnerModule,
        MatTableModule,
        TranslateModule.forRoot()
      ],
      declarations: [VisioServersComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [SessionListService, TranslateService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisioServersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
