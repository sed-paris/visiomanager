'use strict';

import {Component, OnInit, OnDestroy, AfterViewInit, ViewChild} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {ActivatedRoute, Router} from '@angular/router';

import {catchError, timeout} from 'rxjs/operators';

import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';


import {environment} from '../../environments/environment';
import {SessionListService} from '../session-list.service';
import {FormBuilder} from '@angular/forms';
import {MatSnackBar} from '@angular/material/snack-bar';
import {TranslateService} from '@ngx-translate/core';

export interface VisioInstance {
  name: string;
  url: string;
  server_type: string;
  server_state: string;
  ip: string;
  created: string;
  duration: string;
  elapsed: string;
  zone: string;
}

const ELEMENT_DATA: VisioInstance[] = [];

@Component({
  selector: 'app-root',
  templateUrl: './visio-servers.component.html',
  styleUrls: ['./visio-servers.component.scss']
})
export class VisioServersComponent implements OnInit, AfterViewInit, OnDestroy {

  title = 'Visio Manager';

  displayedColumns: string[] = ['url', 'server_type', 'state', 'created', 'end', 'duration', 'elapsed', 'actionq'];
  dataSource = new MatTableDataSource(ELEMENT_DATA);

  hidePwd = true;

  @ViewChild(MatSort) sort: MatSort;

  html = {
    response: '',
    request_new_video: 'Launch a new BBB server',
    delete: 'delete'
  };

  showSpinner = false;

  API_DELETE_ALL_VISIO = '/delete_all_visio';
  API_GET_VISIO_INFO = '/check_visios';
  API_DELETE_SINGLE_VISIO = '/delete_single_visio';
  API_ADD_EXTRA_TIME = '/add_time_visio';

  // feedback message to be displayed after network calls
  message = '';

  hideForm = false;

  /**
   *
   * @param router
   * @param http
   * @param meta
   * @param formBuilder
   */
  constructor(
    private router: Router,
    private http: HttpClient,
    private sessiontListService: SessionListService,
    private snackBar: MatSnackBar,
    private translate: TranslateService
  ) {
    if (!sessiontListService.isUserConnected()) {
      this.router.navigate(['login']);
    }

  }

  ngOnInit() {
    this.dataSource.sort = this.sort;
    this.reloaVisiodInfo();
  }

  ngAfterViewInit() {
  }

  ngOnDestroy() {
  }

  reloaVisiodInfo() {
    // clean up current data
    this._getVisioStatus();
  }

  deleteAllVisio() {
    this._deleteAllVisio();
  }

  // Actions

  isUserServer(serverName) {
    return true;
  }

  detailsItem(serverName) {
  }

  extraTime(serverName) {

    // display spinner while loading data
    this.showSpinner = true;
    this.html.response = '';

    const instanceData = {
      name: serverName,
    };

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      })
    };

    // call backedn server for room info with 60 sec timeout
    this.http.post(environment.apiEndpoint + this.API_ADD_EXTRA_TIME, instanceData
      , httpOptions)
      .pipe(timeout(60000),
        catchError(e => {
          // do something on a timeout
          return null;
        }))
      .subscribe(data => {
          const response: any = data;
          if (response['status'] === 'success') {
            // completed, hides spinner
            // this.html.response = "Stopping the visio now (resources should be freed in a few minutes)";
            this.openSnackBar('30 min extra time added to your visio', 'OK');
            this.reloaVisiodInfo();
          } else {
            // completed, hides spinner
            // this.html.response = "Unable to stop visios : " + response["message"];
            this.openSnackBar('Unable to proceed', 'OK');
          }
          this.showSpinner = false;
        }, err => {
          // console.log('Error during http call ' + JSON.stringify(err));
          // this.html.response = "Unable to stop visios : " + JSON.stringify(err);
          this.openSnackBar('Unable to proceed', 'OK');
          this.showSpinner = false;
        }
      );
  }


  deleteItem(serverName) {

    // display spinner while loading data
    this.showSpinner = true;
    this.html.response = '';

    const instanceData = {
      name: serverName,
    };

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      })
    };

    // call backedn server for room info with 60 sec timeout
    this.http.post(environment.apiEndpoint + this.API_DELETE_SINGLE_VISIO, instanceData
      , httpOptions)
      .pipe(timeout(60000),
        catchError(e => {
          // do something on a timeout
          return null;
        }))
      .subscribe(data => {
          const response: any = data;
          if (response['status'] === 'success') {
            // completed, hides spinner
            // this.html.response = "Stopping the visio now (resources should be freed in a few minutes)";
            this.openSnackBar('Stopping the visio now (resource should be freed in a few minutes)"', 'OK');
          } else {
            // completed, hides spinner
            // this.html.response = "Unable to stop visios : " + response["message"];
            this.openSnackBar('Unable to proceed', 'OK');
          }
          this.showSpinner = false;
        }, err => {
          // console.log('Error during http call ' + JSON.stringify(err));
          // this.html.response = "Unable to stop visios : " + JSON.stringify(err);
          this.openSnackBar('Unable to proceed', 'OK');
          this.showSpinner = false;
        }
      );
  }

  cancel() {
  }

  isAdmin() {
    if (this.sessiontListService.isUserConnected()) {
      return this.sessiontListService.isAdmin();
    }
    return false;
  }

  //

  /**
   */
  async _deleteAllVisio() {

    // display spinner while loading data
    this.showSpinner = true;
    this.html.response = '';

    const httpOptions = {
      headers: new HttpHeaders({
        'Accept': 'application/json'
      })
    };

    // call backedn server for room info with 60 sec timeout
    this.http.get(environment.apiEndpoint + this.API_DELETE_ALL_VISIO
      , httpOptions)
      .pipe(timeout(60000),
        catchError(e => {
          // do something on a timeout
          return null;
        }))
      .subscribe(data => {
          const response: any = data;
          if (response['status'] === 'success') {
            // completed, hides spinner
            this.openSnackBar('Stopping all visios now (resources should be freed in a few minutes)', 'OK');
            // this.html.response = "Stopping all visios now (resources should be freed in a few minutes)";
          } else {
            // completed, hides spinner
            // this.html.response = "Unable to stop visios : " + response["message"];
            this.openSnackBar('Unable to proceed', 'OK');
          }
          this.showSpinner = false;
        }, err => {
          // console.log('Error during http call ' + JSON.stringify(err));
          // this.html.response = "Unable to stop visios : " + JSON.stringify(err);
          this.openSnackBar('Unable to proceed', 'OK');
          this.showSpinner = false;
        }
      );
  }

  /**
   */
  async _getVisioStatus() {

    // display spinner while loading data
    this.showSpinner = true;

    const httpOptions = {
      headers: new HttpHeaders({
        'Accept': 'application/json'
      })
    };

    // call backedn server for room info with 60 sec timeout
    this.http.get(environment.apiEndpoint + this.API_GET_VISIO_INFO
      , httpOptions)
      .pipe(timeout(60000),
        catchError(e => {
          // do something on a timeout
          return null;
        }))
      .subscribe(data => {
          const response: any = data;

          if (response['status'] === 'success') {
            // completed, hides spinner
            const visios = response['servers'] as VisioInstance[];

            ELEMENT_DATA.length = 0;
            for (const visio of visios) {
              const element: VisioInstance = visio as any as VisioInstance;
              ELEMENT_DATA.push(element);
            }
            this.dataSource = new MatTableDataSource(ELEMENT_DATA);
            this.openSnackBar('Instance list refreshed', 'OK');
          } else {
            // completed, hides spinner
            // console.log('Call failed');
            this.openSnackBar('Unable to proceed', 'OK');
          }
          this.showSpinner = false;
        }, err => {
          // console.log('Error during http call ' + JSON.stringify(err));
          this.showSpinner = false;
          this.openSnackBar('Unable to proceed', 'OK');
        }
      );
  }

  openSnackBar(message: string, action: string) {
    const snackBarRef = this.snackBar.open(message, action, {
      duration: 8000,
    });
    snackBarRef.afterDismissed().subscribe(() => {
      console.log('The snack-bar was dismissed');
    });
    snackBarRef.onAction().subscribe(() => {
      console.log('The snack-bar action was triggered!');
    });
  }
}
