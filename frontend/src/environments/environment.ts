export const environment = {
  apiEndpoint : 'http://localhost:4200/api',
  host: 'localhost',
  cas: false,
  production: false,
  enable_registration: false
};
