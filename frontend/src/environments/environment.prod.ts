export const environment = {
  apiEndpoint : 'https://salons.paris.inria.fr/api',
  host: 'salons.paris.inria.fr',
  cas: true,
  production: true,
    enable_registration: false
};
