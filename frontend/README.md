[[_TOC_]]
# Frontend server for VisioManager

The frontend server is generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.0.1.
This documentation explains how to configure, launch and test frontend server.

**Assumption** : we suppose that we start in `frontend` directory.

## Environment requirements

You have to install angular CLI on your host (check [https://angular.io/](https://angular.io/)).


## Configuring

TODO: configuration is hard-coded in `environment.ts` files

## Building & Launching frontend server

The frontend server may be built and launched according 3 configurations:

The different npm commands are defined in `package.json` file.
Environments are defined in `angular.json` file.

### Development

For development, you may launch with or without CAS functionality.

```
# Install npm/angular packages
npm install

# 1. Testing with cas
npm run start

# 2. Testing without cas
npm run start_without_cas
```

Access the Web app at:

- [http://toto.inria.fr:4200](http://toto.inria.fr) for scenario 1.
- [http://localhost:4200](http://localhost:4200) for scenario 2.


### Qualification & Production

To be able to launch qualif/prod application, you need to install `http-server`.
```
npm install -g http-server
```

For building and launching frontend server, use the following commands:

```
# Install npm/angular packages
npm install

# Scenario 1. Build qualification version
npm run build-qualif

# Scenario 2. Build production version
npm run build-prod

# Launch
http-server -p 4200 -c-1 dist/visiomanager/
```

Access the default superdsi/superdsi account to access requests and servers
pages.

## Testing

NPM testing commands are defined in `package.json`.

### Unit testing

For running unit tests ([.src/test.ts](./src/test.ts) collecting all *.spec.ts):

```
npm run test
```

**Note** : The command options are defined in `package.json` file.

### E2E testing

E2E testing needs that:

- the path to `CHROME_BIN` is correctly set,
- the version of webdriver-manager corresponds to the version of `CHROME_BIN` version,
- the backend server is launched (cf. [../backend/README.md](../backend/README.md))

Assuming that backend server is running:

```
export CHROME_BIN='/usr/bin/chromium-browser'
./node_modules/protractor/bin/webdriver-manager update --versions.chrome=$($CHROME_BIN --version | cut -d ' ' -f 2)
npm run e2e
```

**Note** : The command options are defined in `package.json` file.

## Misc (Angular)

This section is used as general tips about angular. It is not specific to this project.

### Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

### Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

### Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

### Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

### Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

### Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
