import { browser, by, element } from 'protractor';

export class AppPage {
  async navigateTo(): Promise<unknown> {
    return browser.get(browser.baseUrl);
  }

  async getTitleText(): Promise<string> {
    return element(by.css('app-root .body span')).getText();
  }

    async getSidenavTitle(): Promise<string> {
      var elem = element(by.id("navbarTitle"))
      return elem.getText();
  }
}
