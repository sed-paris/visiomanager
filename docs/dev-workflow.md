[[_TOC_]]

# Development workflow

If you want to contribute (thanks !), here is an explanation about our
development workflow. Some recommendations for proposing and validating merge
requests are also described here.

## Summary steps for contributing (all developers)

1. From **dev** branch, create a branch named 'ISSUEID-xxxx-yyy-zz' (i.e.,
issue number and short title separated by hyphens). For minor changes (e.g., typos, comments, ...), if there
is no issue number, your branch can be named 'minor-xxxx-yyy-zz'.
2. Make your changes and update CHANGELOG.md
3. Create a Merge Request (MR) from your branch into **dev** branch
4. Update the MR according the review and discussions
5. Once approved and pipeline succeeds, a maintainer can merge the MR by
   squashing or not your commits. If necessary, rebase your MR on **dev**
branch and fix conflicts.
6. In case of squash commit, pay attention to commit message and to the name of
   the main MR contributor.
7. After merge, the source branch is deleted.

## Branching model

The structure of our branches model is based on the [git-flow](https://nvie.com/posts/a-successful-git-branching-model/) model.

### Maintainer branches

We have two main branches with infinite lifetime:

- **master**: represents the production/deployment versions
- **dev**: represents the integration of all development changes

No contribution is made directly on these branches.

These branches are protected. Only maintainers are allowed to push on these
branches.

#### Development branch

This branch is used to have the last state of all integrated development
changes.

When it is considered enough stable, it may be merged into master branch for
being released and deployed.

#### Master branch

To remain quite simple with branching model, master branch is also used as
stabilization branch for preparing the release (e.g., changelog, version id)and
the deployment.

Once ready to deploy, the production version is tagged on master.
**The master branch must be merged back to dev branch.**

### Contribution branches

Next to these 2 branches, we use a limited life time branch model for all
contributions (feature development, bugfix, etc.).

So, to contribute, please create a branch named 'ISSUEID-xxxx-yyy-zz' (i.e.,
issue number and short title separated by hyphens). For minor changes, if there
is no issue number, your branch can be named 'minor-xxxx-yyy-zz'.

For example:

```
git checkout -b 64-add-contrib-workflow
```

**Do not forget to include updated CHANGELOG.md in your MR**

Once the corresponding Merge Request (MR) is approved, it is merged into
**dev** branch and the contribution branch is deleted.

#### Specific case for hotfix on production branch (for maintainers only)

During release process or after deployment, it may be possible to discover a
critical issue which needs to be fixed quickly for production.

For that, we also use contribution branch model, **except that the branch is
created from master and will be merged directly in master**.

Once a new version is deployed, do not forget to merge back the master branch
in **dev** branch to retrieve the last changes for this new version.

## Merge requests

For the project, we use Gitlab merge commit with semi-linear history. It means
that each merge creates a merge commit but we force a similar behavior as
fast-forward, i.e., if merge conflicts arise, the user is given the option to
rebase before merging.

### Standard contributions

Merge requests shall be proposed for merging contribution branch (created from
**dev** branch) to **dev** branch. All developers may propose this MR.

**Do not forget to include updated CHANGELOG.md**.

Once approved and pipeline succeeds, the maintainer can  merge the MR by
squashing commits (default behavior). **DO NOT FORGET TO CHANGE SQUASH COMMIT
MESSAGE !**

If you want to keep a certain granularity (with several commits), please ensure
that your commit history is enough clean to be readable and relevant.
It may be discussed with the reviewer/maintainer.

Once merged, the source branch is deleted (default).

### Hotfix contributions

Merge requests shall be proposed for merging contribution branch (created from
master branch) to master branch. Only maintainers may propose this MR (fixing
prod version).

**Do not forget to include CHANGELOG.md**.

Once approved and pipeline succeeds, you can merge the MR by squashing commits
(default behavior). **DO NOT FORGET TO CHANGE SQUASH COMMIT MESSAGE !**

If you want to keep a certain granularity (with several commits), please ensure
that your commit history is enough clean to be readable and relevant.

Once merged, the source branch is deleted (default).
