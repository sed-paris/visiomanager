# How to log in application ?

This document explains how is managed logging in the source code.

## Backend

Backend source code is written in Python language.

For logging, we use [Python logging module](https://docs.python.org/3/library/logging.html) for two reasons:

- Flask uses this module
- as it is the standard Python module, our application log can also include 
  messages from third-party modules.

### Logger configuration

Each module has its proper logger name corresponding to the module name.

Declare in each module file:
```
import logging
logger = logging.getLogger(__name__)
```

#### Logging output

The application has two handlers for logging:

- a stream handler: application logs directly in console at error level
- a file handler: application logs in files at debug level

The file handler logs in files using a timed rotating strategy, i.e., a new log
file named *runtime<suffix>.log* is created each day. Files are automatically
removed after 7 files.

### Logging levels

Python logging module has these default levels:

- DEBUG : Detailed information, typically of interest only when diagnosing
  problems.
- INFO : Confirmation that things are working as expected. 
- WARNING : An indication that something unexpected happened, or indicative of
  some problem in the near future (e.g. ‘disk space low’).
  The software is still working as expected.
- ERROR : Due to a more serious problem, the software has not been able to
  perform some function.
- CRITICAL : A serious error, indicating that the program itself may be unable
  to continue running.

Our strategy is to use info log at each function/method entry with input
arguments, debug log with some intermediate variables to be used for
debugging, warning log to alert something, error log when a problem occurs.