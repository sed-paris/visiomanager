# How to build the base images for BBB ?

This document explains how to build the base image, and specifically on Scaleway.

## creating scaleway images

- https://www.scaleway.com/en/docs/create-an-image-from-scratch/
- https://www.scaleway.com/en/docs/create-an-image-from-my-server/
- https://developers.scaleway.com/en/products/instance/api/#post-4026b9

## Certificate generation

First, a multi-domains certificate must be generated for the targeted subdomains.
This is achieved by the `generate_multi_domains_san.sh` script. It creates a folder 
with all the Let's Encrypt configuration files, and the generated multi-domains certs.

This script relies on the OVH API only. It can generate the cert for the test, qualif or prod projects.

## BBB image creation

The `build_base_image.sh`is the starting script to build images.
It is used first to build an initial BBB image in the context of a scaleway project, doing the full installation 
(including the certificate generation using certbot).

Then additional images (for the same scaleway project) can be derived, re-installing BBB (does not download/install 
anything, but reconfigures BBB/greenlight and their components to account for the new domain name.)

In order to build the base image, the following is required
- the project type (prod, qualif, test)
- the domain name for the image (visio1.drearch.online)
- the scaleway ID of the flexible IP reserved for this domain

To build additional images, the following parameters need to be provided
- the domaine of the base image
- the Scaleway image ID

