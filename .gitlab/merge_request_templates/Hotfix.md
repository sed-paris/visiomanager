---
name: Hotfix
about: Quick bugfix with very localized and low impact changes.

---

Closes #XXXX

**Description**

A brief description of the bugfix or the improvements.
If it has an associated issue please fix "Closes #XXXX" above.

**Changelog**

Please add a summary of the changes in CHANGELOG.md and in this MR:

E.g.
- Fixed Y (#XXXX)

**How to test**

List of steps to follow in order to reproduce this changes.

E.g.
- Compile XXXX
- Run script YYYY

**Misc**

Commentaries not related with the previous sections
