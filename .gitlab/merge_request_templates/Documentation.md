---
name: Documentation and Styling
about: Adding or modifying **documentation** or **code style**.

---

Closes #XXXX

**Description**

A brief description of the changes.

**Changelog**

Please add a summary of the changes in CHANGELOG.md and in this MR:

E.g.
- Added doc for X
- Code style for Y
