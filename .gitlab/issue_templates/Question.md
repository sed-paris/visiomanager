---
name: Question
about: Doubts and questions

---

**Description**

What is happening?

**Scope**

Which areas of *VisioManager* are involved

E.g.
- Compilation
- Core
- Documentation

