# Docker deployment

The `docker` folder contains the scripts and required to build the
application's Docker image and deploy the application using docker.

All commands should be started from the root folder of the app:

- `./docker/docker_build.sh`: script to build the app's Docker image, providing
  the image name (and version).
- `./docker/docker_run.sh`: script to launch the Web app, providing the full
  name of the Docker image.
- `./docker/docker_clean.sh`: script to clean previous docker containers &
  images, and save a version of the database.

## Building the docker image

As example, if you want to build an image and name it `visiomanager:local` :

```
$ ./docker/docker_build.sh visiomanager:local

```

## Configuring the docker image

Application configuration is not part of the Docker image, but may be provided
to the starting containers. 

Before using the `docker_run.sh` script, you have to follow some steps.

### Creating a local folder

You have to create a folder on your host machine that will be used to:

- provide configuration information to the docker containers
- store data (i.e., database, logs) outside the container.

The absolute path to this folder corresponds to the value of `VM_APP_DATA`
environment variable.

**Note**: Refer to "Setting environment variables" for more details.

### Providing the database

If you need to initialize a database, refer to 
[../backend/README.md](../backend/README.md) documentation.

The database shall be copied to `$VM_APP_DATA/resources/visiomanager_sqlite.db`

### Providing configuration files

Backend server may be configured using a `configs` directory.
By default, the image contains the example provided in the backend source code:
[../backend/configs](../backend/configs).

To modify this configuration, you have to create your own `configs` folder.
Refer to the example and the documentation (i.e.,
[../backend/configs/README.md](../backend/configs/README.md))

Then, you have to put your `configs` directory in `$VM_APP_DATA` folder.
It will allow this folder to be copied inside the container.

### Setting environment variables

Environment variables are required for different reasons.

*VM_ID_FILE* : name of the identify file (private key) to be used on the
Webapp host (in $HOME) to access scaleway instances (e.g., *id_rsa*)

This variable is used inside and outside the container. It means that the ssh
key shall be located on the host computer at `$HOME/.ssh` location.

1. For docker scripts

    - *VM_APP_DATA* : local (i.e., outside of container) folder containing the
      resources folder for database, the configs folder for configuration files
      and will be used for saving runtime logs.
    - *WEB_ENV* : defines the deployment environment, which will define the
      configuration file to use both on the backend and on frontend.
      Values can be prod/production, qualif, testing, dev/development.
    - *VM_ID_FILE*

2. For backend configuration

Refer to [../backend/README.md](../backend/README.md) for the list of variables
to set.

#### Set them all together

The simplest way is to use a dedicated .env file which stores all necessary
variables and their values. Then use this file to set all variables before
creating/starting containers.

As example, if your file is located in your home directory :

```
set -a
. $HOME/visiomanager.env
set +a
```

Once the environment variables are set, you can use `docker_run.sh` script.

**Note** : in order to reload the configuration, the containers (not the image)
must be terminated and recreated (not only stop/start).

#### Separate needs

Another possibility is to create a backend `backend.env` file to be copied
inside container for backend. This file shall be placed into `$VM_APP_DATA`.

For docker, you just have to set VM_APP_DATA, WEB_ENV and VM_ID_FILE.

TODO: Add CAS variable

### Setting up the database

When the docker container starts, it mounts the volume `$VM_APP_DATA/resources`
inside the container.

You have to ensure that:

- the environment `VM_APP_DATA` is correctly set
- the database named `visiomanager_sqlite.db` is accessible at the path
  `VM_APP_DATA/resources`

If your database is not existing, you can refer to
[../backend/README.md](../backend/README.md) documentation to create your
database.

## Launching the web application

```
$ ./docker/docker_run.sh <visio_image>
```

For development, the application may then be accessed using the following link:

- dev: <http://localhost:4200/>

TODO: for qualif/prod (for the time being, only salons.paris, 
frontend configuration to do)

**Beware**: since the frontend is being compiled when the
container is launched, it may take about 1 minute for the page to be available.
You can check the progress with `docker logs`.

