#!/usr/bin/env bash

script=$0

usage() {
    echo "Usage: $script IMAGE_NAME"
    echo
    echo "Stop and remove all containers based on docker image name"
    echo "The docker image is also removed"
    echo
    echo "Example: '$script visiomanager' deletes visiomanager images and all
    their containers"
}

# check that all required parameters are provided
if [ $# -ne 1 ]
then
    echo "Error : Invalid parameters"
    usage
    exit 1
fi

# stop and remove all containers based on image name
IMAGE_NAME="$1"

echo "Stop and delete containers from image name visiomanager"
container_ids=$(docker ps -a -q --filter name=visiomanager.*)
echo "Container ids are: $container_ids"

for container_id in $container_ids
do
    docker stop $container_id
    docker rm --force $container_id
done


now=`date +"%F_%H-%M-%S"`
echo "Save database at: ${VM_APP_DATA}/resources/backup_${now}"
cp ${VM_APP_DATA}/resources/visiomanager_sqlite.db ${VM_APP_DATA}/resources/backup_${now}

echo "Deleting image $IMAGE_NAME"
image_ids=$(docker images -q $IMAGE_NAME)
echo "Image ids are: $image_ids"

for image_id in $image_ids
do
    docker rmi --force $image_id
done
