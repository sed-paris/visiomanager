#!/usr/bin/env bash

script=$0

usage() {
    echo "Usage: $script IMAGE_NAME [DOCKER_OPTIONS]"
    echo
    echo "Run container from docker image IMAGE_NAME"
    echo
    echo "[DOCKER_OPTIONS] may be additional options for docker container"
    echo "Example: '$script visiomanager:1.0' runs docker image named
    visiomanager:1.0"
}


# check that all required parameters are provided
if [ $# -lt 1 ]
then
    usage
    exit 1
fi

IMAGE_NAME=${1}
shift

# Create local directories if not existing
mkdir -p ${VM_APP_DATA}/resources
mkdir -p ${VM_APP_DATA}/logs

# Launch backend container

container_id=$(docker create -p 127.0.0.1:5002:5002 \
    --name visiomanager-backend \
    --env SCW_S_KEY="${SCW_S_KEY}" \
    --env SCW_PROJECT_ID="${SCW_PROJECT_ID}" \
    --env SCW_BBB_ZONE="${SCW_BBB_ZONE}" \
    --env VM_ID_FILE="${VM_ID_FILE}" \
    --env VM_SENDER_ACCOUNT_PWD="${VM_SENDER_ACCOUNT_PWD}" \
    --env VM_SENDER_ACCOUNT_EMAIL="${VM_SENDER_ACCOUNT_EMAIL}" \
    --env VM_SENDER_ACCOUNT_SMTP="${VM_SENDER_ACCOUNT_SMTP}" \
    --env VM_ADMIN_EMAIL="${VM_ADMIN_EMAIL}" \
    --env DB_SALT="${DB_SALT}" \
    --env DB_SALT_B64="${DB_SALT_B64}" \
    --env WEB_ENV="${WEB_ENV}" \
    -u $(id -u ) \
    -v ${VM_APP_DATA}/resources:/app/resources \
    -v ${VM_APP_DATA}/logs:/app/backend/logs \
    "$@" \
    "$IMAGE_NAME" \
    ./docker/run_backend.sh)

# Copy SSH private key in container
docker cp $HOME/.ssh/$VM_ID_FILE $container_id:/.ssh/$VM_ID_FILE

# Copy backend.env file (if exists) in container
if [ -f "$VM_APP_DATA/backend.env" ]; then
    docker cp $VM_APP_DATA/backend.env $container_id:/app/backend/backend.env
fi

# Copy configs directory
if [ -d "$VM_APP_DATA/configs" ]; then
    docker cp $VM_APP_DATA/configs $container_id:/app/backend/
else
    echo "$VM_APP_DATA/configs does not exist: get default configs"
    docker cp backend/configs $container_id:/app/backend/
fi

# Start backend container
docker start $container_id

# Launch frontend container
container_id=$(docker create -p 127.0.0.1:4200:4200 \
    --name visiomanager-frontend \
    --env WEB_ENV="${WEB_ENV}" \
    "$@" \
    "$IMAGE_NAME" \
    ./docker/run_frontend.sh)

docker start $container_id

