#!/usr/bin/env bash

script=$0

usage() {
    echo "Usage: $script IMAGE_NAME [test]"
    echo
    echo "Build visiomanager image $IMAGE_NAME"
    echo
    echo "Example: '$script visiomanager:1.0' builds docker image
    visiomanager 1.0"
}

# check that all required parameters are provided
if [ $# -lt 1 ]
then
    usage
    exit 1
fi

if [ "$2" == "test" ]
then
    docker build -f ./docker/Dockerfile --target test-image --tag="$1" .
else
    docker build -f ./docker/Dockerfile --target prod-image --tag="$1" .
fi
