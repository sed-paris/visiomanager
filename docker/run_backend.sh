#!/bin/bash

cd backend || exit
export PYTHONPATH=.

case $WEB_ENV in
  development | dev) echo "Launch in development mode"
    python3 tools/wsgi.py
    ;;
  qualif) echo "Launch in qualif mode"
    python3 tools/wsgi.py --config qualif
    ;;
  production | prod) echo "Launch in production mode"
    python3 tools/wsgi.py --config prod
    ;;
  *)  echo "Default, launch in development mode"
    python3 tools/wsgi.py
    ;;
esac

