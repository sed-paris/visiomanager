#!/bin/bash

cd ./frontend || exit

case $WEB_ENV in
  development | dev) echo "Launch in development mode"
    npm run start
    ;;
  ci) echo "Launch in CI"
    npm run start
    ;;
  qualif) echo "Launch in qualif mode"
    npm run build-qualif
    http-server -p 4200 -c-1 dist/visiomanager/
    ;;
  production | prod) echo "Launch in production mode"
    npm run build-prod
    http-server -p 4200 -c-1 dist/visiomanager/
    ;;
  *)  echo "Default, launch in development mode"
    npm run start
    ;;
esac
