[[_TOC_]]
# VisioManager

**Master** [![build status](https://gitlab.inria.fr/sed-paris/visiomanager/badges/master/pipeline.svg)](https://gitlab.inria.fr/sed-paris/visiomanager/-/commits/master)

**Dev** [![build status](https://gitlab.inria.fr/sed-paris/visiomanager/badges/dev/pipeline.svg)](https://gitlab.inria.fr/sed-paris/visiomanager/-/commits/dev)

![](https://gitlab.inria.fr/sed-paris/visiomanager/-/wikis/uploads/396875cd06ed0434b9e60c0a134be36d/image.png)

Simple Web app to launch BBB instances on scaleway

## Project structure

The project is composed of:

- the backend source code
- the frontend source code
- the docker files and scripts for docker deployment
- the scaleway shell scripts (WIP: some of them are available in backend)

## How to configure the web application

Backend and frontend application should be configured with your own data
(e.g., scaleway configuration, emails, ...).

Configuration is made via json files and environment variables.

As it may depend on the deployment strategy (docker or not), the details are
described at different locations (i.e., backend, frontend, docker).

## How to build and run the web application

You have two methods for running the web application:

- using docker image
- in your computer environment

### Docker

For using docker, refer to [./docker/docker.md](./docker/docker.md) documentation

### Local computer

For running locally, you can launch backend and frontend server.

For backend server, refer to [./backend/README.md](./backend/README.md).
For frontend server, refer to [./frontend/README.md](./frontend/README.md).

## Scaleway shell scripts

These scripts use the curl/REST API of scaleway to create, manipulate, and
delete resources. The full documentation is available
[here](https://developers.scaleway.com/en/products/instance/api/#introduction).

This assumes you have an account on[Scaleway
Elements](https://console.scaleway.com/login#referer=%2Fdashboard%3F_ga%3D2.87301200.993177818.1605343433-1816858423.1605343433).

Live status of resources on Scaleway can be monitored
[here](https://console.scaleway.com/instance/servers).

Backend server uses these scripts, that's why backend should be configured for
scaleway via configuration files and environment variables.
Refer to [./backend/README.md](./backend/README.md) documentation.

## Deploying in production

The deployment in production is managed in a private repository.

For this project, the Gitlab CI is responsible to build and push docker image in
the container registry at each new tag.

Then the deployment repository can select the new tag for deploying in
qualif/prod servers.

It is also possible to trigger manually this deployment for protected branches
(i.e., master, dev). It allows to test the deployment during stabilization phase
for example.

