#!/usr/bin/env bash

# This script is used to monitor the service availability of salons-paris.
# It uses the monitoring backend APIs only available on localhost.
# An external scheduled program (like gitlab job) may be used to trigger the
# execution of this script on the host.

# Check if backend is still alive
res=`curl -s -o /dev/null -I -w "%{http_code}" -X GET "http://127.0.0.1:5002/health"`
if [ "$res" != "200" ]
then
    echo "Http status code:$res"
    echo "Impossible to access to health API"
    exit 1
fi

# Check if backend service is connected to scaleway
res=`curl -s -o /dev/null -I -w "%{http_code}" -X GET "http://127.0.0.1:5002/scw_health"`
if [ "$res" != "200" ]
then
    echo "Http status code:$res"
    echo "Impossible for backend to access to scaleway"
    exit 1
fi

