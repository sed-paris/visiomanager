#!/bin/bash

if [ "$#" -ne 3 ]
then
  echo >&2 "usage: $0 image_source_host target_host letsencrypt_folder"
  echo >&2 "   - example: $0 visio1.drearch.online visio2.drearch.online ./lets"
  exit 1
fi


# remove from know_hosts - hostname
ssh-keygen -R "$2"
# remove from know_hosts - ip
# host <hostname> returns either
# - <hostname>> has address <ip address>>
# - Host <hostname> not found: 3(NXDOMAIN)
# TODO: should check IP address is really an IP address
# TODO: dependency to host command
host_info=`host $2`
arr_host_info=($host_info)
echo ${arr_host_info[3]}
[[ ${arr_host_info[3]} =~ "found:" ]] && ret="INVALID" || ret="VALID"
echo host ip ${arr_host_info[3]} ${ret}
if [ "$ret" == "INVALID" ]
then
  exit 1
fi
ssh-keygen -R "${arr_host_info[3]}"

# add host in known hosts
ssh-keyscan -H "$2" >> $HOME/.ssh/known_hosts

## launch local script to add the multi-domains certs on remote host
## only copy the cert and not the config otherwise certbot complains
## of confiug error between webroot and dns challenges
echo "Copy multi-domains cert from $3/$1 to $2:/etc/letsencrypt/$1"
scp -rp $3/archive/$1/* root@$2:/etc/letsencrypt/archive/$1

echo "Will now update configs and nginx on new remote host"
ssh "root@$2" "bash -s" < ./srv_letsencrypt.sh "$1" "$2"


