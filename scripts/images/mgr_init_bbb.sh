#!/bin/bash

if [ "$#" -ne 4 ]
then
  echo >&2 "usage: $0 target_host identity_file target_host do_cert"
  echo >&2 "   - example: $0 myvisio.bbb.org id_rsa_bbb myvisio.bbb.org true"
  exit 1
fi


# remove from know_hosts - hostname
ssh-keygen -R "$1"

# remove from know_hosts - ip
# host <hostname> returns either
# - <hostname>> has address <ip address>>
# - Host <hostname> not found: 3(NXDOMAIN)
# TODO: should check IP address is really an IP address
# TODO: dependency to host command
host_info=`host $1`
arr_host_info=($host_info)
echo ${arr_host_info[3]}
[[ ${arr_host_info[3]} =~ "found:" ]] && ret="INVALID" || ret="VALID"
echo host ip ${arr_host_info[3]} ${ret}
if [ "$ret" == "INVALID" ]
then
  exit 1
fi
ssh-keygen -R "${arr_host_info[3]}"

# add host in known hosts
ssh-keyscan -H "$1" >> $HOME/.ssh/known_hosts

## launch local script to add account on remote host
ssh -i $HOME/.ssh/$2 "root@$1" "bash -s" < ./srv_init_bbb.sh "$3 $4"
