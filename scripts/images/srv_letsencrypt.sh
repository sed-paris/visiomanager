#!/bin/bash

# shell script on instance side to have proper ownership/mode on certificates
# and have the proper server definition in nginx
# restart nginx to be sure

if [ "$#" -ne 2 ]
then
  echo >&2 "usage: $0 image_source_host target_host"
  echo >&2 "   - example: $0 visio1.drearch.online visio2.drearch.online"
  exit 1
fi

echo "Certificates for $2 should be available at /etc/letsencrypt/live/$1"
chown -R root:root /etc/letsencrypt
# copy all newly copied certs from $1 to $2
# TODO: change to directly copy in the proper folder
cd /etc/letsencrypt/live/
# cannot link folder as bbb install script test for directory
# ln -s $1 $2
mkdir $2
cp -P $1/* $2/

echo "Update and restart nginx (update config, keep location of certs as overwritten)"
# update nginx config for site and restart
# use sites-available to preserve symlink
sed -i "s/$1/$2/g" /etc/nginx/sites-available/bigbluebutton
/etc/init.d/nginx restart
