#!/bin/bash

# shell script to clone the BBB repository
apt-get update
DEBIAN_FRONTEND=noninteractive apt-get upgrade -q -y -u  -o Dpkg::Options::="--force-confdef" --allow-downgrades --allow-remove-essential --allow-change-held-packages --allow-change-held-packages --allow-unauthenticated;
apt-get install -y git
git clone https://gitlab.inria.fr/rouillie/bbb.git
