#!/usr/bin/env bash

if [ "$#" -ne 2 ]
then
  echo >&2 "usage: $0 ovh_ini_file output_dir"
  echo     "     ./$0 /home/me/.ovh/ovh.ini /home/me/.ovhovh/21032021"
  exit 1
fi

LEDIR="$2/letsencrypt-test"
certbot certonly --dns-ovh --config-dir $LEDIR  --work-dir $LEDIR  --logs-dir $LEDIR --dns-ovh-credentials "$1" \
    -d visio1.drearch.online -d visio2.drearch.online -d visio3.drearch.online -d visio4.drearch.online \
    -d visio5.drearch.online

LEDIR="$2/letsencrypt-qualif"
certbot certonly --dns-ovh --config-dir $LEDIR  --work-dir $LEDIR  --logs-dir $LEDIR --dns-ovh-credentials "$1" \
    -d  qualif1.drearch.online -d  qualif2.drearch.online -d  qualif3.drearch.online

LEDIR="$2/letsencrypt-prod"
certbot certonly --dns-ovh --config-dir $LEDIR  --work-dir $LEDIR  --logs-dir $LEDIR --dns-ovh-credentials "$1" \
    -d  salon1.drearch.online -d  salon2.drearch.online -d  salon3.drearch.online \
    -d  salon4.drearch.online -d  salon5.drearch.online -d  salon6.drearch.online \
    -d  salon7.drearch.online -d  salon8.drearch.online -d  salon9.drearch.online \
    -d  salon10.drearch.online
