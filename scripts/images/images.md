# Building BBB Images on Scaleway

Building the BBB image on scaleway involves first generating the certificates, then generating images 
for each project (test, qualif, prod) and for each hostname.

## Certificates

Let's Encrypt certificates are used on the BBB images. When installing BBB with the scripts (on a target host), 
BBB uses CertBot with a host challenge to verify the hostname/domain, and then generates the private key/certificate 
for the host. The nginx configuration of the host is then modified to use these files.

As Salons Paris uses many hostnames to host the visios, there are issues wrt the 50 requests limit over a week 
on generating Let's Encrypt certificates. Thus wildcard certificates are used. Generating these certificates requires 
the use of a DNS challenge. The wildcard certificate is then used on each image. 

More accurately, a SAN certificate is generated for a list of hostnames (one for test hosts, one for qualif hosts, ...) 
and is used on these hosts (not a truly wildcard certificate that would require a more complex process).

**Info on the certificates issued by LetsEncrypt**  https://crt.sh/?q=drearch.online

### OVH API keys

As OVH is used to manage the domain drearch.online, the OVH plugin of CertBot is used to handle the DNS challenge:

https://buzut.net/certbot-challenge-dns-ovh-wildcard/

To use this plugin, a number of keys need to be generated by OVH to access their API
 
#### Preferred method (1 step) :

https://api.ovh.com/createToken/

Once logged in, and the API methods selected, the keys are generated with a duration of 1 day.
- Application Key
- Application Secret
- Consumer Key

Check link above for API paths, but can be:

    GET /domain/zone/*
    PUT /domain/zone/*
    POST /domain/zone/*
    DELETE /domain/zone/*

#### Alternatively (2 steps)

Login, create a temporary app (e.g., visiomanager) on the following page and retrieve the keys
- https://eu.api.ovh.com/createApp/

Then ask for a token valid for 1 day of the APIs

- Go to OVH API page : https://eu.api.ovh.com

And then, need to do explicit retrieval of consumer token thru curl

### Certbot

Prior to using the script to generate the scripts, Certbot needs to be installed
```
pip install certbot
pip install certbot-dns-ovh
 ```

Then create file `ovh.ini` in secure location, and add the various keys to access OVH API.
```
dns_ovh_endpoint = ovh-eu
dns_ovh_application_key = xyxYxYxYxYxYXYxxY
dns_ovh_application_secret = XyXyXyXYXYxyxyXyXYxYx
dns_ovh_consumer_key = xYXYXYXyXyxYxYxyXYx
```

### Certificate generation

Then can the script with the keys' file and the location where to put the generated certificates

```
./generate_multi_domains_san.sh /home/me/.ovh/ovh.ini /home/me/.ovh/21032021
```
This generates the certificates for all three configurations (test, qualif, prod)

The actual key/certificate are in the `live` subfolder and must be copied on the BBB server 

```
scp -rp letsencrypt-test root@visio1.drearch.online:/etc/letsencrypt
```

## Images

First, the proper Scaleway environment variables (project ID, access/secret keys...) needs to be set in order to access 
the Scaleway project's resources and APIs (test, qualif, prod).

Building images on Scaleway is a two step process. 
- generate a first image that will use the internal BBB host challenge process to get a certificate 
- generate additional images for the other hosts, copying directly the multi-hosts" SAN certificate

Building the initial image currently requires both the fully qualified hostname, and also the Scaleway ID 
for the IP address to use. 
```
./build_base_image.sh /home/me/.ovh/21032021 test visio1.drearch.online \
        4334ccff-5236-4023-8f60-3fd72aacd42a
```

Building the additional images requires the Scaleway ID of the initial image.

```
./build_base_image.sh /home/me/.ovh/21032021 test visio2.drearch.online \
        6f80d075-9e2e-4994-b80c-5a503e199cb1  \
        visio1.drearch.online 7583576f-2d27-4a8a-82da-7be135e7e561
```

Then the scaleway configuration files can be updated with the newly generated images' id.

