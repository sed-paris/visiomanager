import json
import os
import sys
from pathlib import Path
import subprocess


if __name__ == "__main__":
    if len(sys.argv) != 6:
        print(f'usage : {sys.argv[0]} scaleway_config base_image_id base_server_name cert_location target')
        print(f'        target is prod, qualif or test')
        sys.exit(-1)

    config_file = sys.argv[1]
    initial_image_id = sys.argv[2]
    initial_server_name = sys.argv[3]
    cert_loc = sys.argv[4]
    target = sys.argv[5]

    with open(config_file) as defs_file:
        defs = json.load(defs_file)

        for server_def in defs["dns"]:
            server_name = server_def["server"]
            ip_address = server_def["address"]
            ip_id = server_def["id"]

            print (f' - build for server {server_name} with ip id {ip_id}')

            cmd = ["./build_base_image.sh"]
            cmd.append("{}".format(cert_loc))
            cmd.append("{}".format(target))
            cmd.append("{}".format(server_name))
            cmd.append("{}".format(ip_id))
            cmd.append("{}".format(initial_server_name))
            cmd.append("{}".format(initial_image_id))

            env_vars = os.environ.copy()  # necessary if backend.env file is used

            print (f' - cmd {cmd}')


            try:
                process = subprocess.Popen(
                    cmd,
                    env=env_vars,
                    shell=False
                )

                # wait for maximum of 20 minutes, kill subprocess if no return
                data, err = process.communicate(timeout=1200)
            except subprocess.TimeoutExpired:
                print(f"Executing {cmd} failed: process communicate timeout")
                process.kill()
                data, err = process.communicate()

            if process.returncode == 0:
                print(f"Executing {cmd} succeeded")
            else:
                print(f"Executing {cmd} failed {err}")
